// Generated code from Butter Knife. Do not modify!
package com.kalajitha.musicearn.ui.activity.login;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kalajitha.musicearn.R;
import com.kalajitha.musicearn.view.ButtonMyriadProBold;
import com.kalajitha.musicearn.view.EditViewRegular;
import com.kalajitha.musicearn.view.TextViewBold;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LoginScreenActivity_ViewBinding implements Unbinder {
  private LoginScreenActivity target;

  private View view2131558531;

  private View view2131558532;

  @UiThread
  public LoginScreenActivity_ViewBinding(LoginScreenActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public LoginScreenActivity_ViewBinding(final LoginScreenActivity target, View source) {
    this.target = target;

    View view;
    target.editEmailNumber = Utils.findRequiredViewAsType(source, R.id.edit_email_number, "field 'editEmailNumber'", EditViewRegular.class);
    target.textinputLayoutEmailNumber = Utils.findRequiredViewAsType(source, R.id.textinput_layout_email_number, "field 'textinputLayoutEmailNumber'", TextInputLayout.class);
    target.editPassword = Utils.findRequiredViewAsType(source, R.id.edit_password, "field 'editPassword'", EditViewRegular.class);
    target.textinputLayoutPassword = Utils.findRequiredViewAsType(source, R.id.textinput_layout_password, "field 'textinputLayoutPassword'", TextInputLayout.class);
    view = Utils.findRequiredView(source, R.id.btn_login, "field 'btnLogin' and method 'OnClickButton'");
    target.btnLogin = Utils.castView(view, R.id.btn_login, "field 'btnLogin'", ButtonMyriadProBold.class);
    view2131558531 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnClickButton(Utils.<ButtonMyriadProBold>castParam(p0, "doClick", 0, "OnClickButton", 0));
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_singup, "field 'btnSingup' and method 'OnClickButton'");
    target.btnSingup = Utils.castView(view, R.id.btn_singup, "field 'btnSingup'", ButtonMyriadProBold.class);
    view2131558532 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnClickButton(Utils.<ButtonMyriadProBold>castParam(p0, "doClick", 0, "OnClickButton", 0));
      }
    });
    target.txtForgotPassword = Utils.findRequiredViewAsType(source, R.id.txt_forgot_password, "field 'txtForgotPassword'", TextViewBold.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    LoginScreenActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.editEmailNumber = null;
    target.textinputLayoutEmailNumber = null;
    target.editPassword = null;
    target.textinputLayoutPassword = null;
    target.btnLogin = null;
    target.btnSingup = null;
    target.txtForgotPassword = null;

    view2131558531.setOnClickListener(null);
    view2131558531 = null;
    view2131558532.setOnClickListener(null);
    view2131558532 = null;
  }
}
