package com.kalajitha.musicearn.ui.activity.otpverify;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class OTPVerifyModule_ProvideOTPVerifyPresenterFactory
    implements Factory<IOTPVerifyMVP.IOTPVerifyPresenter> {
  private final OTPVerifyModule module;

  private final Provider<IOTPVerifyMVP.IOTPVerifyModel> modelProvider;

  public OTPVerifyModule_ProvideOTPVerifyPresenterFactory(
      OTPVerifyModule module, Provider<IOTPVerifyMVP.IOTPVerifyModel> modelProvider) {
    assert module != null;
    this.module = module;
    assert modelProvider != null;
    this.modelProvider = modelProvider;
  }

  @Override
  public IOTPVerifyMVP.IOTPVerifyPresenter get() {
    return Preconditions.checkNotNull(
        module.provideOTPVerifyPresenter(modelProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<IOTPVerifyMVP.IOTPVerifyPresenter> create(
      OTPVerifyModule module, Provider<IOTPVerifyMVP.IOTPVerifyModel> modelProvider) {
    return new OTPVerifyModule_ProvideOTPVerifyPresenterFactory(module, modelProvider);
  }
}
