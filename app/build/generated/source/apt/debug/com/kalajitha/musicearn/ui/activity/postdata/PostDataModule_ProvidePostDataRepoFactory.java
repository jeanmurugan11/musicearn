package com.kalajitha.musicearn.ui.activity.postdata;

import com.kalajitha.musicearn.network.APIServiceInterface;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PostDataModule_ProvidePostDataRepoFactory implements Factory<IPostDataRepo> {
  private final PostDataModule module;

  private final Provider<APIServiceInterface> loginModuleAPIServiceProvider;

  public PostDataModule_ProvidePostDataRepoFactory(
      PostDataModule module, Provider<APIServiceInterface> loginModuleAPIServiceProvider) {
    assert module != null;
    this.module = module;
    assert loginModuleAPIServiceProvider != null;
    this.loginModuleAPIServiceProvider = loginModuleAPIServiceProvider;
  }

  @Override
  public IPostDataRepo get() {
    return Preconditions.checkNotNull(
        module.providePostDataRepo(loginModuleAPIServiceProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<IPostDataRepo> create(
      PostDataModule module, Provider<APIServiceInterface> loginModuleAPIServiceProvider) {
    return new PostDataModule_ProvidePostDataRepoFactory(module, loginModuleAPIServiceProvider);
  }
}
