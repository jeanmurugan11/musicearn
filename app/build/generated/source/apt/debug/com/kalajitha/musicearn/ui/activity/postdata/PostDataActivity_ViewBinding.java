// Generated code from Butter Knife. Do not modify!
package com.kalajitha.musicearn.ui.activity.postdata;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kalajitha.musicearn.R;
import com.kalajitha.musicearn.croppings.ImageViewRounded;
import com.kalajitha.musicearn.view.ButtonMyriadProBold;
import com.kalajitha.musicearn.view.EditViewRegular;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PostDataActivity_ViewBinding implements Unbinder {
  private PostDataActivity target;

  private View view2131558590;

  private View view2131558690;

  private View view2131558538;

  private View view2131558598;

  private View view2131558599;

  private View view2131558600;

  private View view2131558601;

  private View view2131558602;

  private View view2131558603;

  private View view2131558604;

  private View view2131558609;

  private View view2131558610;

  private View view2131558611;

  @UiThread
  public PostDataActivity_ViewBinding(PostDataActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public PostDataActivity_ViewBinding(final PostDataActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.image_profile, "field 'imageProfile' and method 'imageButtonOnclick'");
    target.imageProfile = Utils.castView(view, R.id.image_profile, "field 'imageProfile'", ImageViewRounded.class);
    view2131558590 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.imageButtonOnclick();
      }
    });
    target.nameMusicianFragmentProfix = Utils.findRequiredViewAsType(source, R.id.name_musician_fragment_profix, "field 'nameMusicianFragmentProfix'", EditViewRegular.class);
    view = Utils.findRequiredView(source, R.id.profile_orginaztion_edittext, "field 'orginaztionEdittext' and method 'submit'");
    target.orginaztionEdittext = Utils.castView(view, R.id.profile_orginaztion_edittext, "field 'orginaztionEdittext'", EditViewRegular.class);
    view2131558690 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.submit(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.profile_title_edittext, "field 'titleEdittext' and method 'submit'");
    target.titleEdittext = Utils.castView(view, R.id.profile_title_edittext, "field 'titleEdittext'", EditViewRegular.class);
    view2131558538 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.submit(p0);
      }
    });
    target.firstName = Utils.findRequiredViewAsType(source, R.id.first_name, "field 'firstName'", EditViewRegular.class);
    target.midileName = Utils.findRequiredViewAsType(source, R.id.midile_name, "field 'midileName'", EditViewRegular.class);
    target.lastName = Utils.findRequiredViewAsType(source, R.id.last_name, "field 'lastName'", EditViewRegular.class);
    target.mobileNumber = Utils.findRequiredViewAsType(source, R.id.mobile_number, "field 'mobileNumber'", EditViewRegular.class);
    target.emailAddress = Utils.findRequiredViewAsType(source, R.id.email_address, "field 'emailAddress'", EditViewRegular.class);
    view = Utils.findRequiredView(source, R.id.country_edittext, "field 'countryEdittext' and method 'submit'");
    target.countryEdittext = Utils.castView(view, R.id.country_edittext, "field 'countryEdittext'", EditViewRegular.class);
    view2131558598 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.submit(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.state_edittext, "field 'stateEdittext' and method 'submit'");
    target.stateEdittext = Utils.castView(view, R.id.state_edittext, "field 'stateEdittext'", EditViewRegular.class);
    view2131558599 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.submit(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.city_edittext, "field 'cityEdittext' and method 'submit'");
    target.cityEdittext = Utils.castView(view, R.id.city_edittext, "field 'cityEdittext'", EditViewRegular.class);
    view2131558600 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.submit(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.art_form, "field 'artForm' and method 'submit'");
    target.artForm = Utils.castView(view, R.id.art_form, "field 'artForm'", EditViewRegular.class);
    view2131558601 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.submit(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.category_edittext, "field 'categoryEdittext' and method 'submit'");
    target.categoryEdittext = Utils.castView(view, R.id.category_edittext, "field 'categoryEdittext'", EditViewRegular.class);
    view2131558602 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.submit(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.type_edittext, "field 'typeEdittext' and method 'submit'");
    target.typeEdittext = Utils.castView(view, R.id.type_edittext, "field 'typeEdittext'", EditViewRegular.class);
    view2131558603 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.submit(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.gredu_edittext, "field 'greduEdittext' and method 'submit'");
    target.greduEdittext = Utils.castView(view, R.id.gredu_edittext, "field 'greduEdittext'", EditViewRegular.class);
    view2131558604 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.submit(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.male_radio, "field 'maleRadio' and method 'onRadioButtonClicked'");
    target.maleRadio = Utils.castView(view, R.id.male_radio, "field 'maleRadio'", RadioButton.class);
    view2131558609 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onRadioButtonClicked(Utils.<RadioButton>castParam(p0, "doClick", 0, "onRadioButtonClicked", 0));
      }
    });
    view = Utils.findRequiredView(source, R.id.female_radio, "field 'femaleRadio' and method 'onRadioButtonClicked'");
    target.femaleRadio = Utils.castView(view, R.id.female_radio, "field 'femaleRadio'", RadioButton.class);
    view2131558610 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onRadioButtonClicked(Utils.<RadioButton>castParam(p0, "doClick", 0, "onRadioButtonClicked", 0));
      }
    });
    target.registerRadiogroup = Utils.findRequiredViewAsType(source, R.id.register_radiogroup, "field 'registerRadiogroup'", RadioGroup.class);
    view = Utils.findRequiredView(source, R.id.user_submit, "field 'userSubmit' and method 'createUser'");
    target.userSubmit = Utils.castView(view, R.id.user_submit, "field 'userSubmit'", ButtonMyriadProBold.class);
    view2131558611 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.createUser();
      }
    });
    target.childLinearLaout = Utils.findRequiredViewAsType(source, R.id.child_linearLaout, "field 'childLinearLaout'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    PostDataActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.imageProfile = null;
    target.nameMusicianFragmentProfix = null;
    target.orginaztionEdittext = null;
    target.titleEdittext = null;
    target.firstName = null;
    target.midileName = null;
    target.lastName = null;
    target.mobileNumber = null;
    target.emailAddress = null;
    target.countryEdittext = null;
    target.stateEdittext = null;
    target.cityEdittext = null;
    target.artForm = null;
    target.categoryEdittext = null;
    target.typeEdittext = null;
    target.greduEdittext = null;
    target.maleRadio = null;
    target.femaleRadio = null;
    target.registerRadiogroup = null;
    target.userSubmit = null;
    target.childLinearLaout = null;

    view2131558590.setOnClickListener(null);
    view2131558590 = null;
    view2131558690.setOnClickListener(null);
    view2131558690 = null;
    view2131558538.setOnClickListener(null);
    view2131558538 = null;
    view2131558598.setOnClickListener(null);
    view2131558598 = null;
    view2131558599.setOnClickListener(null);
    view2131558599 = null;
    view2131558600.setOnClickListener(null);
    view2131558600 = null;
    view2131558601.setOnClickListener(null);
    view2131558601 = null;
    view2131558602.setOnClickListener(null);
    view2131558602 = null;
    view2131558603.setOnClickListener(null);
    view2131558603 = null;
    view2131558604.setOnClickListener(null);
    view2131558604 = null;
    view2131558609.setOnClickListener(null);
    view2131558609 = null;
    view2131558610.setOnClickListener(null);
    view2131558610 = null;
    view2131558611.setOnClickListener(null);
    view2131558611 = null;
  }
}
