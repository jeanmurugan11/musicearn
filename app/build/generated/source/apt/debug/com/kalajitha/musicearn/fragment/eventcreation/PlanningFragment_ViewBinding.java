// Generated code from Butter Knife. Do not modify!
package com.kalajitha.musicearn.fragment.eventcreation;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kalajitha.musicearn.R;
import com.kalajitha.musicearn.croppings.ImageViewRounded;
import com.kalajitha.musicearn.view.ButtonMyriadProBold;
import com.kalajitha.musicearn.view.EditViewRegular;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PlanningFragment_ViewBinding implements Unbinder {
  private PlanningFragment target;

  private View view2131558633;

  private View view2131558635;

  private View view2131558636;

  private View view2131558638;

  private View view2131558639;

  private View view2131558640;

  @UiThread
  public PlanningFragment_ViewBinding(final PlanningFragment target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.event_purpose_name_edittext, "field 'eventPurposeNameEdittext' and method 'getPurposeoftheEvent'");
    target.eventPurposeNameEdittext = Utils.castView(view, R.id.event_purpose_name_edittext, "field 'eventPurposeNameEdittext'", EditViewRegular.class);
    view2131558633 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.getPurposeoftheEvent();
      }
    });
    target.eventTitleEdittext = Utils.findRequiredViewAsType(source, R.id.event_title_edittext, "field 'eventTitleEdittext'", EditViewRegular.class);
    view = Utils.findRequiredView(source, R.id.event_start_date_textview, "field 'eventStartDateTextview' and method 'dataTextViewOnclick'");
    target.eventStartDateTextview = Utils.castView(view, R.id.event_start_date_textview, "field 'eventStartDateTextview'", EditViewRegular.class);
    view2131558635 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.dataTextViewOnclick(Utils.<EditViewRegular>castParam(p0, "doClick", 0, "dataTextViewOnclick", 0));
      }
    });
    view = Utils.findRequiredView(source, R.id.event_end_date_textview, "field 'eventEndDateTextview' and method 'dataTextViewOnclick'");
    target.eventEndDateTextview = Utils.castView(view, R.id.event_end_date_textview, "field 'eventEndDateTextview'", EditViewRegular.class);
    view2131558636 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.dataTextViewOnclick(Utils.<EditViewRegular>castParam(p0, "doClick", 0, "dataTextViewOnclick", 0));
      }
    });
    target.eventSloganEditview = Utils.findRequiredViewAsType(source, R.id.event_slogan_editview, "field 'eventSloganEditview'", EditViewRegular.class);
    view = Utils.findRequiredView(source, R.id.event_creation_avatar, "field 'eventCreationAvatar' and method 'imageButtonOnclick'");
    target.eventCreationAvatar = Utils.castView(view, R.id.event_creation_avatar, "field 'eventCreationAvatar'", ImageViewRounded.class);
    view2131558638 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.imageButtonOnclick();
      }
    });
    view = Utils.findRequiredView(source, R.id.event_create_button, "field 'eventCreateButton' and method 'createEventButtonOnclick'");
    target.eventCreateButton = Utils.castView(view, R.id.event_create_button, "field 'eventCreateButton'", ButtonMyriadProBold.class);
    view2131558639 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.createEventButtonOnclick();
      }
    });
    view = Utils.findRequiredView(source, R.id.event_list, "field 'eventList' and method 'onItemSelected'");
    target.eventList = Utils.castView(view, R.id.event_list, "field 'eventList'", ListView.class);
    view2131558640 = view;
    ((AdapterView<?>) view).setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> p0, View p1, int p2, long p3) {
        target.onItemSelected(p2);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    PlanningFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.eventPurposeNameEdittext = null;
    target.eventTitleEdittext = null;
    target.eventStartDateTextview = null;
    target.eventEndDateTextview = null;
    target.eventSloganEditview = null;
    target.eventCreationAvatar = null;
    target.eventCreateButton = null;
    target.eventList = null;

    view2131558633.setOnClickListener(null);
    view2131558633 = null;
    view2131558635.setOnClickListener(null);
    view2131558635 = null;
    view2131558636.setOnClickListener(null);
    view2131558636 = null;
    view2131558638.setOnClickListener(null);
    view2131558638 = null;
    view2131558639.setOnClickListener(null);
    view2131558639 = null;
    ((AdapterView<?>) view2131558640).setOnItemClickListener(null);
    view2131558640 = null;
  }
}
