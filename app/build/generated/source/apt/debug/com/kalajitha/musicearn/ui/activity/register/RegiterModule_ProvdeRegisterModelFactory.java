package com.kalajitha.musicearn.ui.activity.register;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class RegiterModule_ProvdeRegisterModelFactory
    implements Factory<IRegisterMVP.IRegisterModel> {
  private final RegiterModule module;

  private final Provider<IRegisterRepository> repositoryProvider;

  public RegiterModule_ProvdeRegisterModelFactory(
      RegiterModule module, Provider<IRegisterRepository> repositoryProvider) {
    assert module != null;
    this.module = module;
    assert repositoryProvider != null;
    this.repositoryProvider = repositoryProvider;
  }

  @Override
  public IRegisterMVP.IRegisterModel get() {
    return Preconditions.checkNotNull(
        module.provdeRegisterModel(repositoryProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<IRegisterMVP.IRegisterModel> create(
      RegiterModule module, Provider<IRegisterRepository> repositoryProvider) {
    return new RegiterModule_ProvdeRegisterModelFactory(module, repositoryProvider);
  }
}
