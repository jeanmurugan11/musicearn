package com.kalajitha.musicearn.fragment.user;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MusicianModule_ProvideMusicianFragmentPresenterFactory
    implements Factory<MusicianMVP.Presenter> {
  private final MusicianModule module;

  private final Provider<MusicianMVP.Model> modelProvider;

  public MusicianModule_ProvideMusicianFragmentPresenterFactory(
      MusicianModule module, Provider<MusicianMVP.Model> modelProvider) {
    assert module != null;
    this.module = module;
    assert modelProvider != null;
    this.modelProvider = modelProvider;
  }

  @Override
  public MusicianMVP.Presenter get() {
    return Preconditions.checkNotNull(
        module.provideMusicianFragmentPresenter(modelProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<MusicianMVP.Presenter> create(
      MusicianModule module, Provider<MusicianMVP.Model> modelProvider) {
    return new MusicianModule_ProvideMusicianFragmentPresenterFactory(module, modelProvider);
  }
}
