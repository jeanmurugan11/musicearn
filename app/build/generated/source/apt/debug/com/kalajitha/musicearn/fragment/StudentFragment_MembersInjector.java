package com.kalajitha.musicearn.fragment;

import com.kalajitha.musicearn.fragment.user.MusicianMVP;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class StudentFragment_MembersInjector implements MembersInjector<StudentFragment> {
  private final Provider<MusicianMVP.Presenter> presenterProvider;

  public StudentFragment_MembersInjector(Provider<MusicianMVP.Presenter> presenterProvider) {
    assert presenterProvider != null;
    this.presenterProvider = presenterProvider;
  }

  public static MembersInjector<StudentFragment> create(
      Provider<MusicianMVP.Presenter> presenterProvider) {
    return new StudentFragment_MembersInjector(presenterProvider);
  }

  @Override
  public void injectMembers(StudentFragment instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.presenter = presenterProvider.get();
  }

  public static void injectPresenter(
      StudentFragment instance, Provider<MusicianMVP.Presenter> presenterProvider) {
    instance.presenter = presenterProvider.get();
  }
}
