// Generated code from Butter Knife. Do not modify!
package com.kalajitha.musicearn.ui.activity.register;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kalajitha.musicearn.R;
import com.kalajitha.musicearn.view.ButtonMyriadProBold;
import com.kalajitha.musicearn.view.EditViewRegular;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RegisterActivityScreenImp_ViewBinding implements Unbinder {
  private RegisterActivityScreenImp target;

  private View view2131558538;

  private View view2131558532;

  private View view2131558531;

  @UiThread
  public RegisterActivityScreenImp_ViewBinding(RegisterActivityScreenImp target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public RegisterActivityScreenImp_ViewBinding(final RegisterActivityScreenImp target, View source) {
    this.target = target;

    View view;
    target.editEmail = Utils.findRequiredViewAsType(source, R.id.edit_email, "field 'editEmail'", EditViewRegular.class);
    target.textinputLayoutEmail = Utils.findRequiredViewAsType(source, R.id.textinput_layout_email, "field 'textinputLayoutEmail'", TextInputLayout.class);
    target.editNumber = Utils.findRequiredViewAsType(source, R.id.edit_number, "field 'editNumber'", EditViewRegular.class);
    target.textinputLayoutNumber = Utils.findRequiredViewAsType(source, R.id.textinput_layout_number, "field 'textinputLayoutNumber'", TextInputLayout.class);
    view = Utils.findRequiredView(source, R.id.profile_title_edittext, "field 'profileTitleEdittext' and method 'submit'");
    target.profileTitleEdittext = Utils.castView(view, R.id.profile_title_edittext, "field 'profileTitleEdittext'", EditViewRegular.class);
    view2131558538 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.submit(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_singup, "field 'btnSingup' and method 'onClickButton'");
    target.btnSingup = Utils.castView(view, R.id.btn_singup, "field 'btnSingup'", ButtonMyriadProBold.class);
    view2131558532 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickButton(Utils.<ButtonMyriadProBold>castParam(p0, "doClick", 0, "onClickButton", 0));
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_login, "field 'btnLogin' and method 'onClickButton'");
    target.btnLogin = Utils.castView(view, R.id.btn_login, "field 'btnLogin'", ButtonMyriadProBold.class);
    view2131558531 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickButton(Utils.<ButtonMyriadProBold>castParam(p0, "doClick", 0, "onClickButton", 0));
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    RegisterActivityScreenImp target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.editEmail = null;
    target.textinputLayoutEmail = null;
    target.editNumber = null;
    target.textinputLayoutNumber = null;
    target.profileTitleEdittext = null;
    target.btnSingup = null;
    target.btnLogin = null;

    view2131558538.setOnClickListener(null);
    view2131558538 = null;
    view2131558532.setOnClickListener(null);
    view2131558532 = null;
    view2131558531.setOnClickListener(null);
    view2131558531 = null;
  }
}
