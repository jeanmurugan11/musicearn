package com.kalajitha.musicearn.network.loginmodule;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import okhttp3.OkHttpClient;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class LoginModuleAPIService_ProvideClientFactory implements Factory<OkHttpClient> {
  private final LoginModuleAPIService module;

  public LoginModuleAPIService_ProvideClientFactory(LoginModuleAPIService module) {
    assert module != null;
    this.module = module;
  }

  @Override
  public OkHttpClient get() {
    return Preconditions.checkNotNull(
        module.provideClient(), "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<OkHttpClient> create(LoginModuleAPIService module) {
    return new LoginModuleAPIService_ProvideClientFactory(module);
  }
}
