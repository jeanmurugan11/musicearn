// Generated code from Butter Knife. Do not modify!
package com.kalajitha.musicearn.fragment.eventcreation;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ListView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kalajitha.musicearn.R;
import com.kalajitha.musicearn.view.ButtonMyriadProBold;
import com.kalajitha.musicearn.view.EditViewRegular;
import com.kalajitha.musicearn.view.TextViewRegular;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProvideActivityDetails_ViewBinding implements Unbinder {
  private ProvideActivityDetails target;

  private View view2131558693;

  private View view2131558694;

  private View view2131558640;

  @UiThread
  public ProvideActivityDetails_ViewBinding(ProvideActivityDetails target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ProvideActivityDetails_ViewBinding(final ProvideActivityDetails target, View source) {
    this.target = target;

    View view;
    target.eventTitleEdittext = Utils.findRequiredViewAsType(source, R.id.event_title_edittext, "field 'eventTitleEdittext'", EditViewRegular.class);
    target.eventActivityEdittext = Utils.findRequiredViewAsType(source, R.id.event_activity_edittext, "field 'eventActivityEdittext'", EditViewRegular.class);
    target.eventStartDateTextview = Utils.findRequiredViewAsType(source, R.id.event_start_date_textview, "field 'eventStartDateTextview'", TextViewRegular.class);
    target.eventEndDateTextview = Utils.findRequiredViewAsType(source, R.id.event_end_date_textview, "field 'eventEndDateTextview'", TextViewRegular.class);
    view = Utils.findRequiredView(source, R.id.activity_purpose_name_edittext, "field 'activityPurposeNameEdittext' and method 'getPurposeoftheActivity'");
    target.activityPurposeNameEdittext = Utils.castView(view, R.id.activity_purpose_name_edittext, "field 'activityPurposeNameEdittext'", EditViewRegular.class);
    view2131558693 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.getPurposeoftheActivity();
      }
    });
    view = Utils.findRequiredView(source, R.id.venue_purpose_name_edittext, "field 'venuePurposeNameEdittext' and method 'getPurposeoftheVenue'");
    target.venuePurposeNameEdittext = Utils.castView(view, R.id.venue_purpose_name_edittext, "field 'venuePurposeNameEdittext'", EditViewRegular.class);
    view2131558694 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.getPurposeoftheVenue();
      }
    });
    target.eventCreateButton = Utils.findRequiredViewAsType(source, R.id.event_create_button, "field 'eventCreateButton'", ButtonMyriadProBold.class);
    view = Utils.findRequiredView(source, R.id.event_list, "field 'eventList' and method 'getPurposeoftheActivityList'");
    target.eventList = Utils.castView(view, R.id.event_list, "field 'eventList'", ListView.class);
    view2131558640 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.getPurposeoftheActivityList();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ProvideActivityDetails target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.eventTitleEdittext = null;
    target.eventActivityEdittext = null;
    target.eventStartDateTextview = null;
    target.eventEndDateTextview = null;
    target.activityPurposeNameEdittext = null;
    target.venuePurposeNameEdittext = null;
    target.eventCreateButton = null;
    target.eventList = null;

    view2131558693.setOnClickListener(null);
    view2131558693 = null;
    view2131558694.setOnClickListener(null);
    view2131558694 = null;
    view2131558640.setOnClickListener(null);
    view2131558640 = null;
  }
}
