package com.kalajitha.musicearn.ui.activity.register;

import com.kalajitha.musicearn.network.loginmodule.ILoginModuleAPIService;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class RegiterModule_ProvideRepoFactory implements Factory<IRegisterRepository> {
  private final RegiterModule module;

  private final Provider<ILoginModuleAPIService> apiServiceInterfaceProvider;

  public RegiterModule_ProvideRepoFactory(
      RegiterModule module, Provider<ILoginModuleAPIService> apiServiceInterfaceProvider) {
    assert module != null;
    this.module = module;
    assert apiServiceInterfaceProvider != null;
    this.apiServiceInterfaceProvider = apiServiceInterfaceProvider;
  }

  @Override
  public IRegisterRepository get() {
    return Preconditions.checkNotNull(
        module.provideRepo(apiServiceInterfaceProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<IRegisterRepository> create(
      RegiterModule module, Provider<ILoginModuleAPIService> apiServiceInterfaceProvider) {
    return new RegiterModule_ProvideRepoFactory(module, apiServiceInterfaceProvider);
  }
}
