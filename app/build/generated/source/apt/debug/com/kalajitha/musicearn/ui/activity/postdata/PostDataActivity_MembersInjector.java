package com.kalajitha.musicearn.ui.activity.postdata;

import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PostDataActivity_MembersInjector implements MembersInjector<PostDataActivity> {
  private final Provider<IPostDataMVP.IPresender> presenterProvider;

  public PostDataActivity_MembersInjector(Provider<IPostDataMVP.IPresender> presenterProvider) {
    assert presenterProvider != null;
    this.presenterProvider = presenterProvider;
  }

  public static MembersInjector<PostDataActivity> create(
      Provider<IPostDataMVP.IPresender> presenterProvider) {
    return new PostDataActivity_MembersInjector(presenterProvider);
  }

  @Override
  public void injectMembers(PostDataActivity instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.presenter = presenterProvider.get();
  }

  public static void injectPresenter(
      PostDataActivity instance, Provider<IPostDataMVP.IPresender> presenterProvider) {
    instance.presenter = presenterProvider.get();
  }
}
