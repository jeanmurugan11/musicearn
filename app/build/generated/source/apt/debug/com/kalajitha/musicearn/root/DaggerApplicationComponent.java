package com.kalajitha.musicearn.root;

import com.kalajitha.musicearn.fragment.AudienceFragment;
import com.kalajitha.musicearn.fragment.AudienceFragment_MembersInjector;
import com.kalajitha.musicearn.fragment.DancerFragment;
import com.kalajitha.musicearn.fragment.DancerFragment_MembersInjector;
import com.kalajitha.musicearn.fragment.GuestFragment;
import com.kalajitha.musicearn.fragment.GuestFragment_MembersInjector;
import com.kalajitha.musicearn.fragment.PressFragment;
import com.kalajitha.musicearn.fragment.PressFragment_MembersInjector;
import com.kalajitha.musicearn.fragment.SponserFragment;
import com.kalajitha.musicearn.fragment.SponserFragment_MembersInjector;
import com.kalajitha.musicearn.fragment.StudentFragment;
import com.kalajitha.musicearn.fragment.StudentFragment_MembersInjector;
import com.kalajitha.musicearn.fragment.TeacherFragment;
import com.kalajitha.musicearn.fragment.TeacherFragment_MembersInjector;
import com.kalajitha.musicearn.fragment.VenderFragment;
import com.kalajitha.musicearn.fragment.VenderFragment_MembersInjector;
import com.kalajitha.musicearn.fragment.eventcreation.PlanningFragment;
import com.kalajitha.musicearn.fragment.eventcreation.PlanningFragment_MembersInjector;
import com.kalajitha.musicearn.fragment.user.MusicianFragment;
import com.kalajitha.musicearn.fragment.user.MusicianFragment_MembersInjector;
import com.kalajitha.musicearn.fragment.user.MusicianMVP;
import com.kalajitha.musicearn.fragment.user.MusicianModule;
import com.kalajitha.musicearn.fragment.user.MusicianModule_ProvideMusicianFragmentModelFactory;
import com.kalajitha.musicearn.fragment.user.MusicianModule_ProvideMusicianFragmentPresenterFactory;
import com.kalajitha.musicearn.fragment.user.MusicianModule_ProvideRepoFactory;
import com.kalajitha.musicearn.fragment.user.Repository;
import com.kalajitha.musicearn.network.APIServiceInterface;
import com.kalajitha.musicearn.network.ApiModuleService;
import com.kalajitha.musicearn.network.ApiModuleService_ProvideApiServiceFactory;
import com.kalajitha.musicearn.network.loginmodule.ILoginModuleAPIService;
import com.kalajitha.musicearn.network.loginmodule.LoginModuleAPIService;
import com.kalajitha.musicearn.network.loginmodule.LoginModuleAPIService_ProvideApiServiceFactory;
import com.kalajitha.musicearn.ui.activity.login.LoginScreenActivity;
import com.kalajitha.musicearn.ui.activity.login.LoginScreenActivity_MembersInjector;
import com.kalajitha.musicearn.ui.activity.otpverify.IOTPVerifyMVP;
import com.kalajitha.musicearn.ui.activity.otpverify.IOTPVerifyRepository;
import com.kalajitha.musicearn.ui.activity.otpverify.OTPVerifyActivity;
import com.kalajitha.musicearn.ui.activity.otpverify.OTPVerifyActivity_MembersInjector;
import com.kalajitha.musicearn.ui.activity.otpverify.OTPVerifyModule;
import com.kalajitha.musicearn.ui.activity.otpverify.OTPVerifyModule_ProvideOTPRepositoryFactory;
import com.kalajitha.musicearn.ui.activity.otpverify.OTPVerifyModule_ProvideOTPVerfiyModelFactory;
import com.kalajitha.musicearn.ui.activity.otpverify.OTPVerifyModule_ProvideOTPVerifyPresenterFactory;
import com.kalajitha.musicearn.ui.activity.postdata.IPostDataMVP;
import com.kalajitha.musicearn.ui.activity.postdata.IPostDataRepo;
import com.kalajitha.musicearn.ui.activity.postdata.PostDataActivity;
import com.kalajitha.musicearn.ui.activity.postdata.PostDataActivity_MembersInjector;
import com.kalajitha.musicearn.ui.activity.postdata.PostDataModule;
import com.kalajitha.musicearn.ui.activity.postdata.PostDataModule_ProvdePostDataPresenterFactory;
import com.kalajitha.musicearn.ui.activity.postdata.PostDataModule_ProvidePostDataModelFactory;
import com.kalajitha.musicearn.ui.activity.postdata.PostDataModule_ProvidePostDataRepoFactory;
import com.kalajitha.musicearn.ui.activity.register.IRegisterMVP;
import com.kalajitha.musicearn.ui.activity.register.IRegisterRepository;
import com.kalajitha.musicearn.ui.activity.register.RegisterActivityScreenImp;
import com.kalajitha.musicearn.ui.activity.register.RegisterActivityScreenImp_MembersInjector;
import com.kalajitha.musicearn.ui.activity.register.RegiterModule;
import com.kalajitha.musicearn.ui.activity.register.RegiterModule_ProvdeRegisterModelFactory;
import com.kalajitha.musicearn.ui.activity.register.RegiterModule_ProvideRegisterPresenterFactory;
import com.kalajitha.musicearn.ui.activity.register.RegiterModule_ProvideRepoFactory;
import dagger.MembersInjector;
import dagger.internal.DoubleCheck;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DaggerApplicationComponent implements ApplicationComponent {
  private Provider<APIServiceInterface> provideApiServiceProvider;

  private Provider<Repository> provideRepoProvider;

  private Provider<MusicianMVP.Model> provideMusicianFragmentModelProvider;

  private Provider<MusicianMVP.Presenter> provideMusicianFragmentPresenterProvider;

  private MembersInjector<MusicianFragment> musicianFragmentMembersInjector;

  private MembersInjector<DancerFragment> dancerFragmentMembersInjector;

  private MembersInjector<StudentFragment> studentFragmentMembersInjector;

  private MembersInjector<TeacherFragment> teacherFragmentMembersInjector;

  private MembersInjector<GuestFragment> guestFragmentMembersInjector;

  private MembersInjector<PressFragment> pressFragmentMembersInjector;

  private MembersInjector<SponserFragment> sponserFragmentMembersInjector;

  private MembersInjector<AudienceFragment> audienceFragmentMembersInjector;

  private MembersInjector<VenderFragment> venderFragmentMembersInjector;

  private Provider<ILoginModuleAPIService> provideApiServiceProvider2;

  private Provider<IRegisterRepository> provideRepoProvider2;

  private Provider<IRegisterMVP.IRegisterModel> provdeRegisterModelProvider;

  private Provider<IRegisterMVP.IRegisterPresenter> provideRegisterPresenterProvider;

  private MembersInjector<RegisterActivityScreenImp> registerActivityScreenImpMembersInjector;

  private Provider<IOTPVerifyRepository> provideOTPRepositoryProvider;

  private Provider<IOTPVerifyMVP.IOTPVerifyModel> provideOTPVerfiyModelProvider;

  private Provider<IOTPVerifyMVP.IOTPVerifyPresenter> provideOTPVerifyPresenterProvider;

  private MembersInjector<OTPVerifyActivity> oTPVerifyActivityMembersInjector;

  private MembersInjector<LoginScreenActivity> loginScreenActivityMembersInjector;

  private Provider<IPostDataRepo> providePostDataRepoProvider;

  private Provider<IPostDataMVP.IModel> providePostDataModelProvider;

  private Provider<IPostDataMVP.IPresender> provdePostDataPresenterProvider;

  private MembersInjector<PostDataActivity> postDataActivityMembersInjector;

  private MembersInjector<PlanningFragment> planningFragmentMembersInjector;

  private DaggerApplicationComponent(Builder builder) {
    assert builder != null;
    initialize(builder);
  }

  public static Builder builder() {
    return new Builder();
  }

  public static ApplicationComponent create() {
    return builder().build();
  }

  @SuppressWarnings("unchecked")
  private void initialize(final Builder builder) {

    this.provideApiServiceProvider =
        ApiModuleService_ProvideApiServiceFactory.create(builder.apiModuleService);

    this.provideRepoProvider =
        DoubleCheck.provider(
            MusicianModule_ProvideRepoFactory.create(
                builder.musicianModule, provideApiServiceProvider));

    this.provideMusicianFragmentModelProvider =
        MusicianModule_ProvideMusicianFragmentModelFactory.create(
            builder.musicianModule, provideRepoProvider);

    this.provideMusicianFragmentPresenterProvider =
        MusicianModule_ProvideMusicianFragmentPresenterFactory.create(
            builder.musicianModule, provideMusicianFragmentModelProvider);

    this.musicianFragmentMembersInjector =
        MusicianFragment_MembersInjector.create(provideMusicianFragmentPresenterProvider);

    this.dancerFragmentMembersInjector =
        DancerFragment_MembersInjector.create(provideMusicianFragmentPresenterProvider);

    this.studentFragmentMembersInjector =
        StudentFragment_MembersInjector.create(provideMusicianFragmentPresenterProvider);

    this.teacherFragmentMembersInjector =
        TeacherFragment_MembersInjector.create(provideMusicianFragmentPresenterProvider);

    this.guestFragmentMembersInjector =
        GuestFragment_MembersInjector.create(provideMusicianFragmentPresenterProvider);

    this.pressFragmentMembersInjector =
        PressFragment_MembersInjector.create(provideMusicianFragmentPresenterProvider);

    this.sponserFragmentMembersInjector =
        SponserFragment_MembersInjector.create(provideMusicianFragmentPresenterProvider);

    this.audienceFragmentMembersInjector =
        AudienceFragment_MembersInjector.create(provideMusicianFragmentPresenterProvider);

    this.venderFragmentMembersInjector =
        VenderFragment_MembersInjector.create(provideMusicianFragmentPresenterProvider);

    this.provideApiServiceProvider2 =
        LoginModuleAPIService_ProvideApiServiceFactory.create(builder.loginModuleAPIService);

    this.provideRepoProvider2 =
        DoubleCheck.provider(
            RegiterModule_ProvideRepoFactory.create(
                builder.regiterModule, provideApiServiceProvider2));

    this.provdeRegisterModelProvider =
        RegiterModule_ProvdeRegisterModelFactory.create(
            builder.regiterModule, provideRepoProvider2);

    this.provideRegisterPresenterProvider =
        RegiterModule_ProvideRegisterPresenterFactory.create(
            builder.regiterModule, provdeRegisterModelProvider);

    this.registerActivityScreenImpMembersInjector =
        RegisterActivityScreenImp_MembersInjector.create(provideRegisterPresenterProvider);

    this.provideOTPRepositoryProvider =
        DoubleCheck.provider(
            OTPVerifyModule_ProvideOTPRepositoryFactory.create(
                builder.oTPVerifyModule, provideApiServiceProvider2));

    this.provideOTPVerfiyModelProvider =
        OTPVerifyModule_ProvideOTPVerfiyModelFactory.create(
            builder.oTPVerifyModule, provideOTPRepositoryProvider);

    this.provideOTPVerifyPresenterProvider =
        OTPVerifyModule_ProvideOTPVerifyPresenterFactory.create(
            builder.oTPVerifyModule, provideOTPVerfiyModelProvider);

    this.oTPVerifyActivityMembersInjector =
        OTPVerifyActivity_MembersInjector.create(provideOTPVerifyPresenterProvider);

    this.loginScreenActivityMembersInjector =
        LoginScreenActivity_MembersInjector.create(provideApiServiceProvider2);

    this.providePostDataRepoProvider =
        DoubleCheck.provider(
            PostDataModule_ProvidePostDataRepoFactory.create(
                builder.postDataModule, provideApiServiceProvider));

    this.providePostDataModelProvider =
        PostDataModule_ProvidePostDataModelFactory.create(
            builder.postDataModule, providePostDataRepoProvider);

    this.provdePostDataPresenterProvider =
        PostDataModule_ProvdePostDataPresenterFactory.create(
            builder.postDataModule, providePostDataModelProvider);

    this.postDataActivityMembersInjector =
        PostDataActivity_MembersInjector.create(provdePostDataPresenterProvider);

    this.planningFragmentMembersInjector =
        PlanningFragment_MembersInjector.create(provideApiServiceProvider);
  }

  @Override
  public void inject(MusicianFragment target) {
    musicianFragmentMembersInjector.injectMembers(target);
  }

  @Override
  public void inject(DancerFragment target) {
    dancerFragmentMembersInjector.injectMembers(target);
  }

  @Override
  public void inject(StudentFragment target) {
    studentFragmentMembersInjector.injectMembers(target);
  }

  @Override
  public void inject(TeacherFragment target) {
    teacherFragmentMembersInjector.injectMembers(target);
  }

  @Override
  public void inject(GuestFragment target) {
    guestFragmentMembersInjector.injectMembers(target);
  }

  @Override
  public void inject(PressFragment target) {
    pressFragmentMembersInjector.injectMembers(target);
  }

  @Override
  public void inject(SponserFragment target) {
    sponserFragmentMembersInjector.injectMembers(target);
  }

  @Override
  public void inject(AudienceFragment target) {
    audienceFragmentMembersInjector.injectMembers(target);
  }

  @Override
  public void inject(VenderFragment target) {
    venderFragmentMembersInjector.injectMembers(target);
  }

  @Override
  public void inject(RegisterActivityScreenImp target) {
    registerActivityScreenImpMembersInjector.injectMembers(target);
  }

  @Override
  public void inject(OTPVerifyActivity target) {
    oTPVerifyActivityMembersInjector.injectMembers(target);
  }

  @Override
  public void inject(LoginScreenActivity target) {
    loginScreenActivityMembersInjector.injectMembers(target);
  }

  @Override
  public void inject(PostDataActivity target) {
    postDataActivityMembersInjector.injectMembers(target);
  }

  @Override
  public void inject(PlanningFragment target) {
    planningFragmentMembersInjector.injectMembers(target);
  }

  public static final class Builder {
    private ApiModuleService apiModuleService;

    private MusicianModule musicianModule;

    private LoginModuleAPIService loginModuleAPIService;

    private RegiterModule regiterModule;

    private OTPVerifyModule oTPVerifyModule;

    private PostDataModule postDataModule;

    private Builder() {}

    public ApplicationComponent build() {
      if (apiModuleService == null) {
        this.apiModuleService = new ApiModuleService();
      }
      if (musicianModule == null) {
        this.musicianModule = new MusicianModule();
      }
      if (loginModuleAPIService == null) {
        this.loginModuleAPIService = new LoginModuleAPIService();
      }
      if (regiterModule == null) {
        this.regiterModule = new RegiterModule();
      }
      if (oTPVerifyModule == null) {
        this.oTPVerifyModule = new OTPVerifyModule();
      }
      if (postDataModule == null) {
        this.postDataModule = new PostDataModule();
      }
      return new DaggerApplicationComponent(this);
    }

    /**
     * @deprecated This module is declared, but an instance is not used in the component. This
     *     method is a no-op. For more, see https://google.github.io/dagger/unused-modules.
     */
    @Deprecated
    public Builder applicationModule(ApplicationModule applicationModule) {
      Preconditions.checkNotNull(applicationModule);
      return this;
    }

    public Builder apiModuleService(ApiModuleService apiModuleService) {
      this.apiModuleService = Preconditions.checkNotNull(apiModuleService);
      return this;
    }

    public Builder loginModuleAPIService(LoginModuleAPIService loginModuleAPIService) {
      this.loginModuleAPIService = Preconditions.checkNotNull(loginModuleAPIService);
      return this;
    }

    public Builder musicianModule(MusicianModule musicianModule) {
      this.musicianModule = Preconditions.checkNotNull(musicianModule);
      return this;
    }

    public Builder regiterModule(RegiterModule regiterModule) {
      this.regiterModule = Preconditions.checkNotNull(regiterModule);
      return this;
    }

    public Builder oTPVerifyModule(OTPVerifyModule oTPVerifyModule) {
      this.oTPVerifyModule = Preconditions.checkNotNull(oTPVerifyModule);
      return this;
    }

    public Builder postDataModule(PostDataModule postDataModule) {
      this.postDataModule = Preconditions.checkNotNull(postDataModule);
      return this;
    }
  }
}
