package com.kalajitha.musicearn.fragment;

import com.kalajitha.musicearn.fragment.user.MusicianMVP;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class VenderFragment_MembersInjector implements MembersInjector<VenderFragment> {
  private final Provider<MusicianMVP.Presenter> presenterProvider;

  public VenderFragment_MembersInjector(Provider<MusicianMVP.Presenter> presenterProvider) {
    assert presenterProvider != null;
    this.presenterProvider = presenterProvider;
  }

  public static MembersInjector<VenderFragment> create(
      Provider<MusicianMVP.Presenter> presenterProvider) {
    return new VenderFragment_MembersInjector(presenterProvider);
  }

  @Override
  public void injectMembers(VenderFragment instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.presenter = presenterProvider.get();
  }

  public static void injectPresenter(
      VenderFragment instance, Provider<MusicianMVP.Presenter> presenterProvider) {
    instance.presenter = presenterProvider.get();
  }
}
