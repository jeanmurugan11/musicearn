package com.kalajitha.musicearn.fragment.eventcreation;

import com.kalajitha.musicearn.network.APIServiceInterface;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PlanningFragment_MembersInjector implements MembersInjector<PlanningFragment> {
  private final Provider<APIServiceInterface> apiServiceInterfaceProvider;

  public PlanningFragment_MembersInjector(
      Provider<APIServiceInterface> apiServiceInterfaceProvider) {
    assert apiServiceInterfaceProvider != null;
    this.apiServiceInterfaceProvider = apiServiceInterfaceProvider;
  }

  public static MembersInjector<PlanningFragment> create(
      Provider<APIServiceInterface> apiServiceInterfaceProvider) {
    return new PlanningFragment_MembersInjector(apiServiceInterfaceProvider);
  }

  @Override
  public void injectMembers(PlanningFragment instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.apiServiceInterface = apiServiceInterfaceProvider.get();
  }

  public static void injectApiServiceInterface(
      PlanningFragment instance, Provider<APIServiceInterface> apiServiceInterfaceProvider) {
    instance.apiServiceInterface = apiServiceInterfaceProvider.get();
  }
}
