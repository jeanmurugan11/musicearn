package com.kalajitha.musicearn.fragment;

import com.kalajitha.musicearn.fragment.user.MusicianMVP;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class SponserFragment_MembersInjector implements MembersInjector<SponserFragment> {
  private final Provider<MusicianMVP.Presenter> presenterProvider;

  public SponserFragment_MembersInjector(Provider<MusicianMVP.Presenter> presenterProvider) {
    assert presenterProvider != null;
    this.presenterProvider = presenterProvider;
  }

  public static MembersInjector<SponserFragment> create(
      Provider<MusicianMVP.Presenter> presenterProvider) {
    return new SponserFragment_MembersInjector(presenterProvider);
  }

  @Override
  public void injectMembers(SponserFragment instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.presenter = presenterProvider.get();
  }

  public static void injectPresenter(
      SponserFragment instance, Provider<MusicianMVP.Presenter> presenterProvider) {
    instance.presenter = presenterProvider.get();
  }
}
