package com.kalajitha.musicearn.fragment.user;

import com.kalajitha.musicearn.network.APIServiceInterface;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MusicianModule_ProvideRepoFactory implements Factory<Repository> {
  private final MusicianModule module;

  private final Provider<APIServiceInterface> movieApiServiceProvider;

  public MusicianModule_ProvideRepoFactory(
      MusicianModule module, Provider<APIServiceInterface> movieApiServiceProvider) {
    assert module != null;
    this.module = module;
    assert movieApiServiceProvider != null;
    this.movieApiServiceProvider = movieApiServiceProvider;
  }

  @Override
  public Repository get() {
    return Preconditions.checkNotNull(
        module.provideRepo(movieApiServiceProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<Repository> create(
      MusicianModule module, Provider<APIServiceInterface> movieApiServiceProvider) {
    return new MusicianModule_ProvideRepoFactory(module, movieApiServiceProvider);
  }
}
