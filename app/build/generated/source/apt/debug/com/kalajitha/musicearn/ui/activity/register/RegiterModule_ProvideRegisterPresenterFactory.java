package com.kalajitha.musicearn.ui.activity.register;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class RegiterModule_ProvideRegisterPresenterFactory
    implements Factory<IRegisterMVP.IRegisterPresenter> {
  private final RegiterModule module;

  private final Provider<IRegisterMVP.IRegisterModel> modelProvider;

  public RegiterModule_ProvideRegisterPresenterFactory(
      RegiterModule module, Provider<IRegisterMVP.IRegisterModel> modelProvider) {
    assert module != null;
    this.module = module;
    assert modelProvider != null;
    this.modelProvider = modelProvider;
  }

  @Override
  public IRegisterMVP.IRegisterPresenter get() {
    return Preconditions.checkNotNull(
        module.provideRegisterPresenter(modelProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<IRegisterMVP.IRegisterPresenter> create(
      RegiterModule module, Provider<IRegisterMVP.IRegisterModel> modelProvider) {
    return new RegiterModule_ProvideRegisterPresenterFactory(module, modelProvider);
  }
}
