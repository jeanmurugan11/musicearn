package com.kalajitha.musicearn.ui.activity.otpverify;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class OTPVerifyModule_ProvideOTPVerfiyModelFactory
    implements Factory<IOTPVerifyMVP.IOTPVerifyModel> {
  private final OTPVerifyModule module;

  private final Provider<IOTPVerifyRepository> iotpVerifyRepositoryProvider;

  public OTPVerifyModule_ProvideOTPVerfiyModelFactory(
      OTPVerifyModule module, Provider<IOTPVerifyRepository> iotpVerifyRepositoryProvider) {
    assert module != null;
    this.module = module;
    assert iotpVerifyRepositoryProvider != null;
    this.iotpVerifyRepositoryProvider = iotpVerifyRepositoryProvider;
  }

  @Override
  public IOTPVerifyMVP.IOTPVerifyModel get() {
    return Preconditions.checkNotNull(
        module.provideOTPVerfiyModel(iotpVerifyRepositoryProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<IOTPVerifyMVP.IOTPVerifyModel> create(
      OTPVerifyModule module, Provider<IOTPVerifyRepository> iotpVerifyRepositoryProvider) {
    return new OTPVerifyModule_ProvideOTPVerfiyModelFactory(module, iotpVerifyRepositoryProvider);
  }
}
