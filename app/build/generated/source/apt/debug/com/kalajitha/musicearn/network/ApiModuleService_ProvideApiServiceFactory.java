package com.kalajitha.musicearn.network;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ApiModuleService_ProvideApiServiceFactory
    implements Factory<APIServiceInterface> {
  private final ApiModuleService module;

  public ApiModuleService_ProvideApiServiceFactory(ApiModuleService module) {
    assert module != null;
    this.module = module;
  }

  @Override
  public APIServiceInterface get() {
    return Preconditions.checkNotNull(
        module.provideApiService(), "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<APIServiceInterface> create(ApiModuleService module) {
    return new ApiModuleService_ProvideApiServiceFactory(module);
  }
}
