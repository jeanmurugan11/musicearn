package com.kalajitha.musicearn.network;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import okhttp3.OkHttpClient;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ApiModuleService_ProvideClientFactory implements Factory<OkHttpClient> {
  private final ApiModuleService module;

  public ApiModuleService_ProvideClientFactory(ApiModuleService module) {
    assert module != null;
    this.module = module;
  }

  @Override
  public OkHttpClient get() {
    return Preconditions.checkNotNull(
        module.provideClient(), "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<OkHttpClient> create(ApiModuleService module) {
    return new ApiModuleService_ProvideClientFactory(module);
  }
}
