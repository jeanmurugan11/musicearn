package com.kalajitha.musicearn.fragment.user;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MusicianModule_ProvideMusicianFragmentModelFactory
    implements Factory<MusicianMVP.Model> {
  private final MusicianModule module;

  private final Provider<Repository> repositoryProvider;

  public MusicianModule_ProvideMusicianFragmentModelFactory(
      MusicianModule module, Provider<Repository> repositoryProvider) {
    assert module != null;
    this.module = module;
    assert repositoryProvider != null;
    this.repositoryProvider = repositoryProvider;
  }

  @Override
  public MusicianMVP.Model get() {
    return Preconditions.checkNotNull(
        module.provideMusicianFragmentModel(repositoryProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<MusicianMVP.Model> create(
      MusicianModule module, Provider<Repository> repositoryProvider) {
    return new MusicianModule_ProvideMusicianFragmentModelFactory(module, repositoryProvider);
  }
}
