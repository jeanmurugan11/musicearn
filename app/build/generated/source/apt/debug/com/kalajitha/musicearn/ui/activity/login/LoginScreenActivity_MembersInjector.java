package com.kalajitha.musicearn.ui.activity.login;

import com.kalajitha.musicearn.network.loginmodule.ILoginModuleAPIService;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class LoginScreenActivity_MembersInjector
    implements MembersInjector<LoginScreenActivity> {
  private final Provider<ILoginModuleAPIService> iLoginModuleAPIServiceProvider;

  public LoginScreenActivity_MembersInjector(
      Provider<ILoginModuleAPIService> iLoginModuleAPIServiceProvider) {
    assert iLoginModuleAPIServiceProvider != null;
    this.iLoginModuleAPIServiceProvider = iLoginModuleAPIServiceProvider;
  }

  public static MembersInjector<LoginScreenActivity> create(
      Provider<ILoginModuleAPIService> iLoginModuleAPIServiceProvider) {
    return new LoginScreenActivity_MembersInjector(iLoginModuleAPIServiceProvider);
  }

  @Override
  public void injectMembers(LoginScreenActivity instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.iLoginModuleAPIService = iLoginModuleAPIServiceProvider.get();
  }

  public static void injectILoginModuleAPIService(
      LoginScreenActivity instance,
      Provider<ILoginModuleAPIService> iLoginModuleAPIServiceProvider) {
    instance.iLoginModuleAPIService = iLoginModuleAPIServiceProvider.get();
  }
}
