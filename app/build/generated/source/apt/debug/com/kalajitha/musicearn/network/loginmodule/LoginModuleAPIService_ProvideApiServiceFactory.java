package com.kalajitha.musicearn.network.loginmodule;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class LoginModuleAPIService_ProvideApiServiceFactory
    implements Factory<ILoginModuleAPIService> {
  private final LoginModuleAPIService module;

  public LoginModuleAPIService_ProvideApiServiceFactory(LoginModuleAPIService module) {
    assert module != null;
    this.module = module;
  }

  @Override
  public ILoginModuleAPIService get() {
    return Preconditions.checkNotNull(
        module.provideApiService(), "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<ILoginModuleAPIService> create(LoginModuleAPIService module) {
    return new LoginModuleAPIService_ProvideApiServiceFactory(module);
  }
}
