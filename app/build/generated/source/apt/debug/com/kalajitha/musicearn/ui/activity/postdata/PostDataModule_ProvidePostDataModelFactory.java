package com.kalajitha.musicearn.ui.activity.postdata;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PostDataModule_ProvidePostDataModelFactory
    implements Factory<IPostDataMVP.IModel> {
  private final PostDataModule module;

  private final Provider<IPostDataRepo> postDataRepoProvider;

  public PostDataModule_ProvidePostDataModelFactory(
      PostDataModule module, Provider<IPostDataRepo> postDataRepoProvider) {
    assert module != null;
    this.module = module;
    assert postDataRepoProvider != null;
    this.postDataRepoProvider = postDataRepoProvider;
  }

  @Override
  public IPostDataMVP.IModel get() {
    return Preconditions.checkNotNull(
        module.providePostDataModel(postDataRepoProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<IPostDataMVP.IModel> create(
      PostDataModule module, Provider<IPostDataRepo> postDataRepoProvider) {
    return new PostDataModule_ProvidePostDataModelFactory(module, postDataRepoProvider);
  }
}
