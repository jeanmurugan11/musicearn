package com.kalajitha.musicearn.network.loginmodule;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class LoginModuleAPIService_ProvideRetrofitFactory implements Factory<Retrofit> {
  private final LoginModuleAPIService module;

  private final Provider<String> baseURLProvider;

  private final Provider<OkHttpClient> clientProvider;

  public LoginModuleAPIService_ProvideRetrofitFactory(
      LoginModuleAPIService module,
      Provider<String> baseURLProvider,
      Provider<OkHttpClient> clientProvider) {
    assert module != null;
    this.module = module;
    assert baseURLProvider != null;
    this.baseURLProvider = baseURLProvider;
    assert clientProvider != null;
    this.clientProvider = clientProvider;
  }

  @Override
  public Retrofit get() {
    return Preconditions.checkNotNull(
        module.provideRetrofit(baseURLProvider.get(), clientProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<Retrofit> create(
      LoginModuleAPIService module,
      Provider<String> baseURLProvider,
      Provider<OkHttpClient> clientProvider) {
    return new LoginModuleAPIService_ProvideRetrofitFactory(
        module, baseURLProvider, clientProvider);
  }
}
