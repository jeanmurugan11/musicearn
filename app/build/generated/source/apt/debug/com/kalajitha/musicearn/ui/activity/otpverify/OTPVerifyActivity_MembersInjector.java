package com.kalajitha.musicearn.ui.activity.otpverify;

import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class OTPVerifyActivity_MembersInjector implements MembersInjector<OTPVerifyActivity> {
  private final Provider<IOTPVerifyMVP.IOTPVerifyPresenter> presenterProvider;

  public OTPVerifyActivity_MembersInjector(
      Provider<IOTPVerifyMVP.IOTPVerifyPresenter> presenterProvider) {
    assert presenterProvider != null;
    this.presenterProvider = presenterProvider;
  }

  public static MembersInjector<OTPVerifyActivity> create(
      Provider<IOTPVerifyMVP.IOTPVerifyPresenter> presenterProvider) {
    return new OTPVerifyActivity_MembersInjector(presenterProvider);
  }

  @Override
  public void injectMembers(OTPVerifyActivity instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.presenter = presenterProvider.get();
  }

  public static void injectPresenter(
      OTPVerifyActivity instance, Provider<IOTPVerifyMVP.IOTPVerifyPresenter> presenterProvider) {
    instance.presenter = presenterProvider.get();
  }
}
