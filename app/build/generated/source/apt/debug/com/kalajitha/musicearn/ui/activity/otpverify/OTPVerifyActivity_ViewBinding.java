// Generated code from Butter Knife. Do not modify!
package com.kalajitha.musicearn.ui.activity.otpverify;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kalajitha.musicearn.R;
import com.kalajitha.musicearn.view.ButtonMyriadProBold;
import com.kalajitha.musicearn.view.EditViewRegular;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OTPVerifyActivity_ViewBinding implements Unbinder {
  private OTPVerifyActivity target;

  private View view2131558541;

  @UiThread
  public OTPVerifyActivity_ViewBinding(OTPVerifyActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public OTPVerifyActivity_ViewBinding(final OTPVerifyActivity target, View source) {
    this.target = target;

    View view;
    target.editOtp = Utils.findRequiredViewAsType(source, R.id.edit_otp, "field 'editOtp'", EditViewRegular.class);
    target.editPassword = Utils.findRequiredViewAsType(source, R.id.edit_password, "field 'editPassword'", EditViewRegular.class);
    target.editConfirmPassword = Utils.findRequiredViewAsType(source, R.id.edit_confirm_password, "field 'editConfirmPassword'", EditViewRegular.class);
    view = Utils.findRequiredView(source, R.id.verify_otp, "field 'verifyOtp' and method 'onClickButton'");
    target.verifyOtp = Utils.castView(view, R.id.verify_otp, "field 'verifyOtp'", ButtonMyriadProBold.class);
    view2131558541 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickButton();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    OTPVerifyActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.editOtp = null;
    target.editPassword = null;
    target.editConfirmPassword = null;
    target.verifyOtp = null;

    view2131558541.setOnClickListener(null);
    view2131558541 = null;
  }
}
