package com.kalajitha.musicearn.ui.activity.otpverify;

import com.kalajitha.musicearn.network.loginmodule.ILoginModuleAPIService;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class OTPVerifyModule_ProvideOTPRepositoryFactory
    implements Factory<IOTPVerifyRepository> {
  private final OTPVerifyModule module;

  private final Provider<ILoginModuleAPIService> iLoginModuleAPIServiceProvider;

  public OTPVerifyModule_ProvideOTPRepositoryFactory(
      OTPVerifyModule module, Provider<ILoginModuleAPIService> iLoginModuleAPIServiceProvider) {
    assert module != null;
    this.module = module;
    assert iLoginModuleAPIServiceProvider != null;
    this.iLoginModuleAPIServiceProvider = iLoginModuleAPIServiceProvider;
  }

  @Override
  public IOTPVerifyRepository get() {
    return Preconditions.checkNotNull(
        module.provideOTPRepository(iLoginModuleAPIServiceProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<IOTPVerifyRepository> create(
      OTPVerifyModule module, Provider<ILoginModuleAPIService> iLoginModuleAPIServiceProvider) {
    return new OTPVerifyModule_ProvideOTPRepositoryFactory(module, iLoginModuleAPIServiceProvider);
  }
}
