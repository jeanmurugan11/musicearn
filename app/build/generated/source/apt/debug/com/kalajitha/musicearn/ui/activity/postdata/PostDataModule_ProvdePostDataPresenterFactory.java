package com.kalajitha.musicearn.ui.activity.postdata;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PostDataModule_ProvdePostDataPresenterFactory
    implements Factory<IPostDataMVP.IPresender> {
  private final PostDataModule module;

  private final Provider<IPostDataMVP.IModel> modelProvider;

  public PostDataModule_ProvdePostDataPresenterFactory(
      PostDataModule module, Provider<IPostDataMVP.IModel> modelProvider) {
    assert module != null;
    this.module = module;
    assert modelProvider != null;
    this.modelProvider = modelProvider;
  }

  @Override
  public IPostDataMVP.IPresender get() {
    return Preconditions.checkNotNull(
        module.provdePostDataPresenter(modelProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<IPostDataMVP.IPresender> create(
      PostDataModule module, Provider<IPostDataMVP.IModel> modelProvider) {
    return new PostDataModule_ProvdePostDataPresenterFactory(module, modelProvider);
  }
}
