package com.kalajitha.musicearn.ui.activity.register;

import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class RegisterActivityScreenImp_MembersInjector
    implements MembersInjector<RegisterActivityScreenImp> {
  private final Provider<IRegisterMVP.IRegisterPresenter> iRegisterPresenterProvider;

  public RegisterActivityScreenImp_MembersInjector(
      Provider<IRegisterMVP.IRegisterPresenter> iRegisterPresenterProvider) {
    assert iRegisterPresenterProvider != null;
    this.iRegisterPresenterProvider = iRegisterPresenterProvider;
  }

  public static MembersInjector<RegisterActivityScreenImp> create(
      Provider<IRegisterMVP.IRegisterPresenter> iRegisterPresenterProvider) {
    return new RegisterActivityScreenImp_MembersInjector(iRegisterPresenterProvider);
  }

  @Override
  public void injectMembers(RegisterActivityScreenImp instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.iRegisterPresenter = iRegisterPresenterProvider.get();
  }

  public static void injectIRegisterPresenter(
      RegisterActivityScreenImp instance,
      Provider<IRegisterMVP.IRegisterPresenter> iRegisterPresenterProvider) {
    instance.iRegisterPresenter = iRegisterPresenterProvider.get();
  }
}
