package com.kalajitha.musicearn.fragment.user;

import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class MusicianFragment_MembersInjector implements MembersInjector<MusicianFragment> {
  private final Provider<MusicianMVP.Presenter> presenterProvider;

  public MusicianFragment_MembersInjector(Provider<MusicianMVP.Presenter> presenterProvider) {
    assert presenterProvider != null;
    this.presenterProvider = presenterProvider;
  }

  public static MembersInjector<MusicianFragment> create(
      Provider<MusicianMVP.Presenter> presenterProvider) {
    return new MusicianFragment_MembersInjector(presenterProvider);
  }

  @Override
  public void injectMembers(MusicianFragment instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.presenter = presenterProvider.get();
  }

  public static void injectPresenter(
      MusicianFragment instance, Provider<MusicianMVP.Presenter> presenterProvider) {
    instance.presenter = presenterProvider.get();
  }
}
