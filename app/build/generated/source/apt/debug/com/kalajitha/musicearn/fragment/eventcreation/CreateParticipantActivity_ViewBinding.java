// Generated code from Butter Knife. Do not modify!
package com.kalajitha.musicearn.fragment.eventcreation;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.LinearLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.kalajitha.musicearn.R;
import com.kalajitha.musicearn.view.ButtonMyriadProBold;
import com.kalajitha.musicearn.view.ButtonMyriadProRegular;
import com.kalajitha.musicearn.view.EditViewRegular;
import com.kalajitha.musicearn.view.TextViewBold;
import com.kalajitha.musicearn.view.TextViewRegular;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CreateParticipantActivity_ViewBinding implements Unbinder {
  private CreateParticipantActivity target;

  @UiThread
  public CreateParticipantActivity_ViewBinding(CreateParticipantActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CreateParticipantActivity_ViewBinding(CreateParticipantActivity target, View source) {
    this.target = target;

    target.createTitleEdittext = Utils.findRequiredViewAsType(source, R.id.create_title_edittext, "field 'createTitleEdittext'", EditViewRegular.class);
    target.createActivityEdittext = Utils.findRequiredViewAsType(source, R.id.create_activity_edittext, "field 'createActivityEdittext'", EditViewRegular.class);
    target.createPurposeNameEdittext = Utils.findRequiredViewAsType(source, R.id.create_purpose_name_edittext, "field 'createPurposeNameEdittext'", EditViewRegular.class);
    target.createRoleEdittext = Utils.findRequiredViewAsType(source, R.id.create_role_edittext, "field 'createRoleEdittext'", EditViewRegular.class);
    target.createPerformanceEdittext = Utils.findRequiredViewAsType(source, R.id.create_performance_edittext, "field 'createPerformanceEdittext'", EditViewRegular.class);
    target.createTypeEdittext = Utils.findRequiredViewAsType(source, R.id.create_type_edittext, "field 'createTypeEdittext'", EditViewRegular.class);
    target.createNameEdittext = Utils.findRequiredViewAsType(source, R.id.create_name_edittext, "field 'createNameEdittext'", EditViewRegular.class);
    target.budgetTextview = Utils.findRequiredViewAsType(source, R.id.budget_textview, "field 'budgetTextview'", TextViewBold.class);
    target.createRenumerationEdittext = Utils.findRequiredViewAsType(source, R.id.create_renumeration_edittext, "field 'createRenumerationEdittext'", EditViewRegular.class);
    target.transportationTextview = Utils.findRequiredViewAsType(source, R.id.transportation_textview, "field 'transportationTextview'", TextViewRegular.class);
    target.transportationLayoutOne = Utils.findRequiredViewAsType(source, R.id.transportation_layout_one, "field 'transportationLayoutOne'", LinearLayout.class);
    target.accomadationTextview = Utils.findRequiredViewAsType(source, R.id.accomadation_textview, "field 'accomadationTextview'", TextViewRegular.class);
    target.transportationLayoutTwo = Utils.findRequiredViewAsType(source, R.id.transportation_layout_two, "field 'transportationLayoutTwo'", LinearLayout.class);
    target.createAccomadationEdittext = Utils.findRequiredViewAsType(source, R.id.create_accomadation_edittext, "field 'createAccomadationEdittext'", EditViewRegular.class);
    target.createParticipantButton = Utils.findRequiredViewAsType(source, R.id.create_participant_button, "field 'createParticipantButton'", ButtonMyriadProRegular.class);
    target.creationParticipantList = Utils.findRequiredViewAsType(source, R.id.creation_participant_list, "field 'creationParticipantList'", TextViewRegular.class);
    target.createButton = Utils.findRequiredViewAsType(source, R.id.create_button, "field 'createButton'", ButtonMyriadProBold.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CreateParticipantActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.createTitleEdittext = null;
    target.createActivityEdittext = null;
    target.createPurposeNameEdittext = null;
    target.createRoleEdittext = null;
    target.createPerformanceEdittext = null;
    target.createTypeEdittext = null;
    target.createNameEdittext = null;
    target.budgetTextview = null;
    target.createRenumerationEdittext = null;
    target.transportationTextview = null;
    target.transportationLayoutOne = null;
    target.accomadationTextview = null;
    target.transportationLayoutTwo = null;
    target.createAccomadationEdittext = null;
    target.createParticipantButton = null;
    target.creationParticipantList = null;
    target.createButton = null;
  }
}
