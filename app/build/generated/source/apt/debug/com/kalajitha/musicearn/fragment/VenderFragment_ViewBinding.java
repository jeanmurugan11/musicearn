// Generated code from Butter Knife. Do not modify!
package com.kalajitha.musicearn.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SearchView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kalajitha.musicearn.R;
import com.kalajitha.musicearn.croppings.ImageViewRounded;
import com.kalajitha.musicearn.view.ButtonMyriadProBold;
import com.kalajitha.musicearn.view.EditViewRegular;
import com.kalajitha.musicearn.view.TextViewBold;
import java.lang.IllegalStateException;
import java.lang.Override;

public class VenderFragment_ViewBinding implements Unbinder {
  private VenderFragment target;

  private View view2131558590;

  private View view2131558609;

  private View view2131558610;

  private View view2131558611;

  private View view2131558538;

  private View view2131558598;

  private View view2131558599;

  private View view2131558600;

  private View view2131558629;

  private View view2131558625;

  private View view2131558626;

  private View view2131558627;

  private View view2131558628;

  @UiThread
  public VenderFragment_ViewBinding(final VenderFragment target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.image_profile, "field 'profileImageMusicianFragment' and method 'imageButtonOnclick'");
    target.profileImageMusicianFragment = Utils.castView(view, R.id.image_profile, "field 'profileImageMusicianFragment'", ImageViewRounded.class);
    view2131558590 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.imageButtonOnclick();
      }
    });
    target.profixNameMusicianFragment = Utils.findRequiredViewAsType(source, R.id.name_musician_fragment_profix, "field 'profixNameMusicianFragment'", EditViewRegular.class);
    target.firstNameMusicianFragment = Utils.findRequiredViewAsType(source, R.id.first_name, "field 'firstNameMusicianFragment'", EditViewRegular.class);
    target.middleNameMusicianFragment = Utils.findRequiredViewAsType(source, R.id.midile_name, "field 'middleNameMusicianFragment'", EditViewRegular.class);
    target.lastNameMusicianFragment = Utils.findRequiredViewAsType(source, R.id.last_name, "field 'lastNameMusicianFragment'", EditViewRegular.class);
    target.mobileNumberMusicianFragment = Utils.findRequiredViewAsType(source, R.id.mobile_number, "field 'mobileNumberMusicianFragment'", EditViewRegular.class);
    target.emailMusicianFragment = Utils.findRequiredViewAsType(source, R.id.email_address, "field 'emailMusicianFragment'", EditViewRegular.class);
    view = Utils.findRequiredView(source, R.id.male_radio, "field 'maleRadio' and method 'onRadioButtonClicked'");
    target.maleRadio = Utils.castView(view, R.id.male_radio, "field 'maleRadio'", RadioButton.class);
    view2131558609 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onRadioButtonClicked(Utils.<RadioButton>castParam(p0, "doClick", 0, "onRadioButtonClicked", 0));
      }
    });
    view = Utils.findRequiredView(source, R.id.female_radio, "field 'femaleRadio' and method 'onRadioButtonClicked'");
    target.femaleRadio = Utils.castView(view, R.id.female_radio, "field 'femaleRadio'", RadioButton.class);
    view2131558610 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onRadioButtonClicked(Utils.<RadioButton>castParam(p0, "doClick", 0, "onRadioButtonClicked", 0));
      }
    });
    target.registerRadiogroup = Utils.findRequiredViewAsType(source, R.id.register_radiogroup, "field 'registerRadiogroup'", RadioGroup.class);
    view = Utils.findRequiredView(source, R.id.user_submit, "field 'userSubmit' and method 'createUser'");
    target.userSubmit = Utils.castView(view, R.id.user_submit, "field 'userSubmit'", ButtonMyriadProBold.class);
    view2131558611 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.createUser();
      }
    });
    view = Utils.findRequiredView(source, R.id.profile_title_edittext, "field 'titleEditText' and method 'submit'");
    target.titleEditText = Utils.castView(view, R.id.profile_title_edittext, "field 'titleEditText'", EditText.class);
    view2131558538 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.submit(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.country_edittext, "field 'countryEdittext' and method 'submit'");
    target.countryEdittext = Utils.castView(view, R.id.country_edittext, "field 'countryEdittext'", EditViewRegular.class);
    view2131558598 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.submit(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.state_edittext, "field 'stateEdittext' and method 'submit'");
    target.stateEdittext = Utils.castView(view, R.id.state_edittext, "field 'stateEdittext'", EditViewRegular.class);
    view2131558599 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.submit(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.city_edittext, "field 'cityEdittext' and method 'submit'");
    target.cityEdittext = Utils.castView(view, R.id.city_edittext, "field 'cityEdittext'", EditViewRegular.class);
    view2131558600 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.submit(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.profile_member_edittext, "field 'memberEdittext' and method 'submit'");
    target.memberEdittext = Utils.castView(view, R.id.profile_member_edittext, "field 'memberEdittext'", EditViewRegular.class);
    view2131558629 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.submit(p0);
      }
    });
    target.venderTextview = Utils.findRequiredViewAsType(source, R.id.vender_service_textview, "field 'venderTextview'", TextViewBold.class);
    target.searchView = Utils.findRequiredViewAsType(source, R.id.searchView, "field 'searchView'", SearchView.class);
    target.searchFrameLayout = Utils.findRequiredViewAsType(source, R.id.search_frame_layout, "field 'searchFrameLayout'", FrameLayout.class);
    target.list = Utils.findRequiredViewAsType(source, R.id.list, "field 'list'", ListView.class);
    target.index = Utils.findRequiredViewAsType(source, R.id.index, "field 'index'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.fab_add, "field 'fabAdd' and method 'listFloatButtonOnclick'");
    target.fabAdd = Utils.castView(view, R.id.fab_add, "field 'fabAdd'", FloatingActionButton.class);
    view2131558625 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.listFloatButtonOnclick(Utils.<FloatingActionButton>castParam(p0, "doClick", 0, "listFloatButtonOnclick", 0));
      }
    });
    target.parentFramagLaout = Utils.findRequiredViewAsType(source, R.id.parentFramagLaout, "field 'parentFramagLaout'", FrameLayout.class);
    view = Utils.findRequiredView(source, R.id.fab_list, "field 'fabList' and method 'listFloatButtonOnclick'");
    target.fabList = Utils.castView(view, R.id.fab_list, "field 'fabList'", FloatingActionButton.class);
    view2131558626 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.listFloatButtonOnclick(Utils.<FloatingActionButton>castParam(p0, "doClick", 0, "listFloatButtonOnclick", 0));
      }
    });
    target.childLinearLaout = Utils.findRequiredViewAsType(source, R.id.child_linearLaout, "field 'childLinearLaout'", LinearLayout.class);
    target.linearVenderList = Utils.findRequiredViewAsType(source, R.id.linear_vender_list, "field 'linearVenderList'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.yes_radio_button, "field 'yesRadio' and method 'onRadioButtonClicked'");
    target.yesRadio = Utils.castView(view, R.id.yes_radio_button, "field 'yesRadio'", RadioButton.class);
    view2131558627 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onRadioButtonClicked(Utils.<RadioButton>castParam(p0, "doClick", 0, "onRadioButtonClicked", 0));
      }
    });
    view = Utils.findRequiredView(source, R.id.no_radio_button, "field 'noRadio' and method 'onRadioButtonClicked'");
    target.noRadio = Utils.castView(view, R.id.no_radio_button, "field 'noRadio'", RadioButton.class);
    view2131558628 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onRadioButtonClicked(Utils.<RadioButton>castParam(p0, "doClick", 0, "onRadioButtonClicked", 0));
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    VenderFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.profileImageMusicianFragment = null;
    target.profixNameMusicianFragment = null;
    target.firstNameMusicianFragment = null;
    target.middleNameMusicianFragment = null;
    target.lastNameMusicianFragment = null;
    target.mobileNumberMusicianFragment = null;
    target.emailMusicianFragment = null;
    target.maleRadio = null;
    target.femaleRadio = null;
    target.registerRadiogroup = null;
    target.userSubmit = null;
    target.titleEditText = null;
    target.countryEdittext = null;
    target.stateEdittext = null;
    target.cityEdittext = null;
    target.memberEdittext = null;
    target.venderTextview = null;
    target.searchView = null;
    target.searchFrameLayout = null;
    target.list = null;
    target.index = null;
    target.fabAdd = null;
    target.parentFramagLaout = null;
    target.fabList = null;
    target.childLinearLaout = null;
    target.linearVenderList = null;
    target.yesRadio = null;
    target.noRadio = null;

    view2131558590.setOnClickListener(null);
    view2131558590 = null;
    view2131558609.setOnClickListener(null);
    view2131558609 = null;
    view2131558610.setOnClickListener(null);
    view2131558610 = null;
    view2131558611.setOnClickListener(null);
    view2131558611 = null;
    view2131558538.setOnClickListener(null);
    view2131558538 = null;
    view2131558598.setOnClickListener(null);
    view2131558598 = null;
    view2131558599.setOnClickListener(null);
    view2131558599 = null;
    view2131558600.setOnClickListener(null);
    view2131558600 = null;
    view2131558629.setOnClickListener(null);
    view2131558629 = null;
    view2131558625.setOnClickListener(null);
    view2131558625 = null;
    view2131558626.setOnClickListener(null);
    view2131558626 = null;
    view2131558627.setOnClickListener(null);
    view2131558627 = null;
    view2131558628.setOnClickListener(null);
    view2131558628 = null;
  }
}
