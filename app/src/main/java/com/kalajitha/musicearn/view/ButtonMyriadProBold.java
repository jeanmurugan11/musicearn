package com.kalajitha.musicearn.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

public class ButtonMyriadProBold extends Button{


    Context mContext;
    static String textFont = "Lato-Bold.ttf";

    public ButtonMyriadProBold(Context context) {
        super(context);
        this.mContext = context;
        inite();
    }

    public ButtonMyriadProBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        inite();
    }

    public ButtonMyriadProBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        inite();
    }

    private void inite() {
        if (!isInEditMode()) {
            if(textFont == "" ||textFont == null){

            }else {
                this.setTypeface(Typeface.createFromAsset(mContext.getAssets(), textFont));
            }
        }
    }
}



