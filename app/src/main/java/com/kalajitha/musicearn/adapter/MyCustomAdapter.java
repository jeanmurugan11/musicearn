package com.kalajitha.musicearn.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.kalajitha.musicearn.R;
import com.kalajitha.musicearn.model.response.VendorService;

import java.util.ArrayList;

public class MyCustomAdapter extends ArrayAdapter<VendorService> {

  public  ArrayList<VendorService> countryList;

    public MyCustomAdapter(Context context, int textViewResourceId,
                           ArrayList<VendorService> countryList) {
        super(context, textViewResourceId, countryList);
        this.countryList = new ArrayList<VendorService>();
        this.countryList.addAll(countryList);
    }

    private class ViewHolder {
        TextView code;
        CheckBox checkBox;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        Log.v("ConvertView", String.valueOf(position));

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.vender_service_list, null);

            holder = new ViewHolder();
            holder.code = (TextView) convertView.findViewById(R.id.code);
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBox1);
            convertView.setTag(holder);

            holder.checkBox.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;
                    VendorService country = (VendorService) cb.getTag();
                    Toast.makeText(getContext(), "Clicked on Checkbox: " + cb.getText() + " is " + cb.isChecked(),
                            Toast.LENGTH_LONG).show();
                    country.setSelected(cb.isChecked());
                }
            });
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        VendorService country = countryList.get(position);
        holder.code.setText(" (" + country.getCode() + ")");
        holder.checkBox.setText(country.getName());
        holder.checkBox.setChecked(country.isSelected());
        holder.checkBox.setTag(country);

        return convertView;

    }

}
