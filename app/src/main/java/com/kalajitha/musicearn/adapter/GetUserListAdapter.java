package com.kalajitha.musicearn.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kalajitha.musicearn.R;
import com.kalajitha.musicearn.model.response.UserResponce;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@SuppressLint("InflateParams")
public class GetUserListAdapter extends BaseAdapter {

	Context context;

	LayoutInflater inflater;

	private List<UserResponce> _data;

	private ArrayList<UserResponce> arraylist;

	Activity activity = (Activity) context;


	public GetUserListAdapter(Context context, List<UserResponce> cafeList) {
		this.context = context;
		this._data = cafeList;
		this.arraylist = new ArrayList<UserResponce>();
		this.arraylist.addAll(_data);
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return _data.size();
	}

	@Override
	public Object getItem(int location) {
		return _data.get(location);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		Viewholder holder;

		if(convertView==null){
			holder=new Viewholder();
			convertView=inflater.inflate(R.layout.favorite_row, parent, false);

			holder.thumbnail = (ImageView) convertView.findViewById(R.id.thumbnail);
			holder.name = (TextView) convertView.findViewById(R.id.name);
			holder.addres = (TextView) convertView.findViewById(R.id.addres);
			holder.parpamence = (TextView) convertView.findViewById(R.id.parpamence);

			convertView.setTag(holder);

		}else {
			holder=(Viewholder)convertView.getTag();
		}


		final UserResponce cafeitems = _data.get(position);

		holder.name.setText(cafeitems.getPrefixName() +" "+cafeitems.getTitleName()
				+ "  "+cafeitems.getFirstName() +" "+cafeitems.getMiddleName()+" " +cafeitems.getLastName());
		holder.addres.setText(cafeitems.getCityName());
		System.out.println("city-->"+cafeitems.getCityName());
		holder.parpamence.setText(cafeitems.getPerformanceTypeName());

		if( cafeitems.getProfileImage() != null && !"".equals(cafeitems.getProfileImage()) ){

			Transformation transformation = new RoundedTransformationBuilder()
					.borderColor(Color.GRAY)
					.borderWidthDp(1)
					.cornerRadiusDp(30)
					.oval(false)
					.build();

			Picasso.with(context)
					.load(cafeitems.getProfileImage())
					.fit()
					.error(R.drawable.ic_no_user)
					.transform(transformation)
					.into(holder.thumbnail);

		}
		else {
			setDefaultImage(holder.thumbnail);
		}

		return convertView;
	}

	public void setDefaultImage(ImageView imageView) {
		Transformation transformation = new RoundedTransformationBuilder()
				.borderColor(Color.GRAY)
				.borderWidthDp(1)
				.cornerRadiusDp(30)
				.oval(false)
				.build();

		Picasso.with(context)
				.load(R.drawable.ic_no_user)
				.fit()
				.error(R.drawable.ic_no_user)
				.transform(transformation)
				.into(imageView);

		imageView.setImageResource(R.drawable.ic_no_user);
	}


	// Filter Class
	public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		_data.clear();
		if (charText.length() == 0) {
			_data.addAll(arraylist);
		} else {
			for (UserResponce wp : arraylist) {
				if (wp.getFirstName().toLowerCase(Locale.getDefault()).contains(charText)) {
					_data.add(wp);
				}
			}
		}
		notifyDataSetChanged();
	}

	static class Viewholder{
		TextView name,addres,parpamence;
		ImageView thumbnail ;
		
	}


}