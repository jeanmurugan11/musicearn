package com.kalajitha.musicearn.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kalajitha.musicearn.R;
import com.kalajitha.musicearn.model.response.ListofCreatedEvent;
import com.kalajitha.musicearn.view.ButtonMyriadProBold;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("InflateParams")
public class EventListAdapter extends BaseAdapter {

	Context context;

	LayoutInflater inflater;

	private List<ListofCreatedEvent> _data;

	private ArrayList<ListofCreatedEvent> arraylist;

	Activity activity = (Activity) context;


	public interface EventActionButtonClick {
		void editButtonOnClick(View v, int position);
		void deleteButtonOnClick(View v, int position);
		void createActivityButtonOnClick(View v, int position);
	}

	private EventActionButtonClick callback;


	public void setEventActionButtonClickListener(EventActionButtonClick listener) {
		this.callback = listener;
	}


	public EventListAdapter(Context context, List<ListofCreatedEvent> cafeList) {
		this.context = context;
		this._data = cafeList;
		this.arraylist = new ArrayList<ListofCreatedEvent>();
		this.arraylist.addAll(_data);
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return _data.size();
	}

	@Override
	public Object getItem(int location) {
		return _data.get(location);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		Viewholder holder;

		if(convertView==null){
			holder=new Viewholder();
			convertView=inflater.inflate(R.layout.event_creation_list, parent, false);
			holder.eventPurpose = (TextView) convertView.findViewById(R.id.event_purpose_tv);
			holder.eventTitle = (TextView) convertView.findViewById(R.id.event_title_tv);
			holder.startDate = (TextView) convertView.findViewById(R.id.start_date_txt);
			holder.endDate = (TextView) convertView.findViewById(R.id.end_date_txt);

			holder.eventcreationDeleteButton = (ButtonMyriadProBold) convertView.findViewById(R.id.eventcreation_delete_button);
			holder.eventcreationEditButton = (ButtonMyriadProBold) convertView.findViewById(R.id.eventcreation_edit_button);
			holder.CreateactivityButton = (ButtonMyriadProBold) convertView.findViewById(R.id.eventcreation_createactivity_button);

			convertView.setTag(holder);

		}else {
			holder=(Viewholder)convertView.getTag();
		}


		final ListofCreatedEvent cafeitems = _data.get(position);

		holder.eventPurpose.setText(cafeitems.getEventPurpose());
		holder.eventTitle.setText(cafeitems.getEventTile());
		holder.startDate.setText(cafeitems.getEventStart());
		holder.endDate.setText(cafeitems.getEventEnd());

		/*holder.eventcreationDeleteButton.setTag(position,cafeitems);
		holder.eventcreationEditButton.setTag(position,cafeitems);
		holder.CreateactivityButton.setTag(position,cafeitems);*/

		holder.eventcreationEditButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if (callback != null) {
					callback.editButtonOnClick(v, position);
				}

			}
		});


		return convertView;
	}


	/*// Filter Class
	public void filter(String charText) {
		*//*charText = charText.toLowerCase(Locale.getDefault());
		_data.clear();
		if (charText.length() == 0) {
			_data.addAll(arraylist);
		} else {
			for (ListofCreatedEvent wp : arraylist) {
				if (wp.getFirstName().toLowerCase(Locale.getDefault()).contains(charText)) {
					_data.add(wp);
				}
			}
		}
		notifyDataSetChanged();*//*
	}*/

	static class Viewholder{
		TextView eventPurpose, eventTitle, startDate,endDate;
		ButtonMyriadProBold eventcreationDeleteButton, eventcreationEditButton, CreateactivityButton;

	}


}