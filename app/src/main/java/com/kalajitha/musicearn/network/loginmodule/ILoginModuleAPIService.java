package com.kalajitha.musicearn.network.loginmodule;


import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.GetPostRegistrationData;
import com.kalajitha.musicearn.model.response.OTPRegistrationResponse;
import com.kalajitha.musicearn.model.response.OTPVerifyResponse;
import com.kalajitha.musicearn.model.response.UserType;
import com.kalajitha.musicearn.network.IConstantsV2;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

public interface ILoginModuleAPIService {

    @POST(IConstantsV2.REGISTER_OTP)
    Observable<OTPRegistrationResponse> registerOTP(@Body Outer.OTPRequest otpRequest);

    @POST(IConstantsV2.GET_USER_TYPE)
    Observable<List<UserType>> getUserType(@Body String body);

    @POST(IConstantsV2.VALIDATE_OTP)
    Observable<OTPVerifyResponse> verifyOTP(@Body Outer.OTPVerifyRequest otpRequest);


    @POST(IConstantsV2.GET_POST_REGISTRATION)
    Call<GetPostRegistrationData> getPostRegistration(@Body Outer.GetPostData postData);    //Call postRegistration();
}

