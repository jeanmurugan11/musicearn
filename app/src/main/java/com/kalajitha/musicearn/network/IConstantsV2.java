package com.kalajitha.musicearn.network;

public interface IConstantsV2 {


    /*Saravanan api endpost*/
    String END_POINTS="http://45.113.189.118/MusicDev/"; /*This end pont used for login page only */
    String LOGIN ="token";
    String REGISTER="api/Account/Register";
    String GET_USER_TYPE="api/v2/Account/GetUserType";
    String REGISTER_OTP="api/v2/Account/OTPRegister";
    String VALIDATE_OTP="api/v2/Account/ValidateUserOTP";
    String GET_POST_REGISTRATION="api/PostRegistration/GetPostRegistrationData";
    String POST_REGISTRATION_DATA="api/PostRegistration/PostRegistrationUsers";


    /*Logesh api endpoints*/
    String ENDPOINT_NEW_TEST="http://45.113.189.118/MusicDev/api/";
    String GET_TITLE ="Profile/getTitleList";
    String GET_COUNTRY ="Profile/getCountryList";
    String GET_STATE ="Profile/getStateList";
    String GET_CITY ="Profile/getCityList";
    String GET_ARTFORM ="Profile/GetArtForm";
    String GET_CATEGORY ="Profile/GetArtFormCategory";
    String GET_TYPE ="Profile/GetArtPerformanceType";
    String GET_GREDU ="Profile/GetAirAccreditation";
    String GET_MEMBER ="Profile/GetMembershipTypeList"; /*Get the Member list it will use only in the user Module Audieance*/
    String GET_VENDER ="Profile/GetVendorServiceList"; /*get the Vender list which is used only in the User Module Vender Tab while clicking the add */
    String GET_ROLE ="Profile/GetRoleList";  /*it will give both organization list and instution list which is used to postdata registration page */


    String GET_USERS ="Profile/GetUsersList";
    String GET_BASIC_PROFILE_EDIT ="/Profile/EditUsers";
    String GET_DETAIL_PROFILE_EDIT ="/Profile/GetFullOrgProfile";

    String USERMODULE_USER_CREATION =ENDPOINT_NEW_TEST+"Profile/AddUsers";
    String USERMODULE_GET_PROFILE_EDIT=ENDPOINT_NEW_TEST+"Profile/EditUsers";
    String USERMODULE_PROFILE_UPDATE =ENDPOINT_NEW_TEST+"Profile/UpdateUsers";



    String MUSICIAN_USER_UPDATE=ENDPOINT_NEW_TEST+"Profile/UpdateUsers";



    /*Event creation */
    String PURPOSE_OF_EVENT="Event/cmbPurposeEvent"; /*Post Method  Request is empty*/
    String EVENT_CREATION="Event/CreateEvent"; /*Post Method  Form data eventPurposeID,eventTile,eventStart,eventEnd,slogan,UserName=9043147655,[file,filename]*/
    String GET_EVENT_LIST="Event/GetEventForDataTable"; /*Post Method  Form data eventPurposeID,eventTile,eventStart,eventEnd,slogan,UserName=9043147655,[file,filename]*/
}

