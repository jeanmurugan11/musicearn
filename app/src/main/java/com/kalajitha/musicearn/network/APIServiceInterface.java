package com.kalajitha.musicearn.network;


import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.AirFormCategoryList;
import com.kalajitha.musicearn.model.response.AirFormGreduList;
import com.kalajitha.musicearn.model.response.AirFormList;
import com.kalajitha.musicearn.model.response.AirFormTypeList;
import com.kalajitha.musicearn.model.response.CityList;
import com.kalajitha.musicearn.model.response.CountryList;
import com.kalajitha.musicearn.model.response.ListofCreatedEvent;
import com.kalajitha.musicearn.model.response.PurposeofEventResponse;
import com.kalajitha.musicearn.model.response.MemberShipList;
import com.kalajitha.musicearn.model.response.OTPRegistrationResponse;
import com.kalajitha.musicearn.model.response.OrginzationAndInstution;
import com.kalajitha.musicearn.model.response.StateList;
import com.kalajitha.musicearn.model.response.TitleList;
import com.kalajitha.musicearn.model.response.UserResponce;
import com.kalajitha.musicearn.model.response.UserType;
import com.kalajitha.musicearn.model.response.VenderOptionList;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

public interface APIServiceInterface {

    @POST(IConstantsV2.REGISTER)
    Observable<List<OTPRegistrationResponse>> getOTP(@Body Outer.OTPRequest otpRequest);

    @POST(IConstantsV2.GET_USER_TYPE)
    Observable<List<UserType>> getUserType(@Body String body);

    @POST(IConstantsV2.GET_TITLE)
    Observable<List<TitleList>> getTitleList(@Body String body);

    @POST(IConstantsV2.GET_COUNTRY)
    Observable<List<CountryList>> getCountryList(@Body String body);

    @POST(IConstantsV2.GET_STATE)
    Observable<List<StateList>> getStateList(@Body Outer.GetStateRequest stateRequest);

    @POST(IConstantsV2.GET_CITY)
    Observable<List<CityList>> getCityList(@Body Outer.GetCityRequest cityRequest);

    @POST(IConstantsV2.GET_ARTFORM)
    Observable<List<AirFormList>> getArtFormList(@Body String body);

    @POST(IConstantsV2.GET_CATEGORY)
    Observable<List<AirFormCategoryList>> getArtFormCatecory(@Body Outer.GetArtFormCategory artFormCategory);

    @POST(IConstantsV2.GET_TYPE)
    Observable<List<AirFormTypeList>> getArtFomType(@Body Outer.GetArtFormType artFormType);

    @POST(IConstantsV2.GET_GREDU)
    Observable<List<AirFormGreduList>> getGredu(@Body String body);

    @POST(IConstantsV2.GET_USERS)
    Observable<List<UserResponce>> getUserList(@Body Outer.GetUsers myOuter2);

    @POST(IConstantsV2.GET_MEMBER)
    Observable<List<MemberShipList>> getMemberList(@Body String body);

    @POST(IConstantsV2.GET_VENDER)
    Observable<List<VenderOptionList>> getVenderOptionList(@Body String body);

    @POST(IConstantsV2.GET_ROLE)
    Observable<List<OrginzationAndInstution>> getOrganization(@Body String body);

    //Observable<TitleList> getTopRatedMovies(@Query("page") Integer page);

    /*Event Creation*/

    @POST(IConstantsV2.PURPOSE_OF_EVENT)
    Call<List<PurposeofEventResponse>> getPurposeOfEvent();    //Call postRegistration();

    @POST(IConstantsV2.GET_EVENT_LIST)
    Call<List<ListofCreatedEvent>> createEvent(@Body Outer.EventTable myOuter2);    //Call postRegistration();
}

