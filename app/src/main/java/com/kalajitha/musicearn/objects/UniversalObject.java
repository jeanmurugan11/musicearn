package com.kalajitha.musicearn.objects;

/**
 * Created by murugan on 09/09/2016.
 */
public interface UniversalObject {


    void put(String key, Object value);

    Object get(String key, Object defaultValue);

}
