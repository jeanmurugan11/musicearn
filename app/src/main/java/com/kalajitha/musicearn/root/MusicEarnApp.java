package com.kalajitha.musicearn.root;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ConnectionQuality;
import com.androidnetworking.interfaces.ConnectionQualityChangeListener;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.kalajitha.musicearn.constants.Constants;
import com.kalajitha.musicearn.fragment.user.MusicianModule;
import com.kalajitha.musicearn.network.ApiModuleService;
import com.kalajitha.musicearn.network.loginmodule.LoginModuleAPIService;
import com.kalajitha.musicearn.objects.UniversalPreferences;
import com.kalajitha.musicearn.ui.activity.otpverify.OTPVerifyModule;
import com.kalajitha.musicearn.ui.activity.postdata.PostDataModule;
import com.kalajitha.musicearn.ui.activity.register.RegiterModule;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;


public class MusicEarnApp extends Application {

    public static final String TAG = MusicEarnApp.class.getSimpleName();

    private ApplicationComponent component;

    public static ProgressDialog pdialog=null;

    private static MusicEarnApp mInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        UniversalPreferences.initialize(this);

        //AndroidNetworking.initialize(getApplicationContext());
        OkHttpClient okHttpClient = new OkHttpClient() .newBuilder()
                .addNetworkInterceptor(new StethoInterceptor())
                .build();
        AndroidNetworking.initialize(getApplicationContext(),okHttpClient);
        AndroidNetworking.enableLogging();

        AndroidNetworking.setConnectionQualityChangeListener(new ConnectionQualityChangeListener() {
            @Override
            public void onChange(ConnectionQuality currentConnectionQuality, int currentBandwidth) {
                Log.d(TAG, "onChange: currentConnectionQuality : "
                        + currentConnectionQuality + " currentBandwidth : " + currentBandwidth);
            }
        });


        mInstance = this;

        component= DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .apiModuleService(new ApiModuleService())
                .musicianModule(new MusicianModule())
                .regiterModule(new RegiterModule())
                .loginModuleAPIService(new LoginModuleAPIService())
                .oTPVerifyModule(new OTPVerifyModule())
                .postDataModule(new PostDataModule())
                .build();

    }

    public ApplicationComponent getComponent(){

        return component;
    }

    /**
     * @return the main context of the Application
     */
    public static Context getAppContext() {
        return mInstance;
    }

    public static boolean isNetworkConnected(final Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public static void showProgress(final Context mContext) {

        try {
            pdialog = new ProgressDialog(mContext);
            pdialog.setMessage(Constants.Loading);
            pdialog.setCancelable(true);
            pdialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void closeProgress() {

        try {
            if(pdialog != null) {
                pdialog.dismiss();
            }

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static synchronized MusicEarnApp getInstance() {
        return mInstance;
    }

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;

    public boolean checkAndRequestPermissions(Activity activity) {

        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int writePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);


        List<String> listPermissionsNeeded = new ArrayList<>();

        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }

        if (writePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (readPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(activity, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }

        return true;
    }
    public static void hideKeyboar(Activity activity)
    {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0); // hide
    }
    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            if(inputMethodManager!=null)
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);

        }
        catch (Exception ex){ex.printStackTrace();}

    }
}
