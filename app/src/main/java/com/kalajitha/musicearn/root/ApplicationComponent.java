package com.kalajitha.musicearn.root;

import com.kalajitha.musicearn.fragment.AudienceFragment;
import com.kalajitha.musicearn.fragment.DancerFragment;
import com.kalajitha.musicearn.fragment.GuestFragment;
import com.kalajitha.musicearn.fragment.PressFragment;
import com.kalajitha.musicearn.fragment.SponserFragment;
import com.kalajitha.musicearn.fragment.StudentFragment;
import com.kalajitha.musicearn.fragment.TeacherFragment;
import com.kalajitha.musicearn.fragment.VenderFragment;
import com.kalajitha.musicearn.fragment.eventcreation.PlanningFragment;
import com.kalajitha.musicearn.fragment.user.MusicianFragment;
import com.kalajitha.musicearn.fragment.user.MusicianModule;
import com.kalajitha.musicearn.network.ApiModuleService;
import com.kalajitha.musicearn.network.loginmodule.LoginModuleAPIService;
import com.kalajitha.musicearn.ui.activity.login.LoginScreenActivity;
import com.kalajitha.musicearn.ui.activity.otpverify.OTPVerifyActivity;
import com.kalajitha.musicearn.ui.activity.otpverify.OTPVerifyModule;
import com.kalajitha.musicearn.ui.activity.postdata.PostDataActivity;
import com.kalajitha.musicearn.ui.activity.postdata.PostDataModule;
import com.kalajitha.musicearn.ui.activity.register.RegisterActivityScreenImp;
import com.kalajitha.musicearn.ui.activity.register.RegiterModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by supercomputer on 18/5/17.
 */

@Singleton
@Component(modules = {
        ApplicationModule.class,
        ApiModuleService.class,
        LoginModuleAPIService.class,
        MusicianModule.class,
        RegiterModule.class,
        OTPVerifyModule.class,
        PostDataModule.class})
public interface ApplicationComponent {

    void inject(MusicianFragment target);
    void inject(DancerFragment target);
    void inject(StudentFragment target);
    void inject(TeacherFragment target);
    void inject(GuestFragment target);
    void inject(PressFragment target);
    void inject(SponserFragment target);
    void inject(AudienceFragment target);
    void inject(VenderFragment target);

    void inject(RegisterActivityScreenImp target);
    void inject(OTPVerifyActivity target);
    void inject(LoginScreenActivity target);
    void inject(PostDataActivity target);

    /*Event creation start*/
    void inject(PlanningFragment target);

    //void inject(MusicianFragment target);

}

