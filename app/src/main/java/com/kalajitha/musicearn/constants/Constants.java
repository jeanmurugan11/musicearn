package com.kalajitha.musicearn.constants;

import android.graphics.Bitmap;

/**
 * Created by murugan on 09.05.2017.
 */
//need to be refactored
public class Constants {

    public static final long REQUEST_TIME_OUT = 20L;
    public static final int UPDATE_PROVIDER_LOCATION_TIME_DELAY = 1000 * 60 * 10;//milliseconds
    public static final int START_UPDATE_PROVIDER_LOCATION_TIME = 9;//hours


    public static final String SLASH = "/";
    public static final String ZERO = "0";
    public static final String WHITE_SPACE = " ";

    public static final String COLON = ":";
    public static final String SERVICE = "service_";
    public static final String WHITE = "_white";
    public static final String COMA = ",";
    public static final String DOT = ".";
    public static final String COMA_WHITE_SPACE = ", ";
    public static final String TAB = "\n";
    public static final String OK_TEXT = "OK";

    public static final String REQUEST_ERROR_POP_UP_TAG = "request_error_pop_up_tag";
    public static final String JAMES_ERROR_POP_UP_TAG = "james_error_pop_up_tag";
    //    public static final String CHANGE_PASSWORD_POP_UP_TAG = "change_password_pop_up_tag";
    public static final String CREDENTIALS_ERROR_POP_UP_TAG = "request_error_pop_up_tag";
    public static final String ALERT_DIALOG_POP_UP_TAG = "alert_dialog_pop_up_tag";
    public static final String JAMES_JOB_CANCELED_TAG = "james_job_canceled_tag";


    public static final String IS_LOGGED_KEY = "IS_LOGGED_KEY";
    public static final String USER_EMAIL_KEY = "user_email_key";
    public static final String USER_PASSWORD_KEY = "user_password_key";

    public static final String FEED_BACK_EMAIL = "services.client@james-info.com";
    public static final String FEED_SUBJECT = "feed back";



    public static class Statuses {
        public static final String CANCEL_JOB = "cancel";
        public static final String ACCEPT_JOB = "accept";

        public static final String CONFIRM_PAYMENT = "confirm_payment";
        public static final String FINISH_JOB = "finish";
        public static final String RATE = "rate";

        public static final String WAITING_STATUS = "waiting";
        public static final String SEARCHING_STATUS = "searching";
        public static final String CANCELED_BY_PROVIDER_STATUS = "canceled_by_provider";
        public static final String CANCELED_BY_CLIENT_STATUS = "canceled_by_client";
        public static final String CANCELED_BY_SEARCH = "canceled_by_search";

        public static final String QUEUED_STATUS = "queued";
        public static final String ACTIVE_STATUS = "active";
        public static final String PAYED_STATUS = "payed";

//        public static final String CONFIRM_SHOWN_STATUS = "confirmed";
//        public static final String ACCEPT_SHOWN_STATUS = "accepted";
//        public static final String START_SHOWN_STATUS = "in progress";
//        public static final String FINISH_SHOWN_STATUS = "done";
//        public static final String CANCELED_SHOWN_STATUS = "cancelled";
//        public static final String WAIT_SHOWN_STATUS = "waiting";
    }

    public static class Animation {
        public static final int SPLASH_SCREEN_ANIMATION_DELAY = 2000;
    }

    public static class URLs {

        public static final String ROOT_JAMES_URL = "http://195.154.42.148";
        public static final String AUTH_URL = "/api/auth";
        public static final String USER_URL = "/api/user";
        public static final String JOB_URL = "/api/user/job";
        public static final String JOB_URL_WITH_ID = "/api/user/job/{id}";
        public static final String AVAILABLE_JAMES_URL = "/api/provider";
        public static final String SERVICE_URL = "/api/service";
        public static final String SUB_SERVICE_URL = "/api/service/{serviceId}/sub-service";
        public static final String FORGOT_PASSWORD_URL = "/api/auth/password";
        public static final String USER_ADDRESS_URL = "/api/user/address";
        public static final String PROVIDER_PROFILE_SETUP_URL = "http://james.lamed-consulting.com";
    }


    public static class APIResponseCodes {
        public static final int SUCCESS = 200;
        public static final int UNAUTHORIZED = 401;
        public static final int NOT_FOUND = 404;
//        public static final int FAILED_VALIDATION = 422;
//        public static final int SERVER_ERROR = 500;
    }

    public static class Headers {
        public static final String Authorization = "Authorization";
        //            public static final String ContentType = "Content-Type";
        public static final String BASIC = "Basic";
    }


    public static final String Loading = "Loading...";
    public static final String Uploading = "Uploading...";
    public static final String InternetConnection_Fails = "Internet Connection Required";
    public static final String InternalServerError = "Internal Server Error";
    public static final String UnableToConnectServer = "Unable to connect Server";
    public static final String SomethingWentWrong = "Something went wrong on Server";


    public static final String ProfixNameRequeiredMessage="Please enter Your ProfixName";
    public static final String FirstNameRequeiredMessage="Please enter Your First Name";
    public static final String LastNameRequeiredMessage="Please enter Your Last Name";
    public static final String MiddleNameRequeiredMessage="Please enter Your Middle Name";
    public static final String PhoneNumberRequeiredMessage="Your phone number should have minimum 10 number";
    public static final String EmailRequeiredMessage="Please enter a valid email id";
    public static final String TittleRequeiredMessage="Please Choose Tittle";
    public static final String CountryRequeiredMessage="Please Choose Country";
    public static final String StateRequeiredMessage="Please Choose State";
    public static final String CityRequeiredMessage="Please Choose City";
    public static final String AirFormRequeiredMessage="Please Choose AirForm";
    public static final String AirForCategormRequeiredMessage="Please Choose Main Performance Category";
    public static final String AirForTypeRequeiredMessage="Please Choose Main Performance Type";
    public static final String AirForGradeRequeiredMessage="Please Choose Main Performance Type";
    public static final String MemberShipIdRequeiredMessage="Please Choose Member";
    public static final String OrginizationRequeiredMessage="Please Choose organization";
    public static final String InstutionRequeiredMessage="Please Choose Instution";

    public static final String UserTypeIdErrorMessage ="Error getting TypeId";
    public static final String UserListErrorMessage ="Error getting UserList";
    public static final String TitleErrorMessage ="Error getting title";
    public static final String CountryErrorMessage ="Error getting Country";
    public static final String StateErrorMessage ="Error getting State";
    public static final String CityErrorMessage ="Error getting City";
    public static final String AirFormErrorMessage ="Error getting AirForm";
    public static final String AirFormCategoryErrorMessage ="Error getting Main Performance Category";
    public static final String AirFormGradeErrorMessage ="Error getting AIR Accreditation Grade";
    public static final String MemberErrorMessage ="Error getting Member";
    public static final String VenderOpionErrorMessage ="Error getting VenderOption";
    public static final String OrganizationErrorMessage ="Error getting VenderOption";

    public static final String ErrorRegisterOTP ="Error Register OTP";
    public static final String ErrorVerifyOTP ="Error verify OTP";

    public static final String OrganizationHeading ="Organization";
    public static final String TitleHeading ="Title";
    public static final String CountryHeading ="Country";
    public static final String StateHeading ="State";
    public static final String CityHeading ="City";
    public static final String AirFormHeading ="AirForm";
    public static final String AirFormCategoryHeading ="Main Performance Category";
    public static final String AirFormTypeHeading ="Main Performance Type";
    public static final String AirFormGradeHeading ="AIR Accreditation Grade";
    public static final String AdditionalAirFormCategoryHeading ="Additional Performance Category";
    public static final String AdditionalAirFormTypeHeading ="Additional Performance Type";
    public static final String AdditionalAirFormGradeHeading ="AIR Accreditation Grade";
    public static final String WhoWeare ="Who we are?";

   // Additional
    /*Drawbox option values*/

    public static String userModuleSync;

    public static String orginizationRoleId=null;

    public static String titleId=null;
    public static String countryId=null;
    public static String stateId=null;
    public static String cityId=null;
    public static String airFormId=null;
    public static String airFormCategoryId=null;
    public static String airFormTypeId=null;
    public static String airAcceretionId=null;
    public static String additionalAirFormCategoryId=null;
    public static String addtionalAirFormTypeId=null;
    public static String addtionalAirAcceretionId=null;
    public static String radiogroup_value="1";
    public static String memberShipId=null;
    public static boolean memberShopAvailablelity=false;


    public static class Camara{
        public static final int FROM_LIBRARY = 1010;
        public static final int FROM_CAMERA = 1115;
        public static final int CROP_PIC = 2;
        public static final int ACTION_REQUEST_CROP = 100;
        public static final int IMAGE_MAX_SIZE = 512;
        public static  String profileImagePath ;
        public static Bitmap bitmap ;
    }

}



