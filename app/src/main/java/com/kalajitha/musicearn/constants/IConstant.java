package com.kalajitha.musicearn.constants;

public interface IConstant extends SharedPrefrenceInterface {
    /**
     * ACRA Crash Report Mail Id
     */
    String MailId = "cse406@gmail.com,"/* +
            "tamilselvan@twilightsoftwares.com," +
            "selva@twilightsoftwares.com"*/;


    /**
     * AlertDialog TitleHeading
     */
    String SignUp = "Signup";
    String Login = "Login";
    String SplashScreen = "SplashScreen";
    String Logout = "Logout";
    String ForgotPassword = "Forgot Password";
    String LogoutMessage = "Do you wish to logout from the app?";


    String Id = "Id";
    String UserName = "UserName";
    String EmailId = "EmailId";
    String Password = "Password";
    String DateOfBirth = "DateOfBirth";
    String Gender = "Gender";
    String City = "City";
    String State = "State";
    String Country = "CountryHeading";
    String Zip = "Zip";
    String ProfImg = "ProfImg";
    String ProfImgpath = "ProfImgpath";
    String AuthToken = "AuthToken";
    String UserId = "UserId";
    String CoverImge = "CoverImg";
    String CurPass = "CurrentPassword";
    String NewPass = "NewPassword";





}

