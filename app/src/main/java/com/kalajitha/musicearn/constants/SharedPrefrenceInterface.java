package com.kalajitha.musicearn.constants;

/**
 * Created by jaya on 3/19/2016.
 */
public interface SharedPrefrenceInterface {

    /**
     * SharedPreference
     */
    String SharedPref_Title = "MusicEarn";

    String SharedPref_UserModule_ContectPersonID="_SharedPref_UserModule_ContectPersonID";
    String SharedPref_UserModule_ContectPersonEditableID="_SharedPref_UserModule_ContectEditableID";

    String SharedPref_LoginedUserid ="_SharedPref_Userid"; /*logined userId it will be unique id*/
    String SharedPref_UserTypeId="_SharedPref_UserTypeId"; /*Logined UserTypeId  will be different based on the type ie Musiciearn or dancer */

/*To verify OTP we are saving email id and mobile number */
    String SharedPref_OTPVERIFY_EMAIL ="_SharedPref_email";
    String SharedPref_OTPVERIFY_MOBILE ="_SharedPref_mobile";

   /*  *//* preferences exampl*//*
    UniversalPreferences.getInstance().put("string", "some value");
    String string =  UniversalPreferences.getInstance().get("string", "");
    Log.d("String", string);

    // boolean
    UniversalPreferences.getInstance().put("bool", true);
    boolean bool = UniversalPreferences.getInstance().get("bool", false);
    Log.d("boolean", String.valueOf(bool));

    // int
    UniversalPreferences.getInstance().put("int", 30);
    int value =  UniversalPreferences.getInstance().get("int", 0);
    Log.d("int", String.valueOf(value));

    // float
    UniversalPreferences.getInstance().put("float", 3.0f);
    float valueFloat = UniversalPreferences.getInstance().get("float", 0.0f);
    Log.d("float", String.valueOf(valueFloat));

    // Set<String>
    Set<String> set = new HashSet<String>();
    set.add("test 1");
    set.add("test 2");
    UniversalPreferences.getInstance().put("set", set);
    Set<String> savedSet = UniversalPreferences.getInstance().get("set", new HashSet<>());
    for (String s : savedSet) {
        Log.d("Set", String.valueOf(s));
    }

    //This will get default value, since there is no value with this key
    int noValue =  UniversalPreferences.getInstance().get("novalue", 0);
    Log.d("int", String.valueOf(noValue));

    //UniversalPreferences.getInstance().remove("key"); //remove value with it's corresponding key
    //UniversalPreferences.getInstance().clear(); //clear all data on SharedPreferences*/


}
