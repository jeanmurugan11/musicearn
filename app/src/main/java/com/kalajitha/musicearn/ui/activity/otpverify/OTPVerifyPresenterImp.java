package com.kalajitha.musicearn.ui.activity.otpverify;

import android.text.TextUtils;

import com.kalajitha.musicearn.constants.Constants;
import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.OTPVerifyResponse;
import com.kalajitha.musicearn.utility.Validator;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by supercomputer on 18/7/17.
 */

public class OTPVerifyPresenterImp implements IOTPVerifyMVP.IOTPVerifyPresenter {

    private IOTPVerifyMVP.IOTPVerifyView view;

    private IOTPVerifyMVP.IOTPVerifyModel modelImp;

    private Subscription  subscription=null;

    public OTPVerifyPresenterImp(IOTPVerifyMVP.IOTPVerifyModel modelImp) {
        this.modelImp = modelImp;
    }

    @Override
    public void setView(IOTPVerifyMVP.IOTPVerifyView view) {
        this.view=view;
    }

    @Override
    public void rxUnsubscribe() {
        if(subscription !=null){
            if(!subscription.isUnsubscribed()){
                subscription.unsubscribe();
            }
        }
    }

    @Override
    public void registerButtonClicked(Outer.OTPVerifyRequest otpRequest) {

        if(view !=null) {
            if (TextUtils.isEmpty(otpRequest.getOTP())) {
                view.showInputError("Need OTP");
            } else if (TextUtils.isEmpty(otpRequest.getEmailId())) {
                view.showInputError("Please enter a valid email id");
            } else if (!Validator.isEmailValid(otpRequest.getEmailId())) {
                view.showInputError("Please enter a valid email id");
            } else if (TextUtils.isEmpty(otpRequest.getMobileNumber()) || otpRequest.getMobileNumber().length() < 10) {
                view.showInputError("Your phone number should have minimum 10 number");
            }else if (TextUtils.isEmpty(otpRequest.getPassword())) {
                view.showInputError("Please enter your password");
            } else if (TextUtils.isEmpty(otpRequest.getConfirmPassword())) {
                view.showInputError("Please enter your confirmpassword");
            }else  {
                view.showLoader();
                subscription = modelImp.verifyOTP(otpRequest)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<OTPVerifyResponse>() {
                            @Override
                            public void onCompleted() {
                                view.closeLoader();
                            }
                            @Override
                            public void onError(Throwable e) {
                                view.closeLoader();
                                view.showInputError(Constants.ErrorVerifyOTP);
                            }
                            @Override
                            public void onNext(OTPVerifyResponse otpVerifyResponse) {

                                view.onNavigationToVerification(otpVerifyResponse);
                            }
                        });
            }
        }

    }

    @Override
    public void showInputError(String internetConnection_fails) {

        if(view!=null){
            view.showInputError(internetConnection_fails);
        }
    }
}
