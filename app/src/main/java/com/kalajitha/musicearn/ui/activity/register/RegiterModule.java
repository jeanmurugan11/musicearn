package com.kalajitha.musicearn.ui.activity.register;

import com.kalajitha.musicearn.network.loginmodule.ILoginModuleAPIService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by supercomputer on 17/7/17.
 */

@Module
public class RegiterModule {

    @Provides
    public IRegisterMVP.IRegisterPresenter provideRegisterPresenter(IRegisterMVP.IRegisterModel model){
        return new RegisterPresenderImp(model);
    }

    @Provides
    public IRegisterMVP.IRegisterModel provdeRegisterModel(IRegisterRepository repository){
        return new RegisterModelImp(repository);
    }

    @Singleton
    @Provides
    public IRegisterRepository provideRepo(ILoginModuleAPIService apiServiceInterface){
        return new RegisterRepositoryImp(apiServiceInterface);
    }
}
