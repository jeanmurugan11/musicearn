package com.kalajitha.musicearn.ui.activity.register;

import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.OTPRegistrationResponse;
import com.kalajitha.musicearn.model.response.UserType;

import java.util.List;

import rx.Observable;

/**
 * Created by supercomputer on 17/7/17.
 */

public class RegisterModelImp implements IRegisterMVP.IRegisterModel {

    IRegisterRepository repository;

    public RegisterModelImp(IRegisterRepository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<List<UserType>> getUserType() {
        return repository.getUserTypeList();
    }

    @Override
    public Observable<OTPRegistrationResponse> registerOTP(Outer.OTPRequest otpRequest) {

        return  repository.getOTPFromNetwork(otpRequest);
    }
}
