package com.kalajitha.musicearn.ui.activity.login;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.kalajitha.musicearn.constants.IConstant;
import com.kalajitha.musicearn.network.IConstantsV2;
import com.kalajitha.musicearn.objects.UniversalPreferences;
import com.kalajitha.musicearn.root.MusicEarnApp;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;


public class LoginSreenModelImp implements ILoginMVP.Model {

    public static final String TAG=LoginSreenModelImp.class.getSimpleName();

    private  String userName;
    private  String passWord;
    OnLoginFinishedListener listener;

    @Override
    public void login(String username, String password, OnLoginFinishedListener listener) {
        this.userName=username;
        this.passWord=password;
        this.listener=listener;
        String loginData="grant_type=password&username="+userName+"&password="+passWord;
        new Login(loginData).execute();
    }

    private class Login extends AsyncTask<Void, Void, Boolean> {
        String loginData;
        int responseCode;
        private Login(String data) {
            this.loginData=data;
        }

        @Override
        protected void onPreExecute() {
            Log.e(TAG, "1 - RequestVoteTask is about to start...");
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            boolean status = false;
            String response = "";
            Log.e(TAG, "2 - pre Request to response...");
            try {
                response = performPostCall();
                Log.e(TAG, "3 - give Response...");
                Log.e(TAG, "4 " + response);

            } catch (Exception e) {
                // displayLoding(false);
                Log.e(TAG, "Error ...");
            }
            Log.e(TAG, "5 - after Response...");
            if (!response.equalsIgnoreCase("")) {

                try {
                    Log.e(TAG, "6 - response !empty...");
                    JSONObject jRoot = new JSONObject(response);
                    Log.e("Result", jRoot + "");
                    if(responseCode==200){
                        String access_token = jRoot.getString("access_token");
                        Log.e("access_token",access_token);
                    }
                    status = true;

                } catch (JSONException e) {
                    // displayLoding(false);
                    // e.printStackTrace();
                    Log.e(TAG, "Error " + e.getMessage());
                }
            } else {
                Log.e(TAG, "6 - response is empty...");

                status = false;
            }

            return status;
        }

        @Override
        protected void onPostExecute(Boolean result) {

            Log.e(TAG, "7 - onPostExecute ...");

            if(responseCode==200){
                if(userName!=null){
                    UniversalPreferences.getInstance().put(IConstant.SharedPref_LoginedUserid, userName);
                    String string =  UniversalPreferences.getInstance().get(IConstant.SharedPref_LoginedUserid, "");
                    Log.d("String", string);
                    listener.onSuccess();
                }
            }else if(responseCode==400 || responseCode==500){
                listener.closeLoader();
                Toast.makeText(MusicEarnApp.getAppContext(),"The user name or password is incorrect.",
                        Toast.LENGTH_SHORT).show();
            }

            /*if (result) {
                Log.e(TAG, "8 - Update UI ...");
                // setUpdateUI(adv);
            } else {
                Log.e(TAG, "8 - Finish ...");

                // displayLoding(false);
                // finish();
            }*/

        }

        private String performPostCall() {

            String URL1 = IConstantsV2.END_POINTS+IConstantsV2.LOGIN;

            URL url;

            String response = "";

            try {
                url = new URL(URL1);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                //conn.setReadTimeout(context.getResources().getInteger(R.integer.maximum_timeout_to_server));
                //conn.setConnectTimeout(context.getResources().getInteger(R.integer.maximum_timeout_to_server));
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "Text");
                Log.e(TAG, "11 - url : " + URL1);
                byte[] outputBytes = loginData.getBytes("UTF-8");
                OutputStream os = conn.getOutputStream();
                os.write(outputBytes);
                responseCode = conn.getResponseCode();
                Log.e(TAG, "13 - responseCode : " + responseCode);
                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    Log.e(TAG, "14 - HTTP_OK");
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            conn.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        response += line;
                    }
                } else {
                    Log.e(TAG, "14 - False - HTTP_OK");
                    response = "";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }


}
