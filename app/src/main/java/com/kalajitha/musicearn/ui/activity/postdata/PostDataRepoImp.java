package com.kalajitha.musicearn.ui.activity.postdata;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.kalajitha.musicearn.constants.IConstant;
import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.AirFormCategoryList;
import com.kalajitha.musicearn.model.response.AirFormGreduList;
import com.kalajitha.musicearn.model.response.AirFormList;
import com.kalajitha.musicearn.model.response.AirFormTypeList;
import com.kalajitha.musicearn.model.response.CityList;
import com.kalajitha.musicearn.model.response.CountryList;
import com.kalajitha.musicearn.model.response.OrginzationAndInstution;
import com.kalajitha.musicearn.model.response.StateList;
import com.kalajitha.musicearn.model.response.TitleList;
import com.kalajitha.musicearn.network.APIServiceInterface;
import com.kalajitha.musicearn.network.IConstantsV2;
import com.kalajitha.musicearn.network.volleymultiform.VolleyMultipartRequest;
import com.kalajitha.musicearn.network.volleymultiform.VolleySingleton;
import com.kalajitha.musicearn.objects.UniversalPreferences;
import com.kalajitha.musicearn.root.MusicEarnApp;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.functions.Action1;

public class PostDataRepoImp implements IPostDataRepo {

    APIServiceInterface apiServiceInterface;
    private List<TitleList> titleLists;
    private List<CountryList> countryLists;
    private List<StateList> stateLists;
    private List<CityList> cityLists;
    private List<AirFormList> airFormLists;
    private List<AirFormCategoryList> airFormCategoryLists;
    private List<AirFormTypeList> airFormTypeLists;
    private List<AirFormGreduList> airFormGreduLists;
    private List<OrginzationAndInstution> orginzationAndInstutions;
    private long timestamp;
    private static final long STALE_MS = 20 * 10000; // Data is stale after 20minut

    public PostDataRepoImp(APIServiceInterface apiServiceInterface) {
        this.apiServiceInterface = apiServiceInterface;
        titleLists = new ArrayList<>();
        countryLists = new ArrayList<>();
        stateLists = new ArrayList<>();
        cityLists = new ArrayList<>();
        airFormLists = new ArrayList<>();
        airFormCategoryLists = new ArrayList<>();
        airFormTypeLists = new ArrayList<>();
        airFormGreduLists = new ArrayList<>();
        orginzationAndInstutions = new ArrayList<>();
    }

    public boolean isUpToDate() {
        return System.currentTimeMillis() - timestamp < STALE_MS;
    }

    @Override
    public Observable<List<OrginzationAndInstution>> getOrginzationAndInstutionFromMemory() {
        if (isUpToDate() && orginzationAndInstutions.size() > 0) {
            return Observable.just(orginzationAndInstutions);
        } else {
            timestamp = System.currentTimeMillis();
            orginzationAndInstutions.clear();
            return Observable.empty();
        }

    }

    @Override
    public Observable<List<OrginzationAndInstution>> getOrginzationAndInstutionFromNetwork() {
        Observable<List<OrginzationAndInstution>> topRatedObservable = apiServiceInterface.getOrganization("");
        return topRatedObservable.doOnNext(new Action1<List<OrginzationAndInstution>>() {
            @Override
            public void call(List<OrginzationAndInstution> orginzationAndInstution) {
                orginzationAndInstutions = orginzationAndInstution;
            }
        });
    }

    @Override
    public Observable<List<OrginzationAndInstution>> getOrginzationAndInstutionData() {
        return getOrginzationAndInstutionFromMemory().switchIfEmpty(getOrginzationAndInstutionFromNetwork());
    }

    @Override
    public Observable<List<TitleList>> getTitleFromMemory() {

        if (isUpToDate() && titleLists.size() > 0) {
            return Observable.just(titleLists);
        } else {
            timestamp = System.currentTimeMillis();
            titleLists.clear();
            return Observable.empty();
        }

    }

    @Override
    public Observable<List<TitleList>> getTitleFromNetwork() {
        Observable<List<TitleList>> topRatedObservable = apiServiceInterface.getTitleList("");
        return topRatedObservable.doOnNext(new Action1<List<TitleList>>() {
            @Override
            public void call(List<TitleList> titleList) {
                titleLists = titleList;
            }
        });
    }

    @Override
    public Observable<List<TitleList>> getTitleData() {
        return getTitleFromMemory().switchIfEmpty(getTitleFromNetwork());
    }

    @Override
    public Observable<List<CountryList>> getcountryFromMemory() {

        if (isUpToDate() && countryLists.size() > 0) {
            return Observable.just(countryLists);
        } else {
            timestamp = System.currentTimeMillis();
            countryLists.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<List<CountryList>> getcountryFromNetwork() {
        Observable<List<CountryList>> topRatedObservable = apiServiceInterface.getCountryList("");
        return topRatedObservable.doOnNext(new Action1<List<CountryList>>() {
            @Override
            public void call(List<CountryList> countryList) {
                countryLists = countryList;
            }
        });
    }

    @Override
    public Observable<List<CountryList>> getcountryData() {
        return getcountryFromMemory().switchIfEmpty(getcountryFromNetwork());
    }

    @Override
    public Observable<List<StateList>> getStateFromMemory() {

        if (isUpToDate() && stateLists.size() > 0) {
            return Observable.just(stateLists);
        } else {
            timestamp = System.currentTimeMillis();
            stateLists.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<List<StateList>> getStateFromNetwork(Outer.GetStateRequest myOuter2) {
        Observable<List<StateList>> topRatedObservable;
        topRatedObservable = apiServiceInterface.getStateList(myOuter2);
        return topRatedObservable.doOnNext(new Action1<List<StateList>>() {
            @Override
            public void call(List<StateList> stateList) {
                stateLists = stateList;
            }
        });
    }

    @Override
    public Observable<List<StateList>> getStateData(Outer.GetStateRequest myOuter2) {

        return getStateFromMemory().switchIfEmpty(getStateFromNetwork(myOuter2));
    }

    @Override
    public Observable<List<CityList>> getCityFromMemory() {
        if (isUpToDate() && cityLists.size() > 0) {
            return Observable.just(cityLists);
        } else {
            timestamp = System.currentTimeMillis();
            cityLists.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<List<CityList>> getCityFromNetwork(Outer.GetCityRequest myOuter2) {
        Observable<List<CityList>> topRatedObservable;
        topRatedObservable = apiServiceInterface.getCityList(myOuter2);
        return topRatedObservable.doOnNext(new Action1<List<CityList>>() {
            @Override
            public void call(List<CityList> cityList) {
                cityLists = cityList;
            }
        });
    }

    @Override
    public Observable<List<CityList>> getcityData(Outer.GetCityRequest myOuter2) {
        return getCityFromMemory().switchIfEmpty(getCityFromNetwork(myOuter2));
    }

    @Override
    public Observable<List<AirFormList>> getAirFormMemory() {
        if (isUpToDate() && airFormLists.size() > 0) {
            return Observable.just(airFormLists);
        } else {
            timestamp = System.currentTimeMillis();
            airFormLists.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<List<AirFormList>> getAirFromNetwork() {
        Observable<List<AirFormList>> topRatedObservable = apiServiceInterface.getArtFormList("");
        return topRatedObservable.doOnNext(new Action1<List<AirFormList>>() {
            @Override
            public void call(List<AirFormList> airFormList) {
                airFormLists = airFormList;
            }
        });
    }

    @Override
    public Observable<List<AirFormList>> getAirFormData() {
        return getAirFormMemory().switchIfEmpty(getAirFromNetwork());
    }

    @Override
    public Observable<List<AirFormCategoryList>> getAirFormCategoryFromMemory() {
        if (isUpToDate() && airFormCategoryLists.size() > 0) {
            return Observable.just(airFormCategoryLists);
        } else {
            timestamp = System.currentTimeMillis();
            airFormCategoryLists.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<List<AirFormCategoryList>> getAirFormCategoryFromNewwork(Outer.GetArtFormCategory myOuter2) {
        Observable<List<AirFormCategoryList>> topRatedObservable;
        topRatedObservable = apiServiceInterface.getArtFormCatecory(myOuter2);
        return topRatedObservable.doOnNext(new Action1<List<AirFormCategoryList>>() {
            @Override
            public void call(List<AirFormCategoryList> airFormCategoryList) {
                airFormCategoryLists = airFormCategoryList;
            }
        });
    }

    @Override
    public Observable<List<AirFormCategoryList>> getAirFormCategoryData(Outer.GetArtFormCategory myOuter2) {
        return getAirFormCategoryFromMemory().switchIfEmpty(getAirFormCategoryFromNewwork(myOuter2));
    }


    @Override
    public Observable<List<AirFormTypeList>> getAirFormTypeFromMemory() {
        if (isUpToDate() && airFormTypeLists.size() > 0) {
            return Observable.just(airFormTypeLists);
        } else {
            timestamp = System.currentTimeMillis();
            airFormTypeLists.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<List<AirFormTypeList>> getAirFormTypeFromNetwork(Outer.GetArtFormType myOuter2) {
        Observable<List<AirFormTypeList>> topRatedObservable;
        topRatedObservable = apiServiceInterface.getArtFomType(myOuter2);
        return topRatedObservable.doOnNext(new Action1<List<AirFormTypeList>>() {
            @Override
            public void call(List<AirFormTypeList> airFormTypeList) {
                airFormTypeLists = airFormTypeList;
            }
        });
    }

    @Override
    public Observable<List<AirFormTypeList>> getAirFormTypeData(Outer.GetArtFormType myOuter2) {
        return getAirFormTypeFromMemory().switchIfEmpty(getAirFormTypeFromNetwork(myOuter2));
    }


    @Override
    public Observable<List<AirFormGreduList>> getairFormGraduFromMemory() {
        if (isUpToDate() && airFormGreduLists.size() > 0) {
            return Observable.just(airFormGreduLists);
        } else {
            timestamp = System.currentTimeMillis();
            airFormGreduLists.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<List<AirFormGreduList>> getairFormGraduNetwork() {
        Observable<List<AirFormGreduList>> topRatedObservable = apiServiceInterface.getGredu("");
        return topRatedObservable.doOnNext(new Action1<List<AirFormGreduList>>() {
            @Override
            public void call(List<AirFormGreduList> airFormGreduList) {
                airFormGreduLists = airFormGreduList;
            }
        });
    }

    @Override
    public Observable<List<AirFormGreduList>> getairFormGraduData() {
        return getairFormGraduFromMemory().switchIfEmpty(getairFormGraduNetwork());
    }

    private String resultResponse = null;
    @Override
    public void savePostData(final Context context, final Outer.SavePostData saveUser) {

        MusicEarnApp.showProgress(context);

        String url=IConstantsV2.END_POINTS+IConstantsV2.POST_REGISTRATION_DATA;
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    MusicEarnApp.closeProgress();
                    resultResponse = new String(response.data);
                    try {
                        JSONObject jsonObject = new JSONObject(resultResponse);
                        String resultResponse = jsonObject.getString("message");
                        Toast.makeText(context, ""+resultResponse, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MusicEarnApp.closeProgress();
                NetworkResponse networkResponse = error.networkResponse;
                resultResponse = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        resultResponse = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        resultResponse = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    Log.e("Error Message", result + networkResponse.statusCode);
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        resultResponse = jsonObject.getString("message");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                   /* if (networkResponse.statusCode == 404) {
                        resultResponse = "Resource not found";
                    } else if (networkResponse.statusCode == 401) {
                        resultResponse = result + " Please login again";
                    } else if (networkResponse.statusCode == 400) {
                        resultResponse = result + " Check your inputs";
                    } else if (networkResponse.statusCode == 500) {
                        resultResponse = result + " Something is getting wrong";
                    }*/
                }
                try {
                    Toast.makeText(context, ""+resultResponse, Toast.LENGTH_SHORT).show();
                    Log.i("Error", resultResponse);
                    error.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                try {

                    //String userId = UniversalPreferences.getInstance().get(IConstant.SharedPref_LoginedUserid, "");
                    int value =  UniversalPreferences.getInstance().get(IConstant.SharedPref_UserTypeId, 0);
                    if (1==value || 3==value){

                        params.put("userName", saveUser.getUserLoginedId());
                        if(1==value) {
                            params.put("organisationName", saveUser.getOrganisationName());
                            params.put("organisationRoleId", saveUser.getOrganisationRoleId());
                        }else{
                            params.put("institutionName", saveUser.getInstitutionName());
                            params.put("institutionRoleId", saveUser.getInstitutionRoleId());
                        }

                        params.put("firstName", saveUser.getfName());
                        //params.put("middleName", saveUser.getmName());
                        params.put("lastName", saveUser.getlName());
                        //params.put("mobileNumber",saveUser.getMobileNumber());
                        //params.put("emailId", saveUser.getEmailId());
                        params.put("countryId", saveUser.getCountryId());
                        params.put("stateId", saveUser.getStateId());
                        params.put("cityId", saveUser.getCityId());
                        params.put("gender", saveUser.getRadiogroup_value());
                    }else{
                        params.put("prefixName", saveUser.getPrefixName());
                        params.put("firstName", saveUser.getfName());
                        params.put("lastName", saveUser.getlName());
                        params.put("titleId", saveUser.getTitleId());
                        params.put("countryId", saveUser.getCountryId());
                        params.put("stateId", saveUser.getStateId());
                        params.put("cityId", saveUser.getCityId());
                        params.put("gender", saveUser.getRadiogroup_value());
                        params.put("airFormId", saveUser.getAirFormId());
                        params.put("mainCategoryId", saveUser.getAirFormCategoryId());
                        params.put("mainPerformanceCategoryId", saveUser.getAirFormTypeId());
                        params.put("mainPerformanceOthers", "test others data");
                        params.put("mainAIRAccreditationGradeId", saveUser.getAirAcceretionId());
                    }

                    System.out.println("using entrySet and toString");

                    for (Map.Entry<String, String> entry : params.entrySet()) {
                        System.out.println(entry);
                    }
                    System.out.println();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                byte[] photoimageByte = new byte[0];
                try {
                    if(saveUser.getBitmap()!= null){
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        saveUser.getBitmap().compress(Bitmap.CompressFormat.JPEG, 100, baos);
                        photoimageByte = baos.toByteArray();
                        params.put("profileImage", new DataPart(saveUser.getProfileImagePath(), photoimageByte, "image/jpeg"));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return params;
            }
        };
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(context).addToRequestQueue(multipartRequest);


    }
}
