package com.kalajitha.musicearn.ui.activity.register;

import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.OTPRegistrationResponse;
import com.kalajitha.musicearn.model.response.UserType;

import java.util.List;

import rx.Observable;

public interface IRegisterRepository {

    /*Userlist Repository*/
    Observable<List<UserType>> getUserTypeFromMomory();
    Observable<List<UserType>> getUserTypeFromNatwork();
    Observable<List<UserType>> getUserTypeList();

    /*GetOTP*/
    Observable<OTPRegistrationResponse> getOTPFromNetwork(Outer.OTPRequest otpRequest);
}
