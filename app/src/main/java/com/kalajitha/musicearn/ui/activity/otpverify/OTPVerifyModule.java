package com.kalajitha.musicearn.ui.activity.otpverify;


import com.kalajitha.musicearn.network.loginmodule.ILoginModuleAPIService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class OTPVerifyModule {
    @Provides
    public IOTPVerifyMVP.IOTPVerifyPresenter provideOTPVerifyPresenter(IOTPVerifyMVP.IOTPVerifyModel model){
        return new OTPVerifyPresenterImp(model);
    }
    @Provides
    public IOTPVerifyMVP.IOTPVerifyModel provideOTPVerfiyModel(IOTPVerifyRepository iotpVerifyRepository){
        return new OTPVerifyModelImp(iotpVerifyRepository);
    }
    @Singleton
    @Provides
    public IOTPVerifyRepository provideOTPRepository(ILoginModuleAPIService iLoginModuleAPIService){
        return new OTPVerifyRepositoryImp(iLoginModuleAPIService);
    }
}

