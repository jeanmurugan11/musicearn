package com.kalajitha.musicearn.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.text.Html.ImageGetter;
import android.text.Spanned;
import android.widget.ImageView;
import android.widget.TextView;

import com.kalajitha.musicearn.R;

import java.util.ArrayList;
import java.util.List;

public class TestImageGetter extends Activity implements ImageGetter {
    private final static String TAG = "TestImageGetter";
    private TextView mTv;
    ImageView imageView2;
    TextView textlast;
    String test;
    Context mContext;
    Spanned spanned;

    List<String> src = new ArrayList<>();
    List<String> string = new ArrayList<>();
    List<String> images = new ArrayList<>();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_image_getter);
        String source = "Hi friends i am murugan ....<img alt=\"\" src=\"http://45.113.189.118/MuAPI/Repository/DetailedProfileUploadedFiles/89646C1D/DBAAD200-DE91-4207-B54E-6DA295F5B825.png\" width=\"500\" />";
        String source1 = "Hi friends i am murugan ....<img alt=\"\" src=\"http://45.113.189.118/MuAPI/Repository/DetailedProfileUploadedFiles/89646C1D/DBAAD200-DE91-4207-B54E-6DA295F5B825.png\" width=\"500\" />";
        String source2 = "Hi friends i am murugan ....<img alt=\"\" src=\"http://45.113.189.118/MuAPI/Repository/DetailedProfileUploadedFiles/89646C1D/DBAAD200-DE91-4207-B54E-6DA295F5B825.png\" width=\"500\" />";
        String source3 = "Hi friends i am murugan ....<img alt=\"\" src=\"http://45.113.189.118/MuAPI/Repository/DetailedProfileUploadedFiles/89646C1D/DBAAD200-DE91-4207-B54E-6DA295F5B825.png\" width=\"500\" />";
        String source4 = "<img alt=\"\" src=\"http://45.113.189.118/MuAPI/Repository/DetailedProfileUploadedFiles/89646C1D/DBAAD200-DE91-4207-B54E-6DA295F5B825.png\" width=\"500\" />Hi friends how are you&nbsp;";
        String source5 = "<p style=\"margin-left:30px;\">HI this is test message&nbsp;</p><a href=\"http://google.com\">http://google.com</a><img alt=\"\" src=\"http://45.113.189.118/MuAPI/Repository/DetailedProfileUploadedFiles/89646C1D/DBAAD200-DE91-4207-B54E-6DA295F5B825.png\" width=\"300\"/>";

        src.add(source);
        src.add(source1);
        src.add(source2);
        src.add(source3);
        src.add(source4);
        src.add(source5);

        System.out.println("main"+src.size()+"   ");
        for(String s:src){
            spanned = Html.fromHtml(s, this, null);
            string.add(spanned.toString());
        }

        System.out.println("string"+string.size()+"   ");

    }

    @Override
    public Drawable getDrawable(String source) {
       // System.out.println("jjjj->"+source);
       /* LevelListDrawable d = new LevelListDrawable();
        Drawable empty = getResources().getDrawable(R.drawable.account);
        d.addLevel(0, 0, empty);
        d.setBounds(0, 0, empty.getIntrinsicWidth(), empty.getIntrinsicHeight());*/

        //new LoadImage().execute(source, d);
        images.add(source);

        System.out.println("images"+images.size()+"   ");
       /* imageView2 = (ImageView) findViewById(R.id.imageView2);
        if( source != null && !"".equals(source) ){
            Transformation transformation = new RoundedTransformationBuilder()
                    .borderColor(Color.GRAY)
                    .borderWidthDp(1)
                    .cornerRadiusDp(30)
                    .oval(false)
                    .build();

            Picasso.with(this)
                    .load(source)
                    .fit()
                    .error(R.drawable.ic_no_user)
                    .transform(transformation)
                    .into(imageView2);

        }
        else {
            //setDefaultImage(holder.thumbnail);
        }*/

        return null;
    }

   /* private class LoadImage extends AsyncTask<Object, Void, Bitmap> {

        private LevelListDrawable mDrawable;

        @Override
        protected Bitmap doInBackground(Object... params) {
            String source = (String) params[0];
            mDrawable = (LevelListDrawable) params[1];
            Log.d(TAG, "doInBackground " + source);
            try {
                InputStream is = new URL(source).openStream();
                return BitmapFactory.decodeStream(is);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            Log.d(TAG, "onPostExecute drawable " + mDrawable);
            Log.d(TAG, "onPostExecute bitmap " + bitmap);
            if (bitmap != null) {
                BitmapDrawable d = new BitmapDrawable(bitmap);
                mDrawable.addLevel(1, 1, d);
                mDrawable.setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());
                mDrawable.setLevel(1);
                // i don't know yet a better way to refresh TextView
                // mTv.invalidate() doesn't work as expected
                CharSequence t = mTv.getText();
                mTv.setText(t);
            }
        }
    }*/

   /* public void setContentToView(String content) {

        Document doc = Jsoup.parse(content);
        Elements elements = doc.getAllElements();
        String test=elements.text();
        if(src.contains(test)){
            src.remove(test);
        }else{
            src.add(test);
        }

        System.out.println("yyyyy"+src.size()+"   ");
        for(String s:src){
            System.out.println("kkkk"+s);
        }
    }*/
}