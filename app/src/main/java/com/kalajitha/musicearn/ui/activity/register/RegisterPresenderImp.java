package com.kalajitha.musicearn.ui.activity.register;

import android.text.TextUtils;

import com.kalajitha.musicearn.constants.Constants;
import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.OTPRegistrationResponse;
import com.kalajitha.musicearn.model.response.UserType;
import com.kalajitha.musicearn.utility.Validator;

import java.util.List;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class RegisterPresenderImp implements IRegisterMVP.IRegisterPresenter {

    private IRegisterMVP.IRegisterView view;

    private IRegisterMVP.IRegisterModel model;

    private Subscription subscription=null;

    public RegisterPresenderImp(IRegisterMVP.IRegisterModel model) {
        this.model = model;
    }

    @Override
    public void setView(IRegisterMVP.IRegisterView view) {

        this.view=view;
    }

    @Override
    public void rxUnsubscribe() {

        if(subscription !=null){
            if(!subscription.isUnsubscribed()){
                subscription.unsubscribe();
            }
        }
    }

    @Override
    public void loaduserType() {
        if(view != null) {
            view.showLoader();
            subscription=model.getUserType()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<List<UserType>>() {
                        @Override
                        public void onCompleted() {
                            if(view != null) view.closeLoader();
                        }
                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            if(view!=null){
                                view.closeLoader();
                                view.showInputError(Constants.TitleErrorMessage);
                            }
                        }

                        @Override
                        public void onNext(List<UserType> userType) {

                            view.updateUserType(userType);
                        }
                    });
        }

    }

    @Override
    public void registerButtonClicked(Outer.OTPRequest otpRequest) {

        if(view !=null){
            if(TextUtils.isEmpty(otpRequest.getEmailId())){
                view.showInputError("Please enter a valid email id");
            }else if (!Validator.isEmailValid(otpRequest.getEmailId())) {
                view.showInputError("Please enter a valid email id");
            } else if (TextUtils.isEmpty(otpRequest.getMobileNumber()) || otpRequest.getMobileNumber().length() < 10) {
                view.showInputError("Your phone number should have minimum 10 number");
            }else if(otpRequest.getUserTypeId() ==null){
                view.showInputError("Need UserType");
            }else{
                view.showLoader();
                subscription=model.registerOTP(otpRequest)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<OTPRegistrationResponse>() {
                            @Override
                            public void onCompleted() {
                                if(view != null) view.closeLoader();
                            }
                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                                if(view!=null){
                                    view.closeLoader();
                                    view.showInputError(Constants.ErrorRegisterOTP);
                                }
                            }
                            @Override
                            public void onNext(OTPRegistrationResponse userResponce) {
                                view.closeLoader();
                                view.onNavigationToVerification(userResponce);
                            }
                        });
            }
        }
    }

    @Override
    public void showInputError(String internetConnection_fails) {
        if(view!=null)view.showInputError(internetConnection_fails);
    }
}
