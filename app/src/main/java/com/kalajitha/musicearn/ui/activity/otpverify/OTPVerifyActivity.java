package com.kalajitha.musicearn.ui.activity.otpverify;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.kalajitha.musicearn.R;
import com.kalajitha.musicearn.constants.Constants;
import com.kalajitha.musicearn.constants.IConstant;
import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.OTPVerifyResponse;
import com.kalajitha.musicearn.objects.UniversalPreferences;
import com.kalajitha.musicearn.root.MusicEarnApp;
import com.kalajitha.musicearn.ui.activity.login.LoginScreenActivity;
import com.kalajitha.musicearn.view.ButtonMyriadProBold;
import com.kalajitha.musicearn.view.EditViewRegular;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OTPVerifyActivity extends AppCompatActivity implements IOTPVerifyMVP.IOTPVerifyView {
    @BindView(R.id.edit_otp)
    EditViewRegular editOtp;
    @BindView(R.id.edit_password)
    EditViewRegular editPassword;
    @BindView(R.id.edit_confirm_password)
    EditViewRegular editConfirmPassword;
    @BindView(R.id.verify_otp)
    ButtonMyriadProBold verifyOtp;
    Context mContext;

    @Inject
    IOTPVerifyMVP.IOTPVerifyPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);
        ButterKnife.bind(this);
        mContext=this;
        ((MusicEarnApp)getApplication()).getComponent().inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.setView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.rxUnsubscribe();
    }

    @OnClick(R.id.verify_otp)
    public void onClickButton(){
        if(MusicEarnApp.isNetworkConnected(mContext)){
            String email="",mobile="";
             email= UniversalPreferences.getInstance().get(IConstant.SharedPref_OTPVERIFY_EMAIL,"");
             mobile= UniversalPreferences.getInstance().get(IConstant.SharedPref_OTPVERIFY_MOBILE,"");

            Outer.OTPVerifyRequest otpVerifyRequest=new Outer.OTPVerifyRequest();
            otpVerifyRequest.setOTP(editOtp.getText().toString().trim());
            otpVerifyRequest.setEmailId(email);
            otpVerifyRequest.setMobileNumber(mobile);
            otpVerifyRequest.setPassword(editPassword.getText().toString().trim());
            otpVerifyRequest.setConfirmPassword(editConfirmPassword.getText().toString().trim());
            presenter.registerButtonClicked(otpVerifyRequest);
        }else{
            presenter.showInputError(Constants.InternetConnection_Fails);
        }

    }

    @Override
    public void showInputError(String s) {

        Toast.makeText(mContext, ""+s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoader() {

        MusicEarnApp.showProgress(mContext);
    }

    @Override
    public void closeLoader() {

        MusicEarnApp.closeProgress();
    }

    @Override
    public void onNavigationToVerification(OTPVerifyResponse otpVerifyResponse) {
        Toast.makeText(mContext, ""+otpVerifyResponse.getMessage(), Toast.LENGTH_SHORT).show();
        Intent  intent=new Intent(OTPVerifyActivity.this, LoginScreenActivity.class);
        startActivity(intent);
    }


}
