package com.kalajitha.musicearn.ui.activity.postdata;

import android.content.Context;

import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.AirFormCategoryList;
import com.kalajitha.musicearn.model.response.AirFormGreduList;
import com.kalajitha.musicearn.model.response.AirFormList;
import com.kalajitha.musicearn.model.response.AirFormTypeList;
import com.kalajitha.musicearn.model.response.CityList;
import com.kalajitha.musicearn.model.response.CountryList;
import com.kalajitha.musicearn.model.response.OrginzationAndInstution;
import com.kalajitha.musicearn.model.response.StateList;
import com.kalajitha.musicearn.model.response.TitleList;

import java.util.List;

import rx.Observable;

public interface IPostDataMVP {

    interface IView{
        void showInputError(String s);
        void showLoader();
        void closeLoader();
        void updateOrganization(List<OrginzationAndInstution> orginzationAndInstutions);
        void updateTitle(List<TitleList> titleList);
        void updateCountry(List<CountryList>countryLists);
        void updateState(List<StateList>stateLists);
        void updateCity(List<CityList>cityLists);
        void updateAirForm(List<AirFormList>airFormLists);
        void updateAirFormCategory(List<AirFormCategoryList>airFormCategoryLists);
        void updateAirFormType(List<AirFormTypeList>airFormTypeLists);
        void updateAirFormGradu(List<AirFormGreduList>airFormGreduLists);
    }
    interface IPresender{
        void setView(IPostDataMVP.IView view);
        void rxUnsubscribe();
        void submitButtonClicked(Context context, Outer.SavePostData savePostData);

        void loadOrginazationAndInstution();
        void loadTitle();
        void loadCountry();
        void loadState(Outer.GetStateRequest myOuter2);
        void loadCity(Outer.GetCityRequest myOuter2);
        void loadAirForm();
        void loadAirFormCategory(Outer.GetArtFormCategory myOuter2);
        void loadAirFormType(Outer.GetArtFormType myOuter2);
        void loadAirFormGradu();

        void showInputError(String internetConnection_fails);
    }
    interface IModel{
        Observable<List<OrginzationAndInstution>> getOrginazation();
        Observable<List<TitleList>> getTitleList();
        Observable<List<CountryList>> getCountryList();
        Observable<List<StateList>> getStateList(Outer.GetStateRequest myOuter2);
        Observable<List<CityList>> getCityList(Outer.GetCityRequest myOuter2);
        Observable<List<AirFormList>> getAirFormList();
        Observable<List<AirFormCategoryList>> getAirFormCategoryList(Outer.GetArtFormCategory myOuter2);
        Observable<List<AirFormTypeList>> getAirFormTypeList(Outer.GetArtFormType myOuter2);
        Observable<List<AirFormGreduList>> getAirFormGraduList();
        void createPostData(Context context ,Outer.SavePostData saveUser);
    }
}
