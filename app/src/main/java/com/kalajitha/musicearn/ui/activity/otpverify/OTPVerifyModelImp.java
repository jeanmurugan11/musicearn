package com.kalajitha.musicearn.ui.activity.otpverify;

import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.OTPVerifyResponse;

import rx.Observable;

public class OTPVerifyModelImp implements IOTPVerifyMVP.IOTPVerifyModel {

    IOTPVerifyRepository iotpVerifyRepository;

    public OTPVerifyModelImp(IOTPVerifyRepository iotpVerifyRepository) {
        this.iotpVerifyRepository = iotpVerifyRepository;
    }

    @Override
    public Observable<OTPVerifyResponse> verifyOTP(Outer.OTPVerifyRequest otpVerifyRequest) {
        return iotpVerifyRepository.getOTPFromNetwork(otpVerifyRequest);
    }
}
