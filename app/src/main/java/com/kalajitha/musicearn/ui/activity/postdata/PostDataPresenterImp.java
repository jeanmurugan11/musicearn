package com.kalajitha.musicearn.ui.activity.postdata;

import android.content.Context;

import com.kalajitha.musicearn.constants.Constants;
import com.kalajitha.musicearn.constants.IConstant;
import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.AirFormCategoryList;
import com.kalajitha.musicearn.model.response.AirFormGreduList;
import com.kalajitha.musicearn.model.response.AirFormList;
import com.kalajitha.musicearn.model.response.AirFormTypeList;
import com.kalajitha.musicearn.model.response.CityList;
import com.kalajitha.musicearn.model.response.CountryList;
import com.kalajitha.musicearn.model.response.OrginzationAndInstution;
import com.kalajitha.musicearn.model.response.StateList;
import com.kalajitha.musicearn.model.response.TitleList;
import com.kalajitha.musicearn.objects.UniversalPreferences;

import java.util.List;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class PostDataPresenterImp implements IPostDataMVP.IPresender {

    private IPostDataMVP.IView view;
    private IPostDataMVP.IModel model;
    private Subscription  subscription;

    public PostDataPresenterImp(IPostDataMVP.IModel model) {
        this.model = model;
    }

    @Override
    public void setView(IPostDataMVP.IView view) {

        this.view=view;
    }


    @Override
    public void rxUnsubscribe() {

        if(subscription !=null){
            if(!subscription.isUnsubscribed()){
                subscription.unsubscribe();
            }
        }
    }

    @Override
    public void submitButtonClicked(Context context, Outer.SavePostData saveUser) {

        if(view != null){

            int value =  UniversalPreferences.getInstance().get(IConstant.SharedPref_UserTypeId, 0);

            if (1 == value|| 3 == value){

                if(saveUser.getfName() == null) {
                    view.showInputError(Constants.FirstNameRequeiredMessage);
                } /*else if(saveUser.getmName()== null){
                    view.showInputError(Constants.MiddleNameRequeiredMessage);
                } */else if(saveUser.getlName()== null){
                    view.showInputError(Constants.LastNameRequeiredMessage);
                } else if(saveUser.getCountryId() == null){
                    view.showInputError(Constants.CountryRequeiredMessage);
                } else if(saveUser.getStateId() == null){
                    view.showInputError(Constants.StateRequeiredMessage);
                } else if(saveUser.getCityId() == null){
                    view.showInputError(Constants.CityRequeiredMessage);
                } else{
                    if(1==value){
                        if(saveUser.getOrganisationRoleId() == null){
                            view.showInputError(Constants.OrginizationRequeiredMessage);
                        }else{
                            model.createPostData(context,saveUser);
                        }
                    }else{
                        if(saveUser.getInstitutionRoleId() == null){
                            view.showInputError(Constants.InstutionRequeiredMessage);
                        }else{
                            model.createPostData(context,saveUser);
                        }
                    }
                }
            }else {
                if(saveUser.getPrefixName()== null) {
                    view.showInputError(Constants.ProfixNameRequeiredMessage);
                }
                else if(saveUser.getTitleId() == null){
                    view.showInputError(Constants.TittleRequeiredMessage);
                }
                else if(saveUser.getfName()== null){
                    view.showInputError(Constants.FirstNameRequeiredMessage);
                }
               /* else if(saveUser.getmName()== null){
                    view.showInputError(Constants.MiddleNameRequeiredMessage);
                }*/
                else if(saveUser.getlName()== null){
                    view.showInputError(Constants.LastNameRequeiredMessage);
                }
                else if(saveUser.getCountryId() == null){
                    view.showInputError(Constants.CountryRequeiredMessage);
                }
                else if(saveUser.getStateId() == null){
                    view.showInputError(Constants.StateRequeiredMessage);
                }
                else if(saveUser.getCityId() == null){
                    view.showInputError(Constants.CityRequeiredMessage);
                }
                else if(saveUser.getAirFormId() == null){
                    view.showInputError(Constants.AirFormRequeiredMessage);
                }
                else if(saveUser.getAirFormCategoryId() == null){
                    view.showInputError(Constants.AirForCategormRequeiredMessage);
                }
                else if(saveUser.getAirFormTypeId() == null){
                    view.showInputError(Constants.AirForTypeRequeiredMessage);
                }
                else if(saveUser.getAirAcceretionId() == null){
                    view.showInputError(Constants.AirForGradeRequeiredMessage);

                }else
                    model.createPostData(context,saveUser);
                }
            }

    }

    @Override
    public void loadOrginazationAndInstution() {
        if(view != null){
            view.showLoader();
        }
        subscription=model.getOrginazation()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<OrginzationAndInstution>>() {
                    @Override
                    public void onCompleted() {
                        if(view != null) view.closeLoader();
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view!=null){
                            view.closeLoader();
                            view.showInputError(Constants.OrganizationErrorMessage);
                        }
                    }
                    @Override
                    public void onNext(List<OrginzationAndInstution> orginzationAndInstutions) {

                        view.updateOrganization(orginzationAndInstutions);
                    }
                });
    }

    @Override
    public void loadTitle() {

        if(view != null){
            view.showLoader();
        }
        subscription=model.getTitleList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<TitleList>>() {
                    @Override
                    public void onCompleted() {
                        if(view != null) view.closeLoader();
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view!=null){
                            view.closeLoader();
                            view.showInputError(Constants.TitleErrorMessage);
                        }
                    }

                    @Override
                    public void onNext(List<TitleList> titleLists) {

                        view.updateTitle(titleLists);
                    }
                });
    }

    @Override
    public void loadCountry() {

        if(view != null){
            view.showLoader();
        }
        subscription=model.getCountryList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<CountryList>>() {
                    @Override
                    public void onCompleted() {
                        if(view != null) view.closeLoader();
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view != null){
                            view.closeLoader();
                            view.showInputError(Constants.CountryErrorMessage);
                        }
                    }
                    @Override
                    public void onNext(List<CountryList> countryLists) {
                        view.updateCountry(countryLists);
                    }
                });
    }

    @Override
    public void loadState(Outer.GetStateRequest myOuter2) {

        if(view != null){
            view.showLoader();
        }
        subscription=model.getStateList(myOuter2)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<StateList>>() {
                    @Override
                    public void onCompleted() {
                        if(view != null){
                            view.closeLoader();
                        }
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view != null){
                            view.closeLoader();
                            view.showInputError(Constants.StateErrorMessage);
                        }
                    }

                    @Override
                    public void onNext(List<StateList> stateLists) {

                        view.updateState(stateLists);
                    }
                });
    }

    @Override
    public void loadCity(Outer.GetCityRequest myOuter2) {

        if(view!=null){
            view.showLoader();
        }
        subscription=model.getCityList(myOuter2)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<CityList>>() {
                    @Override
                    public void onCompleted() {
                        if(view!=null){
                            view.closeLoader();
                        }
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view!=null){
                            view.closeLoader();
                            view.showInputError(Constants.CityErrorMessage);
                        }
                    }
                    @Override
                    public void onNext(List<CityList> cityLists) {

                        view.updateCity(cityLists);
                    }
                });
    }

    @Override
    public void loadAirForm() {

        if(view!=null){
            view.showLoader();
        }
        subscription=model.getAirFormList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<AirFormList>>() {
                    @Override
                    public void onCompleted() {
                        if(view!=null){
                            view.closeLoader();
                        }
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view!=null){
                            view.closeLoader();
                            view.showInputError(Constants.AirFormErrorMessage);
                        }
                    }
                    @Override
                    public void onNext(List<AirFormList> airFormList) {

                        view.updateAirForm(airFormList);
                    }
                });
    }

    @Override
    public void loadAirFormCategory(Outer.GetArtFormCategory myOuter2) {

        if(view!=null){
            view.showLoader();
        }
        subscription=model.getAirFormCategoryList(myOuter2)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<AirFormCategoryList>>() {
                    @Override
                    public void onCompleted() {
                        if(view!=null){
                            view.closeLoader();
                        }
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view!=null){
                            view.closeLoader();
                            view.showInputError(Constants.AirFormCategoryErrorMessage);
                        }
                    }

                    @Override
                    public void onNext(List<AirFormCategoryList> airFormCategoryLists) {

                        view.updateAirFormCategory(airFormCategoryLists);
                    }
                });
    }

    @Override
    public void loadAirFormType(Outer.GetArtFormType myOuter2) {

        if(view!=null){
            view.showLoader();
        }
        subscription=model.getAirFormTypeList(myOuter2)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<AirFormTypeList>>() {
                    @Override
                    public void onCompleted() {
                        if(view!=null){
                            view.closeLoader();
                        }
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view!=null){
                            view.closeLoader();
                            view.showInputError(Constants.CityErrorMessage);
                        }
                    }
                    @Override
                    public void onNext(List<AirFormTypeList> airFormTypeLists) {

                        view.updateAirFormType(airFormTypeLists);
                    }
                });
    }

    @Override
    public void loadAirFormGradu() {
        if(view!=null){
            view.showLoader();
        }
        subscription=model.getAirFormGraduList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<AirFormGreduList>>() {
                    @Override
                    public void onCompleted() {
                        if(view!=null){
                            view.closeLoader();
                        }
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view!=null){
                            view.closeLoader();
                            view.showInputError(Constants.AirFormGradeErrorMessage);
                        }
                    }
                    @Override
                    public void onNext(List<AirFormGreduList> airFormGreduLists) {

                        view.updateAirFormGradu(airFormGreduLists);
                    }
                });
    }

    @Override
    public void showInputError(String internetConnection_fails) {
        view.showInputError(internetConnection_fails);
    }


}
