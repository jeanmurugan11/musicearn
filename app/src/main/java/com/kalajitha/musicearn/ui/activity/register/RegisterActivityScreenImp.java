package com.kalajitha.musicearn.ui.activity.register;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.kalajitha.musicearn.R;
import com.kalajitha.musicearn.constants.Constants;
import com.kalajitha.musicearn.constants.IConstant;
import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.OTPRegistrationResponse;
import com.kalajitha.musicearn.model.response.UserType;
import com.kalajitha.musicearn.objects.UniversalPreferences;
import com.kalajitha.musicearn.root.MusicEarnApp;
import com.kalajitha.musicearn.ui.activity.login.LoginScreenActivity;
import com.kalajitha.musicearn.ui.activity.otpverify.OTPVerifyActivity;
import com.kalajitha.musicearn.view.ButtonMyriadProBold;
import com.kalajitha.musicearn.view.EditViewRegular;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class RegisterActivityScreenImp extends AppCompatActivity implements IRegisterMVP.IRegisterView {

    public static final String TAG=RegisterActivityScreenImp.class.getSimpleName();

    @BindView(R.id.edit_email)
    EditViewRegular editEmail;
    @BindView(R.id.textinput_layout_email)
    TextInputLayout textinputLayoutEmail;
    @BindView(R.id.edit_number)
    EditViewRegular editNumber;
    @BindView(R.id.textinput_layout_number)
    TextInputLayout textinputLayoutNumber;
    @BindView(R.id.profile_title_edittext)
    EditViewRegular profileTitleEdittext;
    @BindView(R.id.btn_singup)
    ButtonMyriadProBold btnSingup;
    @BindView(R.id.btn_login)
    ButtonMyriadProBold btnLogin;
    Unbinder unbinder;

    @Inject
    IRegisterMVP.IRegisterPresenter iRegisterPresenter;
    Context mContext;

    String userTypeId=null;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mContext=this;
        unbinder = ButterKnife.bind(this);
        ((MusicEarnApp) getApplication()).getComponent().inject(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        //Onresure code here
        iRegisterPresenter.setView(this);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        iRegisterPresenter.rxUnsubscribe();
    }

    @OnClick({R.id.profile_title_edittext})
    public void submit(View view) {
        switch (view.getId()) {
            case R.id.profile_title_edittext:
                try {
                    if (MusicEarnApp.isNetworkConnected(mContext)) {
                        iRegisterPresenter.loaduserType();
                    } else {
                        iRegisterPresenter.showInputError(Constants.InternetConnection_Fails);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @OnClick({R.id.btn_singup,R.id.btn_login})
    public void onClickButton(ButtonMyriadProBold button){
        switch (button.getId()){
            case R.id.btn_singup:
                Outer.OTPRequest otpRequest =new Outer.OTPRequest();
                otpRequest.setEmailId(editEmail.getText().toString().trim());
                otpRequest.setMobileNumber(editNumber.getText().toString().trim());
                otpRequest.setUserTypeId(userTypeId);
                iRegisterPresenter.registerButtonClicked(otpRequest);
                break;
            case R.id.btn_login:
                Intent intent=new Intent(RegisterActivityScreenImp.this, LoginScreenActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void showInputError(String s) {

        Toast.makeText(mContext, ""+s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoader() {

        MusicEarnApp.showProgress(mContext);
    }

    @Override
    public void closeLoader() {

        MusicEarnApp.closeProgress();
    }

    @Override
    public void updateUserType(final List<UserType> userTypes) {

        try {
            ArrayAdapter<UserType> dataAdapter = new ArrayAdapter<>(mContext,
                    android.R.layout.simple_spinner_dropdown_item, userTypes);
            new AlertDialog.Builder(mContext)
                    .setTitle(Constants.WhoWeare)
                    .setCancelable(true)
                    .setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            profileTitleEdittext.setText(userTypes.get(which).getTitleName());
                            userTypeId= userTypes.get(which).getTitleId();

                        }
                    }).create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNavigationToVerification(OTPRegistrationResponse otpRegistrationResponse) {
        Toast.makeText(mContext, ""+otpRegistrationResponse.getMessage(), Toast.LENGTH_SHORT).show();
        try {
            UniversalPreferences.getInstance().put(IConstant.SharedPref_OTPVERIFY_EMAIL, editEmail.getText().toString().trim());
            UniversalPreferences.getInstance().put(IConstant.SharedPref_OTPVERIFY_MOBILE, editNumber.getText().toString().trim());
            Intent intent=new Intent(RegisterActivityScreenImp.this, OTPVerifyActivity.class);
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }

        
    }
}
