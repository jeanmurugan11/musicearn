package com.kalajitha.musicearn.ui.activity.postdata;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.kalajitha.musicearn.R;
import com.kalajitha.musicearn.constants.Constants;
import com.kalajitha.musicearn.constants.IConstant;
import com.kalajitha.musicearn.croppings.BitmapUtil;
import com.kalajitha.musicearn.croppings.CropperImageActivity;
import com.kalajitha.musicearn.croppings.ImageViewRounded;
import com.kalajitha.musicearn.croppings.RoundedShapeBitmap;
import com.kalajitha.musicearn.croppings.Utils;
import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.AirFormCategoryList;
import com.kalajitha.musicearn.model.response.AirFormGreduList;
import com.kalajitha.musicearn.model.response.AirFormList;
import com.kalajitha.musicearn.model.response.AirFormTypeList;
import com.kalajitha.musicearn.model.response.CityList;
import com.kalajitha.musicearn.model.response.CountryList;
import com.kalajitha.musicearn.model.response.OrginzationAndInstution;
import com.kalajitha.musicearn.model.response.StateList;
import com.kalajitha.musicearn.model.response.TitleList;
import com.kalajitha.musicearn.objects.UniversalPreferences;
import com.kalajitha.musicearn.root.MusicEarnApp;
import com.kalajitha.musicearn.view.ButtonMyriadProBold;
import com.kalajitha.musicearn.view.EditViewRegular;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.kalajitha.musicearn.constants.Constants.Camara.bitmap;

public class PostDataActivity extends AppCompatActivity implements IPostDataMVP.IView {
    public static final String TAG=PostDataActivity.class.getSimpleName();
    @BindView(R.id.image_profile)
    ImageViewRounded imageProfile;
    @BindView(R.id.name_musician_fragment_profix)
    EditViewRegular nameMusicianFragmentProfix;
    @BindView(R.id.profile_orginaztion_edittext)
    EditViewRegular orginaztionEdittext;
    @BindView(R.id.profile_title_edittext)
    EditViewRegular titleEdittext;
    @BindView(R.id.first_name)
    EditViewRegular firstName;
    @BindView(R.id.midile_name)
    EditViewRegular midileName;
    @BindView(R.id.last_name)
    EditViewRegular lastName;
    @BindView(R.id.mobile_number)
    EditViewRegular mobileNumber;
    @BindView(R.id.email_address)
    EditViewRegular emailAddress;
    @BindView(R.id.country_edittext)
    EditViewRegular countryEdittext;
    @BindView(R.id.state_edittext)
    EditViewRegular stateEdittext;
    @BindView(R.id.city_edittext)
    EditViewRegular cityEdittext;
    @BindView(R.id.art_form)
    EditViewRegular artForm;
    @BindView(R.id.category_edittext)
    EditViewRegular categoryEdittext;
    @BindView(R.id.type_edittext)
    EditViewRegular typeEdittext;
    @BindView(R.id.gredu_edittext)
    EditViewRegular greduEdittext;
    @BindView(R.id.male_radio)
    RadioButton maleRadio;
    @BindView(R.id.female_radio)
    RadioButton femaleRadio;
    @BindView(R.id.register_radiogroup)
    RadioGroup registerRadiogroup;
    @BindView(R.id.user_submit)
    ButtonMyriadProBold userSubmit;
    @BindView(R.id.child_linearLaout)
    LinearLayout childLinearLaout;

    @Inject
    IPostDataMVP.IPresender presenter;
    Unbinder unbinder;
    Context mContext;

    String organizationId=null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_data_activity);
        unbinder=ButterKnife.bind(this);
        ((MusicEarnApp)getApplication()).getComponent().inject(this);
        mContext=this;
        presenter.setView(this);
        clearPreference();
    }
    private void clearPreference(){
        try {
            Constants.titleId=null;
            Constants.countryId=null;
            Constants.stateId=null;
            Constants.cityId=null;

            Constants.airFormId=null;
            Constants.airFormCategoryId=null;
            Constants.airFormTypeId=null;
            Constants.airAcceretionId=null;

            Constants.additionalAirFormCategoryId=null;
            Constants.addtionalAirFormTypeId=null;
            Constants.addtionalAirAcceretionId=null;

            Constants.radiogroup_value="1";
            Constants.Camara.profileImagePath=null;
            bitmap=null;

            int value =  UniversalPreferences.getInstance().get(IConstant.SharedPref_UserTypeId, 0);

            if(1==value) {

                nameMusicianFragmentProfix.setVisibility(View.GONE);
                titleEdittext.setVisibility(View.GONE);
                artForm.setVisibility(View.GONE);
                categoryEdittext.setVisibility(View.GONE);
                typeEdittext.setVisibility(View.GONE);
                greduEdittext.setVisibility(View.GONE);
                orginaztionEdittext.setVisibility(View.VISIBLE);

            }else if(3==value){

                nameMusicianFragmentProfix.setVisibility(View.GONE);
                titleEdittext.setVisibility(View.GONE);
                artForm.setVisibility(View.GONE);
                categoryEdittext.setVisibility(View.GONE);
                typeEdittext.setVisibility(View.GONE);
                greduEdittext.setVisibility(View.GONE);
                orginaztionEdittext.setVisibility(View.VISIBLE);
                orginaztionEdittext.setHint("institution");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.setView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.rxUnsubscribe();
        unbinder.unbind();
    }

    @OnClick({R.id.user_submit})
    public void createUser(){
        if(MusicEarnApp.isNetworkConnected(mContext)){

            try {

                int value =  UniversalPreferences.getInstance().get(IConstant.SharedPref_UserTypeId, 0);
                String userLoginedId =  UniversalPreferences.getInstance().get(IConstant.SharedPref_LoginedUserid, "");

                Outer.SavePostData savePostData =new Outer.SavePostData();

                if(value == 1){ /*Organizer */

                    savePostData.setUserLoginedId(userLoginedId); /*set login Type*/

                    /*Set organization name and id*/
                    savePostData.setOrganisationName(orginaztionEdittext.getText().toString().trim());
                    savePostData.setOrganisationRoleId(organizationId);

                    savePostData.setfName(firstName.getText().toString().trim());
                    savePostData.setlName(lastName.getText().toString().trim());
                    savePostData.setRadiogroup_value(Constants.radiogroup_value);

                    savePostData.setCountryId(Constants.countryId);
                    savePostData.setStateId(Constants.stateId);
                    savePostData.setCityId(Constants.cityId);

                    /*Image name and image fiel*/
                    savePostData.setProfileImagePath(Constants.Camara.profileImagePath);
                    savePostData.setBitmap(Constants.Camara.bitmap);

                    presenter.submitButtonClicked(mContext,savePostData);

                }else if(value==3){ /*Instution*/

                    savePostData.setUserLoginedId(userLoginedId);

                    savePostData.setInstitutionName(orginaztionEdittext.getText().toString().trim());
                    savePostData.setInstitutionRoleId(organizationId);

                    savePostData.setfName(firstName.getText().toString().trim());
                    savePostData.setlName(lastName.getText().toString().trim());
                    savePostData.setRadiogroup_value(Constants.radiogroup_value);

                    savePostData.setCountryId(Constants.countryId);
                    savePostData.setStateId(Constants.stateId);
                    savePostData.setCityId(Constants.cityId);

                    savePostData.setProfileImagePath(Constants.Camara.profileImagePath);
                    savePostData.setBitmap(Constants.Camara.bitmap);

                    presenter.submitButtonClicked(mContext,savePostData);

                }else /*Musiciean, Danser,Teacher,Student*/
                savePostData.setUserLoginedId(userLoginedId);

                savePostData.setPrefixName(nameMusicianFragmentProfix.getText().toString().trim());
                savePostData.setTitleId(Constants.titleId);
                savePostData.setCountryId(Constants.countryId);
                savePostData.setStateId(Constants.stateId);
                savePostData.setCityId(Constants.cityId);

                savePostData.setAirFormId(Constants.airFormId);
                savePostData.setAirFormCategoryId(Constants.airFormCategoryId);
                savePostData.setAirFormTypeId(Constants.airFormTypeId);
                savePostData.setAirAcceretionId(Constants.airAcceretionId);

                savePostData.setRadiogroup_value(Constants.radiogroup_value);
                savePostData.setfName(firstName.getText().toString().trim());
                savePostData.setmName(midileName.getText().toString().trim());
                savePostData.setlName(lastName.getText().toString().trim());

                //savePostData.setMobileNumber(mobileNumber.getText().toString().trim());
                //savePostData.setEmailId(emailAddress.getText().toString().trim());
                savePostData.setProfileImagePath(Constants.Camara.profileImagePath);
                savePostData.setBitmap(Constants.Camara.bitmap);


                presenter.submitButtonClicked(mContext,savePostData);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            presenter.showInputError(Constants.InternetConnection_Fails);
        }
    }

    @OnClick({R.id.profile_title_edittext, R.id.country_edittext, R.id.state_edittext,
            R.id.city_edittext, R.id.art_form, R.id.category_edittext,
            R.id.type_edittext, R.id.gredu_edittext,R.id.profile_orginaztion_edittext})
    public void submit(View view) {
        switch (view.getId()) {
            case R.id.profile_title_edittext:

                try {
                    if(MusicEarnApp.isNetworkConnected(mContext)){
                        presenter.loadTitle();
                    }else{
                        presenter.showInputError(Constants.InternetConnection_Fails);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
         case R.id.profile_orginaztion_edittext:

                try {
                    if(MusicEarnApp.isNetworkConnected(mContext)){
                        presenter.loadOrginazationAndInstution();
                    }else{
                        presenter.showInputError(Constants.InternetConnection_Fails);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.country_edittext:
                try {
                    if(MusicEarnApp.isNetworkConnected(mContext)){
                        presenter.loadCountry();
                    }else{
                        presenter.showInputError(Constants.InternetConnection_Fails);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.state_edittext:
                try {
                    if(MusicEarnApp.isNetworkConnected(mContext)){
                        if(Constants.countryId !=null){
                            Outer.GetStateRequest myOuter2 = new Outer.GetStateRequest();
                            myOuter2.setCountryId(Constants.countryId);

                            presenter.loadState(myOuter2);
                        }else{
                            presenter.showInputError(Constants.CountryRequeiredMessage);
                        }
                    }else{
                        presenter.showInputError(Constants.InternetConnection_Fails);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.city_edittext:
                try {
                    if(MusicEarnApp.isNetworkConnected(mContext)){
                        if(Constants.countryId !=null && Constants.stateId !=null){
                            Outer.GetCityRequest myOuter2 = new Outer.GetCityRequest();
                            myOuter2.setCountryId(Constants.countryId);
                            myOuter2.setStateId(Constants.stateId);
                            presenter.loadCity(myOuter2);
                        }else{
                            presenter.showInputError(Constants.CountryRequeiredMessage+" "+Constants.StateRequeiredMessage);
                        }
                    }else{
                        presenter.showInputError(Constants.InternetConnection_Fails);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.art_form:
                if(MusicEarnApp.isNetworkConnected(mContext)){
                    presenter.loadAirForm();
                }else{
                    presenter.showInputError(Constants.InternetConnection_Fails);
                }
                break;
            case R.id.category_edittext:

                try {
                    if(MusicEarnApp.isNetworkConnected(mContext)){
                        if(Constants.airFormId !=null){
                            Outer.GetArtFormCategory myOuter2 = new Outer.GetArtFormCategory();
                            myOuter2.setArtFormId(Constants.airFormId);
                            presenter.loadAirFormCategory(myOuter2);
                        }else{
                            presenter.showInputError(Constants.AirFormRequeiredMessage);
                        }
                    }else{
                        presenter.showInputError(Constants.InternetConnection_Fails);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.type_edittext:
                try {
                    if(MusicEarnApp.isNetworkConnected(mContext)){
                        if(Constants.airFormId != null && Constants.airFormCategoryId != null){
                            Outer.GetArtFormType myOuter2 = new Outer.GetArtFormType();
                            myOuter2.setArtFormId(Constants.airFormId);
                            myOuter2.setArtFormCategoryId(Constants.airFormCategoryId);
                            presenter.loadAirFormType(myOuter2);
                        }else{
                            presenter.showInputError(Constants.AirFormRequeiredMessage+"&"+Constants.AirForCategormRequeiredMessage);
                        }
                    }else{
                        presenter.showInputError(Constants.InternetConnection_Fails);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.gredu_edittext:
                try {
                    if(MusicEarnApp.isNetworkConnected(mContext)){
                        presenter.loadAirFormGradu();
                    }else{
                        presenter.showInputError(Constants.InternetConnection_Fails);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;



        }

    }

    @OnClick(R.id.image_profile)
    void imageButtonOnclick() {
        try {
            selectImage();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick({R.id.male_radio, R.id.female_radio})
    public void onRadioButtonClicked(RadioButton radioButton) {

        boolean checked = radioButton.isChecked();
        // Check which radio button was clicked
        switch (radioButton.getId()) {
            case R.id.male_radio:
                if (checked) {
                    Constants.radiogroup_value = "1";
                }
                break;
            case R.id.female_radio:
                if (checked) {
                    Constants.radiogroup_value = "2";
                }
                break;
        }
    }

    @Override
    public void showInputError(String s) {

        Toast.makeText(mContext, ""+s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoader() {

        MusicEarnApp.showProgress(mContext);
    }

    @Override
    public void closeLoader() {

        MusicEarnApp.closeProgress();
    }

    @Override
    public void updateOrganization(final List<OrginzationAndInstution> orginzationAndInstutions) {
        try {
            ArrayAdapter<OrginzationAndInstution> dataAdapter;
            dataAdapter = new ArrayAdapter<>(mContext,
                    android.R.layout.simple_spinner_dropdown_item, orginzationAndInstutions);
            new AlertDialog.Builder(mContext)
                    //.setTitle(Constants.OrganizationHeading)
                    .setCancelable(true)
                    .setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            orginaztionEdittext.setText(orginzationAndInstutions.get(which).getTitleName());
                            organizationId=orginzationAndInstutions.get(which).getTitleId();

                        }
                    }).create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void updateTitle(final List<TitleList> titleList) {

        try {
            ArrayAdapter<TitleList> dataAdapter;
            dataAdapter = new ArrayAdapter<>(mContext,
                    android.R.layout.simple_spinner_dropdown_item, titleList);
            new AlertDialog.Builder(mContext)
                    .setTitle(Constants.TitleHeading)
                    .setCancelable(true)
                    .setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            titleEdittext.setText(titleList.get(which).getTitleName());
                            Constants.titleId = titleList.get(which).getTitleId();
                            System.out.println(Constants.TitleHeading+Constants.titleId);
                        }
                    }).create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateCountry(final List<CountryList> countryLists) {
        try {
            System.out.println("countryListssize"+countryLists.size());

            ArrayAdapter<CountryList> dataAdapter = new ArrayAdapter<>(mContext,
                    android.R.layout.simple_spinner_dropdown_item, countryLists);
            new AlertDialog.Builder(mContext)
                    .setTitle(Constants.TitleHeading)
                    .setCancelable(true)
                    .setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            countryEdittext.setText(countryLists.get(which).getName());
                            Constants.countryId = countryLists.get(which).getId();
                            System.out.println(Constants.CountryHeading+Constants.countryId);
                        }
                    }).create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateState(final List<StateList> stateLists) {
        try {
            ArrayAdapter<StateList> dataAdapter = new ArrayAdapter<>(mContext,
                    android.R.layout.simple_spinner_dropdown_item, stateLists);
            new AlertDialog.Builder(mContext)
                    .setTitle(Constants.StateHeading)
                    .setCancelable(true)
                    .setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            stateEdittext.setText(stateLists.get(which).getName());
                            Constants.stateId = stateLists.get(which).getId();
                            System.out.println(Constants.StateHeading+ Constants.stateId);
                        }
                    }).create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateCity(final List<CityList> cityLists) {
        try {
            ArrayAdapter<CityList> dataAdapter = new ArrayAdapter<>(mContext,
                    android.R.layout.simple_spinner_dropdown_item, cityLists);
            new AlertDialog.Builder(mContext)
                    .setTitle(Constants.CityHeading)
                    .setCancelable(true)
                    .setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            cityEdittext.setText(cityLists.get(which).getName());
                            Constants.cityId = cityLists.get(which).getId();
                            System.out.println(Constants.CityHeading+ Constants.cityId);
                        }
                    }).create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateAirForm(final List<AirFormList> airFormLists) {

        try {
            ArrayAdapter<AirFormList> dataAdapter = new ArrayAdapter<>(mContext,
                    android.R.layout.simple_spinner_dropdown_item, airFormLists);
            new AlertDialog.Builder(mContext)
                    .setTitle(Constants.AirFormHeading)
                    .setCancelable(true)
                    .setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Constants.airFormId = airFormLists.get(which).getId();
                            artForm.setText(airFormLists.get(which).getName());
                            System.out.println(Constants.AirFormHeading+Constants.airFormId);
                        }
                    }).create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateAirFormCategory(final List<AirFormCategoryList> airFormCategoryLists) {
        try {
            ArrayAdapter<AirFormCategoryList> dataAdapter = new ArrayAdapter<>(mContext,
                    android.R.layout.simple_spinner_dropdown_item, airFormCategoryLists);
            new AlertDialog.Builder(mContext)
                    .setTitle(Constants.AirFormCategoryHeading)
                    .setCancelable(true)
                    .setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            categoryEdittext.setText(airFormCategoryLists.get(which).getName());
                            Constants.airFormCategoryId = airFormCategoryLists.get(which).getId();
                            System.out.println(Constants.AirFormCategoryHeading+ Constants.airFormCategoryId);
                        }
                    }).create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateAirFormType(final List<AirFormTypeList> airFormTypeLists) {

        try {
            ArrayAdapter<AirFormTypeList> dataAdapter = new ArrayAdapter<>(mContext,
                    android.R.layout.simple_spinner_dropdown_item, airFormTypeLists);
            new AlertDialog.Builder(mContext)
                    .setTitle(Constants.AirFormTypeHeading)
                    .setCancelable(true)
                    .setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            typeEdittext.setText(airFormTypeLists.get(which).getName());
                            Constants.airFormTypeId = airFormTypeLists.get(which).getId();
                            System.out.println(Constants.AirFormTypeHeading+ Constants.airFormTypeId);
                        }
                    }).create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateAirFormGradu(final List<AirFormGreduList> airFormGreduLists) {

        try {
            ArrayAdapter<AirFormGreduList> dataAdapter = new ArrayAdapter<>(mContext,
                    android.R.layout.simple_spinner_dropdown_item, airFormGreduLists);
            new AlertDialog.Builder(mContext)
                    .setTitle(Constants.AirFormGradeHeading)
                    .setCancelable(true)
                    .setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            greduEdittext.setText(airFormGreduLists.get(which).getName());
                            Constants.airAcceretionId = airFormGreduLists.get(which).getId();
                            System.out.println(Constants.AirFormGradeHeading+ Constants.airAcceretionId);
                        }
                    }).create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*image*/
    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    initCamera();
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
                    i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                    startActivityForResult(i, Constants.Camara.FROM_LIBRARY);
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void initCamera() {
        Utils.STORAGE availableStorage = Utils.getStorageWithFreeSpace(mContext);
        String rootPath = Utils.getRootPath(mContext, availableStorage);
        File folder = new File(rootPath);
        if (!folder.isDirectory()) {
            folder.mkdir();
        }
        File fileCamera = new File(Utils.getImagePath(mContext, availableStorage, true));
        Constants.Camara.profileImagePath = fileCamera.getPath();
        Log.d("log_tag", "profileImagePath: " + Constants.Camara.profileImagePath);
        if (!fileCamera.exists())
            try {
                fileCamera.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        Uri mImageUri = Uri.fromFile(fileCamera);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
        startActivityForResult(intent, Constants.Camara.FROM_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.Camara.FROM_CAMERA && resultCode == Activity.RESULT_OK) {
            if (Constants.Camara.profileImagePath != null && !Constants.Camara.profileImagePath.equals("")) {
                startActivityForResult(new Intent(mContext, CropperImageActivity.class)
                        .putExtra("Path", Constants.Camara.profileImagePath), Constants.Camara.ACTION_REQUEST_CROP);
            }
        }
        if (requestCode == Constants.Camara.CROP_PIC && resultCode != 0) {
            Bundle extras = data.getExtras();  // Create an instance of bundle and get the returned data
            bitmap = extras.getParcelable("data");  // get the cropped bitmap from extras
            imageProfile.setImageBitmap(bitmap);  // set image bitmap to image view
            imageProfile.setImageBitmap(RoundedShapeBitmap.getRoundedShape(bitmap, 120));
        }
        if (requestCode == Constants.Camara.FROM_LIBRARY && resultCode == Activity.RESULT_OK && null != data) {
            Constants.Camara.profileImagePath = Utils.getRealPathFromURI(mContext,data.getData());
            if (Constants.Camara.profileImagePath != null && !Constants.Camara.profileImagePath.equals("")) {
                startActivityForResult(new Intent(mContext, CropperImageActivity.class)
                        .putExtra("Path", Constants.Camara.profileImagePath), Constants.Camara.ACTION_REQUEST_CROP);
            }
        }else if (requestCode == Constants.Camara.ACTION_REQUEST_CROP && resultCode == Activity.RESULT_OK) {
            String filePath = data.getStringExtra("picture_path");
            System.out.println(" filePath>>>" + filePath);
            bitmap = Utils.decodeFile(new File(filePath));
            try {
                int rotation = BitmapUtil.getExifOrientation(filePath);
                if (bitmap != null) {
                    Matrix matrix = new Matrix();
                    matrix.setRotate(rotation);
                    bitmap = Bitmap.createBitmap(bitmap, 0, 0,
                            bitmap.getWidth(), bitmap.getHeight(),
                            matrix, true);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (bitmap != null) {
                imageProfile.setImageBitmap(bitmap);
            }
        }
    }


}
