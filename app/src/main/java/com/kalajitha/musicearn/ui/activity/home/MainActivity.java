package com.kalajitha.musicearn.ui.activity.home;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

import com.kalajitha.musicearn.R;
import com.kalajitha.musicearn.fragment.UserViewPagerFragment;
import com.kalajitha.musicearn.fragment.eventcreation.EventCreationViewPagerFragment;
import com.kalajitha.musicearn.fragment.home.HomeFragment;
import com.kalajitha.musicearn.navigationliveo.HelpLiveo;
import com.kalajitha.musicearn.navigationliveo.NavigationLiveo;
import com.kalajitha.musicearn.navigationliveo.OnItemClickListener;
import com.kalajitha.musicearn.navigationliveo.OnPrepareOptionsMenuLiveo;
import com.kalajitha.musicearn.root.MusicEarnApp;


public class MainActivity extends NavigationLiveo implements OnItemClickListener {

    private HelpLiveo mHelpLiveo;


    @Override
    public void onInt(Bundle savedInstanceState) {


        // User Information
        this.userName.setText("Organizer");
        this.userType.setText("Kalai makal organization ");
        this.userLocation.setText("Bangalore");
        this.userPhoto.setImageResource(R.drawable.ic_no_user);
        // this.userBackground.setImageResource(R.drawable.ic_user_background_first);

        mHelpLiveo = new HelpLiveo();

        mHelpLiveo.add(getString(R.string.home), R.drawable.ic_home_black_48dp);

        mHelpLiveo.add(getString(R.string.profile), R.drawable.profile);

        mHelpLiveo.add(getString(R.string.createConcert), R.drawable.create);

        mHelpLiveo.add(getString(R.string.users), R.drawable.users);

        mHelpLiveo.add(getString(R.string.gallery), R.drawable.gallery);

        mHelpLiveo.add(getString(R.string.callender), R.drawable.calender);

        mHelpLiveo.add(getString(R.string.template), R.drawable.template);

        mHelpLiveo.add(getString(R.string.teaser), R.drawable.teaser);

        mHelpLiveo.add(getString(R.string.newsletter), R.drawable.news);

        mHelpLiveo.add(getString(R.string.feedback), R.drawable.feedback);

        mHelpLiveo.add(getString(R.string.logout), R.drawable.logout);


        //mHelpLiveo.addSubHeader(getString(R.string.categories)); //Item subHeader
        //{optional} - Header Customization - method customHeader
        //View mCustomHeader = getLayoutInflater().inflate(R.layout.custom_header_user, this.getListView(), false);
        //ImageView imageView = (ImageView) mCustomHeader.findViewById(R.id.imageView);

        with(this).startingPosition(0) //Starting position in the list
                .addAllHelpItem(mHelpLiveo.getHelp())
                //{optional} - List Customization "If you remove these methods and the list will take his white standard color"
                .selectorCheck(R.drawable.selector_check) //Inform the background of the selected item color
                .colorItemDefault(R.color.nliveo_black) //Inform the standard color name, icon and counter
                .colorItemSelected(R.color.nliveo_white) //State the name of the color, icon and meter when it is selected
                //.backgroundList(R.color.nliveo_black_light) //Inform the list of background color
                //.colorLineSeparator(R.color.nliveo_red_colorPrimaryDark) //Inform the color of the subheader line
                .removeFooter()
                .setOnClickUser(onClickPhoto)
                .setOnPrepareOptionsMenu(onPrepare)
                .build();

        int position = this.getCurrentPosition();
        this.setElevationToolBar(position != 2 ? 15 : 0);

        if(MusicEarnApp.getInstance().checkAndRequestPermissions(this)){
           /*permission granted  */
        }else{
            /*permission denied  */
        }
    }



    @Override
    public void onItemClick(int position) {

        // Toast.makeText(this, ""+position, Toast.LENGTH_SHORT).show();
        Fragment mFragment=null;
        FragmentManager mFragmentManager = getSupportFragmentManager();

        switch (position){
            case 0:
                mFragment =  HomeFragment.newInstance(mHelpLiveo.get(position).getName());
                break;

            case 2:
                mFragment =  EventCreationViewPagerFragment.newInstance(mHelpLiveo.get(position).getName());
                break;
            case 3:
                // closeDrawer();
                mFragment =  UserViewPagerFragment.newInstance(mHelpLiveo.get(position).getName());

                break;

        }

        if (mFragment != null){
            mFragmentManager.beginTransaction().replace(R.id.container, mFragment).commit();
        }

        setElevationToolBar(position != 1 ? 15 : 0);
    }

    /**/
    private OnPrepareOptionsMenuLiveo onPrepare = new OnPrepareOptionsMenuLiveo() {
        @Override
        public void onPrepareOptionsMenu(Menu menu, int position, boolean visible) {

            // Toast.makeText(getApplicationContext(), ""+position, Toast.LENGTH_SHORT).show();
        }
    };

    private View.OnClickListener onClickPhoto = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Toast.makeText(getApplicationContext(), "onClickPhoto :D", Toast.LENGTH_SHORT).show();
            closeDrawer();
        }
    };



}
