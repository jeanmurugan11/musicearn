package com.kalajitha.musicearn.ui.activity.postdata;

import android.content.Context;

import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.AirFormCategoryList;
import com.kalajitha.musicearn.model.response.AirFormGreduList;
import com.kalajitha.musicearn.model.response.AirFormList;
import com.kalajitha.musicearn.model.response.AirFormTypeList;
import com.kalajitha.musicearn.model.response.CityList;
import com.kalajitha.musicearn.model.response.CountryList;
import com.kalajitha.musicearn.model.response.OrginzationAndInstution;
import com.kalajitha.musicearn.model.response.StateList;
import com.kalajitha.musicearn.model.response.TitleList;

import java.util.List;

import rx.Observable;

/**
 * Created by supercomputer on 21/7/17.
 */

public class PostDataModelImp implements IPostDataMVP.IModel  {

    public PostDataModelImp(IPostDataRepo repo) {
        this.repo = repo;
    }

    IPostDataRepo repo;

    @Override
    public Observable<List<OrginzationAndInstution>> getOrginazation() {
        return repo.getOrginzationAndInstutionData();
    }

    @Override
    public Observable<List<TitleList>> getTitleList() {
        return repo.getTitleData();
    }

    @Override
    public Observable<List<CountryList>> getCountryList() {
        return repo.getcountryData();
    }

    @Override
    public Observable<List<StateList>> getStateList(Outer.GetStateRequest myOuter2) {
        return repo.getStateData(myOuter2);
    }

    @Override
    public Observable<List<CityList>> getCityList(Outer.GetCityRequest myOuter2) {
        return repo.getcityData(myOuter2);
    }

    @Override
    public Observable<List<AirFormList>> getAirFormList() {
        return repo.getAirFormData();
    }

    @Override
    public Observable<List<AirFormCategoryList>> getAirFormCategoryList(Outer.GetArtFormCategory myOuter2) {
        return repo.getAirFormCategoryData(myOuter2);
    }

    @Override
    public Observable<List<AirFormTypeList>> getAirFormTypeList(Outer.GetArtFormType myOuter2) {
        return repo.getAirFormTypeData(myOuter2);
    }

    @Override
    public Observable<List<AirFormGreduList>> getAirFormGraduList() {
        return repo.getairFormGraduData();
    }

    @Override
    public void createPostData(Context context, Outer.SavePostData saveUser) {

        repo.savePostData(context,saveUser);
    }
}
