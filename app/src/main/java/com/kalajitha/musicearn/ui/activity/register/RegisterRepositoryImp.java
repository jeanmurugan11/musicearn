package com.kalajitha.musicearn.ui.activity.register;

import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.OTPRegistrationResponse;
import com.kalajitha.musicearn.model.response.UserType;
import com.kalajitha.musicearn.network.loginmodule.ILoginModuleAPIService;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.functions.Action1;

/**
 * Created by supercomputer on 17/7/17.
 */

public class RegisterRepositoryImp implements IRegisterRepository {

    ILoginModuleAPIService iLoginModuleAPIService;

    private List<UserType> userTypes;
    private long timestamp;
    private static final long STALE_MS = 20 * 10000; // Data is stale after 20minut

    public RegisterRepositoryImp(ILoginModuleAPIService apiServiceInterface) {
        this.iLoginModuleAPIService = apiServiceInterface;
        this.timestamp = System.currentTimeMillis();
        userTypes=new ArrayList<>();
    }
    public boolean isUpToDate() {
        return System.currentTimeMillis() - timestamp < STALE_MS;
    }

    @Override
    public Observable<List<UserType>> getUserTypeFromMomory() {
        if (isUpToDate() && userTypes.size() > 0) {
            return Observable.just(userTypes);
        } else {
            timestamp = System.currentTimeMillis();
            userTypes.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<List<UserType>> getUserTypeFromNatwork() {

        Observable<List<UserType>>listObservable= iLoginModuleAPIService.getUserType("");
        return listObservable.doOnNext(new Action1<List<UserType>>() {
            @Override
            public void call(List<UserType> userType) {
                userTypes = userType;
            }
        });
    }

    @Override
    public Observable<List<UserType>> getUserTypeList() {
        return getUserTypeFromMomory().switchIfEmpty(getUserTypeFromNatwork());
    }

    OTPRegistrationResponse otpRegistrationResponses; /*Get JsonObject*/
    @Override
    public Observable<OTPRegistrationResponse> getOTPFromNetwork(Outer.OTPRequest otpRequest) {
         Observable<OTPRegistrationResponse> otpRegistrationResponseObservable=iLoginModuleAPIService.registerOTP(otpRequest);
        return otpRegistrationResponseObservable.doOnNext(new Action1<OTPRegistrationResponse>() {
            @Override
            public void call(OTPRegistrationResponse otpRegistrationResponse) {
                otpRegistrationResponses=otpRegistrationResponse;
            }
        });
    }


}
