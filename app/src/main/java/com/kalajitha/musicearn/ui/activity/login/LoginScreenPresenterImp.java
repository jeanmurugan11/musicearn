package com.kalajitha.musicearn.ui.activity.login;

import android.text.TextUtils;

/**
 * Created by supercomputer on 14/7/17.
 */

public class LoginScreenPresenterImp implements ILoginMVP.Presenter,ILoginMVP.Model.OnLoginFinishedListener {

    private ILoginMVP.View view;

    private ILoginMVP.Model loginInteractor;



    public LoginScreenPresenterImp(ILoginMVP.View view) {
        this.view = view;
        this.loginInteractor = new LoginSreenModelImp();
    }

    @Override
    public void validateCredentials(String username, String password) {

        if(view !=null){
            if(TextUtils.isEmpty(username)){
                view.setUsernameError();
            }else if(TextUtils.isEmpty(password)){
                view.setPasswordError();
            }else{
                view.showLoader();
                loginInteractor.login(username,password,this);
            }
        }
    }

    @Override
    public void onDestroy() {

        if(view !=null)view=null;
    }

    @Override
    public void closeLoader() {
        if (view != null) {
            view.closeLoader();
        }
    }

    @Override
    public void onSuccess() {
        if (view != null) {
            view.closeLoader();
            view.loginSuccess();
        }
    }
}
