package com.kalajitha.musicearn.ui.activity.postdata;


import com.kalajitha.musicearn.network.APIServiceInterface;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PostDataModule {

    @Provides
    public IPostDataMVP.IPresender provdePostDataPresenter(IPostDataMVP.IModel model){
        return new PostDataPresenterImp(model);
    }

    @Provides
    public IPostDataMVP.IModel providePostDataModel(IPostDataRepo postDataRepo){
        return new PostDataModelImp(postDataRepo);
    }
    @Singleton
    @Provides
    public IPostDataRepo providePostDataRepo(APIServiceInterface loginModuleAPIService){
        return new PostDataRepoImp(loginModuleAPIService);
    }
}
