package com.kalajitha.musicearn.ui.activity.postdata;


import android.content.Context;

import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.AirFormCategoryList;
import com.kalajitha.musicearn.model.response.AirFormGreduList;
import com.kalajitha.musicearn.model.response.AirFormList;
import com.kalajitha.musicearn.model.response.AirFormTypeList;
import com.kalajitha.musicearn.model.response.CityList;
import com.kalajitha.musicearn.model.response.CountryList;
import com.kalajitha.musicearn.model.response.OrginzationAndInstution;
import com.kalajitha.musicearn.model.response.StateList;
import com.kalajitha.musicearn.model.response.TitleList;

import java.util.List;

import rx.Observable;

public interface IPostDataRepo {

    /*TitleHeading Repository*/
    Observable<List<OrginzationAndInstution>> getOrginzationAndInstutionFromMemory();
    Observable<List<OrginzationAndInstution>> getOrginzationAndInstutionFromNetwork();
    Observable<List<OrginzationAndInstution>> getOrginzationAndInstutionData();


    /*TitleHeading Repository*/
    Observable<List<TitleList>> getTitleFromMemory();
    Observable<List<TitleList>> getTitleFromNetwork();
    Observable<List<TitleList>> getTitleData();

    /*CountryHeading*/
    Observable<List<CountryList>> getcountryFromMemory();
    Observable<List<CountryList>> getcountryFromNetwork();
    Observable<List<CountryList>> getcountryData();

    /*State*/
    Observable<List<StateList>> getStateFromMemory();
    Observable<List<StateList>> getStateFromNetwork(Outer.GetStateRequest myOuter2);
    Observable<List<StateList>> getStateData(Outer.GetStateRequest myOuter2);

    /*City*/
    Observable<List<CityList>> getCityFromMemory();
    Observable<List<CityList>> getCityFromNetwork(Outer.GetCityRequest myOuter2 );
    Observable<List<CityList>> getcityData(Outer.GetCityRequest myOuter2);

    /*Air Form*/
    Observable<List<AirFormList>> getAirFormMemory();
    Observable<List<AirFormList>> getAirFromNetwork();
    Observable<List<AirFormList>> getAirFormData();

    /*Air Form Category*/
    Observable<List<AirFormCategoryList>> getAirFormCategoryFromMemory();
    Observable<List<AirFormCategoryList>> getAirFormCategoryFromNewwork(Outer.GetArtFormCategory myOuter2);
    Observable<List<AirFormCategoryList>> getAirFormCategoryData(Outer.GetArtFormCategory myOuter2);

    /*Air Form type*/
    Observable<List<AirFormTypeList>> getAirFormTypeFromMemory();
    Observable<List<AirFormTypeList>> getAirFormTypeFromNetwork(Outer.GetArtFormType myOuter2);
    Observable<List<AirFormTypeList>> getAirFormTypeData(Outer.GetArtFormType myOuter2);

    /*Air Form Gradu*/
    Observable<List<AirFormGreduList>> getairFormGraduFromMemory();
    Observable<List<AirFormGreduList>> getairFormGraduNetwork();
    Observable<List<AirFormGreduList>> getairFormGraduData();

    void savePostData(Context context, Outer.SavePostData saveUser);

}
