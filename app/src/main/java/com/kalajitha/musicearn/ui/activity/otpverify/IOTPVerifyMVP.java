package com.kalajitha.musicearn.ui.activity.otpverify;

import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.OTPVerifyResponse;

import rx.Observable;


public interface IOTPVerifyMVP {

    interface IOTPVerifyView{
        void showInputError(String s);
        void showLoader();
        void closeLoader();
        void onNavigationToVerification(OTPVerifyResponse otpVerifyResponse);
    }
    interface IOTPVerifyPresenter{
        void setView(IOTPVerifyMVP.IOTPVerifyView view);
        void rxUnsubscribe();
        void registerButtonClicked(Outer.OTPVerifyRequest otpRequest);
        void showInputError(String internetConnection_fails);
    }
    interface IOTPVerifyModel{
        Observable<OTPVerifyResponse> verifyOTP(Outer.OTPVerifyRequest otpVerifyRequest);
    }
}
