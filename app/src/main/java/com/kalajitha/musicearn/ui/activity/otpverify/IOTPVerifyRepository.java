package com.kalajitha.musicearn.ui.activity.otpverify;

import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.OTPVerifyResponse;

import rx.Observable;

/**
 * Created by supercomputer on 18/7/17.
 */

public interface IOTPVerifyRepository  {

    /*GetOTP*/
    Observable<OTPVerifyResponse> getOTPFromNetwork(Outer.OTPVerifyRequest otpVerifyRequest);
}
