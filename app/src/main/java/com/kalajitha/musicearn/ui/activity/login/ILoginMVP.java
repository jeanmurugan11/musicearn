package com.kalajitha.musicearn.ui.activity.login;

public interface ILoginMVP {

    interface View  {

        void showLoader();

        void closeLoader();

        void setUsernameError();

        void setPasswordError();

        void loginSuccess();


    }

    interface Presenter{

        void validateCredentials(String username, String password);
        void onDestroy();

    }

    interface Model{

        interface OnLoginFinishedListener {
            /*void onUsernameError();
            void onPasswordError();*/
            void closeLoader();
            void onSuccess();
        }

        void login(String username, String password, OnLoginFinishedListener listener);


    }
}
