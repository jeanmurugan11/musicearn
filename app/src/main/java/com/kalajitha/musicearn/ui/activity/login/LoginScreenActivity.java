package com.kalajitha.musicearn.ui.activity.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.kalajitha.musicearn.R;
import com.kalajitha.musicearn.constants.Constants;
import com.kalajitha.musicearn.constants.IConstant;
import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.GetPostRegistrationData;
import com.kalajitha.musicearn.model.response.PreFillerData;
import com.kalajitha.musicearn.network.loginmodule.ILoginModuleAPIService;
import com.kalajitha.musicearn.objects.UniversalPreferences;
import com.kalajitha.musicearn.root.MusicEarnApp;
import com.kalajitha.musicearn.ui.activity.home.MainActivity;
import com.kalajitha.musicearn.ui.activity.postdata.PostDataActivity;
import com.kalajitha.musicearn.ui.activity.register.RegisterActivityScreenImp;
import com.kalajitha.musicearn.view.ButtonMyriadProBold;
import com.kalajitha.musicearn.view.EditViewRegular;
import com.kalajitha.musicearn.view.TextViewBold;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kalajitha.musicearn.R.id.edit_email_number;



public class LoginScreenActivity extends AppCompatActivity implements ILoginMVP.View {


    @BindView(edit_email_number)
    EditViewRegular editEmailNumber;
    @BindView(R.id.textinput_layout_email_number)
    TextInputLayout textinputLayoutEmailNumber;
    @BindView(R.id.edit_password)
    EditViewRegular editPassword;
    @BindView(R.id.textinput_layout_password)
    TextInputLayout textinputLayoutPassword;
    @BindView(R.id.btn_login)
    ButtonMyriadProBold btnLogin;
    @BindView(R.id.btn_singup)
    ButtonMyriadProBold btnSingup;
    @BindView(R.id.txt_forgot_password)
    TextViewBold txtForgotPassword;

    ILoginMVP.Presenter presenter;
    Context mContext;

    @Inject
    ILoginModuleAPIService iLoginModuleAPIService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        ButterKnife.bind(this);
        ((MusicEarnApp)getApplication()).getComponent().inject(this);
        mContext=this;
        presenter=new LoginScreenPresenterImp(this);

    }

    @OnClick({R.id.btn_login,R.id.btn_singup})
    public void OnClickButton(ButtonMyriadProBold buttonMyriadProBold){
        switch (buttonMyriadProBold.getId()){

            case R.id.btn_login:
                if(MusicEarnApp.isNetworkConnected(mContext)){
                    String userName=editEmailNumber.getText().toString().trim();
                    String password=editPassword.getText().toString().trim();
                    presenter.validateCredentials(userName,password);
                }else{
                    Toast.makeText(mContext,
                            ""+Constants.InternetConnection_Fails, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_singup:
                Intent intent=new Intent(LoginScreenActivity.this, RegisterActivityScreenImp.class);
                startActivity(intent);
                break;
        }
    }

    @Override protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
    @Override
    public void showLoader() {

        MusicEarnApp.showProgress(mContext);
    }

    @Override
    public void closeLoader() {

        MusicEarnApp.closeProgress();
    }

    @Override
    public void setUsernameError() {

        editEmailNumber.setError(getString(R.string.username_error));
    }

    @Override
    public void setPasswordError() {

        editPassword.setError(getString(R.string.password_error));
    }

    @Override
    public void loginSuccess() {

        /*Check the Logined used profile completed or not */

        Outer.GetPostData getPostData=new Outer.GetPostData();
        getPostData.setUserName(editEmailNumber.getText().toString().trim());

        Call<GetPostRegistrationData> call = iLoginModuleAPIService.getPostRegistration(getPostData);
        call.enqueue(new Callback<GetPostRegistrationData>() {
            @Override
            public void onResponse(Call<GetPostRegistrationData> call, Response<GetPostRegistrationData> response) {
               // System.out.println("iiiiii"+response.body().getIsPostRegistrationDone());
                if(response.body().getIsPostRegistrationDone()){

                    PreFillerData preFillerData=response.body().getPreFillerData();
                    int userTypeId=preFillerData.getUserTypeId();

                    UniversalPreferences.getInstance().put(IConstant.SharedPref_LoginedUserid, editEmailNumber.getText().toString().trim());
                    UniversalPreferences.getInstance().put(IConstant.SharedPref_UserTypeId, userTypeId);

                    Intent intent=new Intent(LoginScreenActivity.this, MainActivity.class);
                    startActivity(intent);
                }else{

                    PreFillerData preFillerData=response.body().getPreFillerData();
                    int userTypeId=preFillerData.getUserTypeId();

                    UniversalPreferences.getInstance().put(IConstant.SharedPref_LoginedUserid, editEmailNumber.getText().toString().trim());
                    UniversalPreferences.getInstance().put(IConstant.SharedPref_UserTypeId, userTypeId);

                    Intent intent=new Intent(LoginScreenActivity.this, PostDataActivity.class);
                    startActivity(intent);

                }
            }
            @Override
            public void onFailure(Call<GetPostRegistrationData> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }


}
