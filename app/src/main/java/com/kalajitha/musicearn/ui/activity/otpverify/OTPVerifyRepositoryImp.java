package com.kalajitha.musicearn.ui.activity.otpverify;

import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.OTPVerifyResponse;
import com.kalajitha.musicearn.network.loginmodule.ILoginModuleAPIService;

import rx.Observable;
import rx.functions.Action1;

public class OTPVerifyRepositoryImp implements IOTPVerifyRepository {

    private ILoginModuleAPIService loginModuleAPIService;

    public OTPVerifyRepositoryImp(ILoginModuleAPIService loginModuleAPIService) {
        this.loginModuleAPIService = loginModuleAPIService;
    }

    OTPVerifyResponse otpVerifyResponse;
    @Override
    public Observable<OTPVerifyResponse> getOTPFromNetwork(Outer.OTPVerifyRequest otpVerifyRequest) {

        Observable<OTPVerifyResponse> otpRegistrationResponseObservable=loginModuleAPIService.verifyOTP(otpVerifyRequest);
        return otpRegistrationResponseObservable.doOnNext(new Action1<OTPVerifyResponse>() {
            @Override
            public void call(OTPVerifyResponse otpVerifyRespons) {
                otpVerifyResponse=otpVerifyRespons;
            }
        });

    }
}
