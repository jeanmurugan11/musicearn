package com.kalajitha.musicearn.ui.activity.register;

import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.OTPRegistrationResponse;
import com.kalajitha.musicearn.model.response.UserType;

import java.util.List;

import rx.Observable;



public interface IRegisterMVP {

    interface IRegisterView{
        void showInputError(String s);
        void showLoader();
        void closeLoader();
        void updateUserType(List<UserType> userTypes);
        void onNavigationToVerification(OTPRegistrationResponse otpRegistrationResponse);
    }
    interface IRegisterPresenter{
        void setView(IRegisterMVP.IRegisterView view);
        void rxUnsubscribe();
        void loaduserType();
        void registerButtonClicked(Outer.OTPRequest otpRequest);
        void showInputError(String internetConnection_fails);
    }
    interface IRegisterModel{

        Observable<List<UserType>> getUserType();
        Observable<OTPRegistrationResponse>registerOTP(Outer.OTPRequest otpRequest);
    }
}
