package com.kalajitha.musicearn.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OTPVerifyResponse {

    @SerializedName("isOTPValidate")
    @Expose
    private Boolean isOTPValidate;
    @SerializedName("message")
    @Expose
    private String message;

    public Boolean getIsOTPValidate() {
        return isOTPValidate;
    }

    public void setIsOTPValidate(Boolean isOTPValidate) {
        this.isOTPValidate = isOTPValidate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}