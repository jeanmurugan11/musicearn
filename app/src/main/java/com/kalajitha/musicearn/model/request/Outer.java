package com.kalajitha.musicearn.model.request;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

public class Outer {



    public static class GetStateRequest  {


        public String getCountryId() {
            return countryId;
        }

        public void setCountryId(String countryId) {
            this.countryId = countryId;
        }

        private String countryId;

    }

    public static class GetCityRequest  {


        public String getCountryId() {
            return countryId;
        }

        public void setCountryId(String countryId) {
            this.countryId = countryId;
        }

        private String countryId;

        public String getStateId() {
            return stateId;
        }

        public void setStateId(String stateId) {
            this.stateId = stateId;
        }

        private String stateId;

    }

    public static class GetArtFormCategory{

        public String getArtFormId() {
            return artFormId;
        }

        public void setArtFormId(String artFormId) {
            this.artFormId = artFormId;
        }

        private String artFormId;
    }

     public static class GetArtFormType {

         public String getArtFormId() {
                return artFormId;
            }

         public void setArtFormId(String artFormId) {
                this.artFormId = artFormId;
            }

         public String getArtFormCategoryId() {
             return artFormCategoryId;
         }

         public void setArtFormCategoryId(String artFormCategoryId) {
             this.artFormCategoryId = artFormCategoryId;
         }

         private String artFormCategoryId;
         private String artFormId;

        }


    public static class GetPostData {

        private String userName;

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

    }


    public static class GetUsers {

        private String userName;
        private String userTypeId;

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getUserTypeId() {
            return userTypeId;
        }

        public void setUserTypeId(String userTypeId) {
            this.userTypeId = userTypeId;
        }
    }


    public static class MTSDProfile {

        private String userTypeId;
        private String userContactId;

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        private String userName;

        public String getUserTypeId() {
            return userTypeId;
        }

        public void setUserTypeId(String userTypeId) {
            this.userTypeId = userTypeId;
        }

        public String getUserContactId() {
            return userContactId;
        }

        public void setUserContactId(String userContactId) {
            this.userContactId = userContactId;
        }
    }


    public static class MTSDDetailsProfileRequest {

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        private String userName;

    }

    public static class OTPRequest{

       private String  emailId;
       private String  mobileNumber;
       private String  userTypeId;

        public String getEmailId() {
            return emailId;
        }

        public void setEmailId(String emailId) {
            this.emailId = emailId;
        }

        public String getMobileNumber() {
            return mobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
        }

        public String getUserTypeId() {
            return userTypeId;
        }

        public void setUserTypeId(String userTypeId) {
            this.userTypeId = userTypeId;
        }
    }

    public static class OTPVerifyRequest{

       private String  OTP;
       private String  emailId;
       private String  mobileNumber;
       private String  password;
       private String  confirmPassword;

        public String getOTP() {
            return OTP;
        }

        public void setOTP(String OTP) {
            this.OTP = OTP;
        }

        public String getEmailId() {
            return emailId;
        }

        public void setEmailId(String emailId) {
            this.emailId = emailId;
        }

        public String getMobileNumber() {
            return mobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getConfirmPassword() {
            return confirmPassword;
        }

        public void setConfirmPassword(String confirmPassword) {
            this.confirmPassword = confirmPassword;
        }
    }

    public static class SaveUser {

        public String getTitleId() {
            return titleId;
        }

        public void setTitleId(String titleId) {
            this.titleId = titleId;
        }

        public String getCountryId() {
            return countryId;
        }

        public void setCountryId(String countryId) {
            this.countryId = countryId;
        }

        public String getStateId() {
            return stateId;
        }

        public void setStateId(String stateId) {
            this.stateId = stateId;
        }

        public String getCityId() {
            return cityId;
        }

        public void setCityId(String cityId) {
            this.cityId = cityId;
        }

        public String getAirFormId() {
            return airFormId;
        }

        public void setAirFormId(String artFormId) {
            this.airFormId = artFormId;
        }

        public String getAirFormCategoryId() {
            return airFormCategoryId;
        }

        public void setAirFormCategoryId(String artFormCategoryId) {
            this.airFormCategoryId = artFormCategoryId;
        }

        public String getAirFormTypeId() {
            return airFormTypeId;
        }

        public void setAirFormTypeId(String artFormTypeId) {
            this.airFormTypeId = artFormTypeId;
        }

        public String getAirAcceretionId() {
            return airAcceretionId;
        }

        public void setAirAcceretionId(String airAcceretionId) {
            this.airAcceretionId = airAcceretionId;
        }

        public String getAdditionalArtFormCategoryId() {
            return additionalArtFormCategoryId;
        }

        public void setAdditionalArtFormCategoryId(String additionalArtFormCategoryId) {
            this.additionalArtFormCategoryId = additionalArtFormCategoryId;
        }

        public String getAddtionalArtFormTypeId() {
            return addtionalArtFormTypeId;
        }

        public void setAddtionalArtFormTypeId(String addtionalArtFormTypeId) {
            this.addtionalArtFormTypeId = addtionalArtFormTypeId;
        }

        public String getAddtionalAirAcceretionId() {
            return addtionalAirAcceretionId;
        }

        public void setAddtionalAirAcceretionId(String addtionalAirAcceretionId) {
            this.addtionalAirAcceretionId = addtionalAirAcceretionId;
        }

        public String getRadiogroup_value() {
            return radiogroup_value;
        }

        public void setRadiogroup_value(String radiogroup_value) {
            this.radiogroup_value = radiogroup_value;
        }

        public String getPrefixName() {
            return prefixName;
        }

        public void setPrefixName(String prefixName) {
            this.prefixName = prefixName;
        }

        public String getmName() {
            return mName;
        }

        public void setmName(String mName) {
            this.mName = mName;
        }

        public String getlName() {
            return lName;
        }

        public void setlName(String lName) {
            this.lName = lName;
        }

        public String getfName() {
            return fName;
        }

        public void setfName(String fName) {
            this.fName = fName;
        }

        public String getMobileNumber() {
            return mobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
        }

        public String getEmailId() {
            return emailId;
        }

        public void setEmailId(String emailId) {
            this.emailId = emailId;
        }

        public String getProfileImagePath() {
            return profileImagePath;
        }

        public void setProfileImagePath(String profileImagePath) {
            this.profileImagePath = profileImagePath;
        }

        public Bitmap getBitmap() {
            return bitmap;
        }

        public void setBitmap(Bitmap bitmap) {
            this.bitmap = bitmap;
        }

        public String getUserTypeId() {
            return userTypeId;
        }

        public void setUserTypeId(String userTypeId) {
            this.userTypeId = userTypeId;
        }

        String userTypeId=null;
        String titleId=null;
        String countryId=null;
        String stateId=null;
        String cityId=null;
        String airFormId=null;
        String airFormCategoryId=null;
        String airFormTypeId=null;
        String airAcceretionId=null;
        String additionalArtFormCategoryId=null;
        String addtionalArtFormTypeId=null;
        String addtionalAirAcceretionId=null;
        String radiogroup_value="1";
        String prefixName=null;
        String mName=null;
        String lName=null;
        String fName=null;
        String mobileNumber=null;
        String emailId=null;
        String profileImagePath=null;
        Bitmap bitmap;

        String memberShipId;/*it will use only in the audiance and vender page */

        public List<Integer> getVenderServiceSelectedList() {
            return venderServiceSelectedList;
        }

        public void setVenderServiceSelectedList(List<Integer> venderServiceSelectedList) {
            this.venderServiceSelectedList = venderServiceSelectedList;
        }

        List<Integer> venderServiceSelectedList =new ArrayList<Integer>(); /*it will use only in the vender page */

        public String getMemberShipId() {
            return memberShipId;
        }

        public void setMemberShipId(String memberShipId) {
            this.memberShipId = memberShipId;
        }


    }

    public static class SavePostData{
        String userTypeId=null;
        String titleId=null;
        String countryId=null;
        String stateId=null;
        String cityId=null;
        String airFormId=null;
        String airFormCategoryId=null;
        String airFormTypeId=null;
        String airAcceretionId=null;
        String radiogroup_value="1";
        String prefixName=null;
        String mName=null;
        String lName=null;
        String fName=null;
        String mobileNumber=null;
        String emailId=null;
        String profileImagePath=null;
        Bitmap bitmap;

        /*If user login as orgaizer*/
        String organisationName=null;
        String organisationRoleId=null;

        /*if user login as Instution*/
       String institutionName=null;
       String institutionRoleId=null;

        public String getUserLoginedId() {
            return userTypeId;
        }

        public void setUserLoginedId(String userTypeId) {
            this.userTypeId = userTypeId;
        }

        public String getTitleId() {
            return titleId;
        }

        public void setTitleId(String titleId) {
            this.titleId = titleId;
        }

        public String getCountryId() {
            return countryId;
        }

        public void setCountryId(String countryId) {
            this.countryId = countryId;
        }

        public String getStateId() {
            return stateId;
        }

        public void setStateId(String stateId) {
            this.stateId = stateId;
        }

        public String getCityId() {
            return cityId;
        }

        public void setCityId(String cityId) {
            this.cityId = cityId;
        }

        public String getAirFormId() {
            return airFormId;
        }

        public void setAirFormId(String airFormId) {
            this.airFormId = airFormId;
        }

        public String getAirFormCategoryId() {
            return airFormCategoryId;
        }

        public void setAirFormCategoryId(String airFormCategoryId) {
            this.airFormCategoryId = airFormCategoryId;
        }

        public String getAirFormTypeId() {
            return airFormTypeId;
        }

        public void setAirFormTypeId(String airFormTypeId) {
            this.airFormTypeId = airFormTypeId;
        }

        public String getAirAcceretionId() {
            return airAcceretionId;
        }

        public void setAirAcceretionId(String airAcceretionId) {
            this.airAcceretionId = airAcceretionId;
        }

        public String getRadiogroup_value() {
            return radiogroup_value;
        }

        public void setRadiogroup_value(String radiogroup_value) {
            this.radiogroup_value = radiogroup_value;
        }

        public String getPrefixName() {
            return prefixName;
        }

        public void setPrefixName(String prefixName) {
            this.prefixName = prefixName;
        }

        public String getmName() {
            return mName;
        }

        public void setmName(String mName) {
            this.mName = mName;
        }

        public String getlName() {
            return lName;
        }

        public void setlName(String lName) {
            this.lName = lName;
        }

        public String getfName() {
            return fName;
        }

        public void setfName(String fName) {
            this.fName = fName;
        }

        public String getMobileNumber() {
            return mobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
        }

        public String getEmailId() {
            return emailId;
        }

        public void setEmailId(String emailId) {
            this.emailId = emailId;
        }

        public String getProfileImagePath() {
            return profileImagePath;
        }

        public void setProfileImagePath(String profileImagePath) {
            this.profileImagePath = profileImagePath;
        }

        public Bitmap getBitmap() {
            return bitmap;
        }

        public void setBitmap(Bitmap bitmap) {
            this.bitmap = bitmap;
        }

        public String getOrganisationName() {
            return organisationName;
        }

        public void setOrganisationName(String organisationName) {
            this.organisationName = organisationName;
        }

        public String getOrganisationRoleId() {
            return organisationRoleId;
        }

        public void setOrganisationRoleId(String organisationRoleId) {
            this.organisationRoleId = organisationRoleId;
        }

        public String getInstitutionName() {
            return institutionName;
        }

        public void setInstitutionName(String institutionName) {
            this.institutionName = institutionName;
        }

        public String getInstitutionRoleId() {
            return institutionRoleId;
        }

        public void setInstitutionRoleId(String institutionRoleId) {
            this.institutionRoleId = institutionRoleId;
        }
    }

    /*Post Method  Form data eventPurposeID,eventTile,eventStart,eventEnd,slogan,UserName=9043147655,[file,filename]*/
    public static class CreateEventRequest{

        String eventPurposeID=null;
        String eventTile=null;
        String eventStart=null;
        String eventEnd=null;
        String slogan=null;
        String UserName=null;
        String profileImagePath=null;
        Bitmap bitmap;

        public String getEventPurposeID() {
            return eventPurposeID;
        }

        public void setEventPurposeID(String eventPurposeID) {
            this.eventPurposeID = eventPurposeID;
        }

        public String getEventTile() {
            return eventTile;
        }

        public void setEventTile(String eventTile) {
            this.eventTile = eventTile;
        }

        public String getEventStart() {
            return eventStart;
        }

        public void setEventStart(String eventStart) {
            this.eventStart = eventStart;
        }

        public String getEventEnd() {
            return eventEnd;
        }

        public void setEventEnd(String eventEnd) {
            this.eventEnd = eventEnd;
        }

        public String getSlogan() {
            return slogan;
        }

        public void setSlogan(String slogan) {
            this.slogan = slogan;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String userName) {
            UserName = userName;
        }

        public String getProfileImagePath() {
            return profileImagePath;
        }

        public void setProfileImagePath(String profileImagePath) {
            this.profileImagePath = profileImagePath;
        }

        public Bitmap getBitmap() {
            return bitmap;
        }

        public void setBitmap(Bitmap bitmap) {
            this.bitmap = bitmap;
        }
    }


    public static class EventTable {

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String userName) {
            UserName = userName;
        }

        String UserName;
    }

}