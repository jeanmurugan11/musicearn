package com.kalajitha.musicearn.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CountryList {


    @SerializedName("countryId")
    @Expose
    private String countryId;

    @SerializedName("countryName")
    @Expose
    private String countryName;


    public String getId() {
        return countryId;
    }

    public void setId(String id) {
        this.countryId = id;
    }

    public String getName() {
        return countryName;
    }

    public void setName(String name) {
        this.countryName = name;
    }


    //to display object as a string in spinner
    @Override
    public String toString() {
        return countryName;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof CountryList){
            CountryList c = (CountryList)obj;
            if(c.getName().equals(countryName) && c.getId().equals(countryId)) return true;
        }

        return false;
    }


}
