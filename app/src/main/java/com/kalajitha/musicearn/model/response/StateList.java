package com.kalajitha.musicearn.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StateList {

    @SerializedName("stateId")
    @Expose
    private String stateId;

    @SerializedName("stateName")
    @Expose
    private String stateName;


    public String getId() {
        return stateId;
    }

    public void setId(String id) {
        this.stateId = id;
    }

    public String getName() {
        return stateName;
    }

    public void setName(String name) {
        this.stateName = name;
    }


    //to display object as a string in spinner
    @Override
    public String toString() {
        return stateName;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof StateList){
            StateList c = (StateList)obj;
            if(c.getName().equals(stateName) && c.getId().equals(stateId)) return true;
        }

        return false;
    }

}
