package com.kalajitha.musicearn.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AirFormCategoryList {

    @SerializedName("artFormCategoryId")
    @Expose
    private String id;

    @SerializedName("artFormCategoryName")
    @Expose
    private String artFormCategoryName;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return artFormCategoryName;
    }

    public void setName(String name) {
        this.artFormCategoryName = name;
    }


    //to display object as a string in spinner
    @Override
    public String toString() {
        return artFormCategoryName;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof AirFormCategoryList){
            AirFormCategoryList c = (AirFormCategoryList)obj;
            if(c.getName().equals(artFormCategoryName) && c.getId()==id ) return true;
        }

        return false;
    }

}
