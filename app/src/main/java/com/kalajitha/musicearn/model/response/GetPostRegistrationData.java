package com.kalajitha.musicearn.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetPostRegistrationData {

    @SerializedName("isPostRegistrationDone")
    @Expose
    private Boolean isPostRegistrationDone;
    @SerializedName("isUserLogged")
    @Expose
    private Boolean isUserLogged;
    @SerializedName("preFillerData")
    @Expose
    private PreFillerData preFillerData;

    public Boolean getIsPostRegistrationDone() {
        return isPostRegistrationDone;
    }

    public void setIsPostRegistrationDone(Boolean isPostRegistrationDone) {
        this.isPostRegistrationDone = isPostRegistrationDone;
    }

    public Boolean getIsUserLogged() {
        return isUserLogged;
    }

    public void setIsUserLogged(Boolean isUserLogged) {
        this.isUserLogged = isUserLogged;
    }

    public PreFillerData getPreFillerData() {
        return preFillerData;
    }

    public void setPreFillerData(PreFillerData preFillerData) {
        this.preFillerData = preFillerData;
    }

}