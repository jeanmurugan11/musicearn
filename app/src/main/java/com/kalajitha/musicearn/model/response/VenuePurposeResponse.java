package com.kalajitha.musicearn.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by jean on 23/9/17.
 */

public class VenuePurposeResponse {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("venueName")
    @Expose
    private String purposename;

    public VenuePurposeResponse(String id, String purposename) {
        this.id = id;
        this.purposename = purposename;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return purposename;
    }

    public void setName(String name) {
        this.purposename = name;
    }


    //to display object as a string in spinner
    @Override
    public String toString() {
        return purposename;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof PurposeofEventResponse){
            VenuePurposeResponse c = (VenuePurposeResponse)obj;
            if(c.getName().equals(purposename) && c.getId().equals(id) ) return true;
        }

        return false;
    }


}

