package com.kalajitha.musicearn.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PreFillerData {

    @SerializedName("userTypeId")
    @Expose
    private Integer userTypeId;
    @SerializedName("prefixName")
    @Expose
    private String prefixName;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("middleName")
    @Expose
    private String middleName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("titleId")
    @Expose
    private Integer titleId;
    @SerializedName("gender")
    @Expose
    private Integer gender;
    @SerializedName("cityId")
    @Expose
    private Integer cityId;
    @SerializedName("stateId")
    @Expose
    private Integer stateId;
    @SerializedName("countryId")
    @Expose
    private Integer countryId;
    @SerializedName("profileImage")
    @Expose
    private String profileImage;
    @SerializedName("userLoginId")
    @Expose
    private Integer userLoginId;
    @SerializedName("organisationName")
    @Expose
    private String organisationName;
    @SerializedName("organisationRoleId")
    @Expose
    private Integer organisationRoleId;
    @SerializedName("institutionName")
    @Expose
    private String institutionName;
    @SerializedName("institutionRoleId")
    @Expose
    private Integer institutionRoleId;
    @SerializedName("artFormId")
    @Expose
    private Integer artFormId;
    @SerializedName("mainCategoryId")
    @Expose
    private Integer mainCategoryId;
    @SerializedName("mainPerformanceCategoryId")
    @Expose
    private Integer mainPerformanceCategoryId;
    @SerializedName("mainAIRAccreditationGradeId")
    @Expose
    private Integer mainAIRAccreditationGradeId;

    public Integer getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(Integer userTypeId) {
        this.userTypeId = userTypeId;
    }

    public String getPrefixName() {
        return prefixName;
    }

    public void setPrefixName(String prefixName) {
        this.prefixName = prefixName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getTitleId() {
        return titleId;
    }

    public void setTitleId(Integer titleId) {
        this.titleId = titleId;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public Integer getUserLoginId() {
        return userLoginId;
    }

    public void setUserLoginId(Integer userLoginId) {
        this.userLoginId = userLoginId;
    }

    public String getOrganisationName() {
        return organisationName;
    }

    public void setOrganisationName(String organisationName) {
        this.organisationName = organisationName;
    }

    public Integer getOrganisationRoleId() {
        return organisationRoleId;
    }

    public void setOrganisationRoleId(Integer organisationRoleId) {
        this.organisationRoleId = organisationRoleId;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public Integer getInstitutionRoleId() {
        return institutionRoleId;
    }

    public void setInstitutionRoleId(Integer institutionRoleId) {
        this.institutionRoleId = institutionRoleId;
    }

    public Integer getArtFormId() {
        return artFormId;
    }

    public void setArtFormId(Integer artFormId) {
        this.artFormId = artFormId;
    }

    public Integer getMainCategoryId() {
        return mainCategoryId;
    }

    public void setMainCategoryId(Integer mainCategoryId) {
        this.mainCategoryId = mainCategoryId;
    }

    public Integer getMainPerformanceCategoryId() {
        return mainPerformanceCategoryId;
    }

    public void setMainPerformanceCategoryId(Integer mainPerformanceCategoryId) {
        this.mainPerformanceCategoryId = mainPerformanceCategoryId;
    }

    public Integer getMainAIRAccreditationGradeId() {
        return mainAIRAccreditationGradeId;
    }

    public void setMainAIRAccreditationGradeId(Integer mainAIRAccreditationGradeId) {
        this.mainAIRAccreditationGradeId = mainAIRAccreditationGradeId;
    }

}