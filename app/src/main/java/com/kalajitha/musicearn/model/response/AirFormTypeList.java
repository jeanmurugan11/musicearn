package com.kalajitha.musicearn.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AirFormTypeList {

    @SerializedName("artPerformanceId")
    @Expose
    private String artPerformanceId;

    @SerializedName("artPerformanceName")
    @Expose
    private String artPerformanceName;

    public String getId() {
        return artPerformanceId;
    }

    public void setId(String id) {
        this.artPerformanceId = id;
    }

    public String getName() {
        return artPerformanceName;
    }

    public void setName(String name) {
        this.artPerformanceName = name;
    }


    //to display object as a string in spinner
    @Override
    public String toString() {
        return artPerformanceName;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof AirFormTypeList){
            AirFormTypeList c = (AirFormTypeList)obj;
            if(c.getName().equals(artPerformanceName) && c.getId().equals(artPerformanceId)) return true;
        }

        return false;
    }

}
