package com.kalajitha.musicearn.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AirFormGreduList {

    @SerializedName("airId")
    @Expose
    private String airId;

    @SerializedName("airAccreditationName")
    @Expose
    private String airAccreditationName;

    public String getId() {
        return airId;
    }

    public void setId(String id) {
        this.airId = id;
    }

    public String getName() {
        return airAccreditationName;
    }

    public void setName(String name) {
        this.airAccreditationName = name;
    }


    //to display object as a string in spinner
    @Override
    public String toString() {
        return airAccreditationName;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof AirFormGreduList){
            AirFormGreduList c = (AirFormGreduList)obj;
            if(c.getName().equals(airAccreditationName) && c.getId().equals(airId)) return true;
        }

        return false;
    }

}
