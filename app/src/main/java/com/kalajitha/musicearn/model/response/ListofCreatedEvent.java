package com.kalajitha.musicearn.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListofCreatedEvent {

    @SerializedName("eventId")
    @Expose
    private String eventId;
    @SerializedName("eventTile")
    @Expose
    private String eventTile;
    @SerializedName("eventStart")
    @Expose
    private String eventStart;
    @SerializedName("eventEnd")
    @Expose
    private String eventEnd;
    @SerializedName("eventPurpose")
    @Expose
    private String eventPurpose;
    @SerializedName("editCode")
    @Expose
    private String editCode;
    @SerializedName("deleteCode")
    @Expose
    private String deleteCode;

    public ListofCreatedEvent(String eventId, String eventTile,
                              String eventStart, String eventEnd,
                              String eventPurpose, String editCode,
                              String deleteCode) {
        this.eventId = eventId;
        this.eventTile = eventTile;
        this.eventStart = eventStart;
        this.eventEnd = eventEnd;
        this.eventPurpose = eventPurpose;
        this.editCode = editCode;
        this.deleteCode = deleteCode;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventTile() {
        return eventTile;
    }

    public void setEventTile(String eventTile) {
        this.eventTile = eventTile;
    }

    public String getEventStart() {
        return eventStart;
    }

    public void setEventStart(String eventStart) {
        this.eventStart = eventStart;
    }

    public String getEventEnd() {
        return eventEnd;
    }

    public void setEventEnd(String eventEnd) {
        this.eventEnd = eventEnd;
    }

    public String getEventPurpose() {
        return eventPurpose;
    }

    public void setEventPurpose(String eventPurpose) {
        this.eventPurpose = eventPurpose;
    }

    public String getEditCode() {
        return editCode;
    }

    public void setEditCode(String editCode) {
        this.editCode = editCode;
    }

    public String getDeleteCode() {
        return deleteCode;
    }

    public void setDeleteCode(String deleteCode) {
        this.deleteCode = deleteCode;
    }

}