package com.kalajitha.musicearn.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TitleList {

    @SerializedName("titleId")
    @Expose
    private String titleId;
    @SerializedName("titleName")
    @Expose
    private String titleName;

    public String getTitleId() {
        return titleId;
    }

    public void setTitleId(String titleId) {
        this.titleId = titleId;
    }

    public String getTitleName() {
        return titleName;
    }

    public void setTitleName(String titleName) {
        this.titleName = titleName;
    }

    //to display object as a string in spinner
    @Override
    public String toString() {
        return titleName;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TitleList) {
            TitleList c = (TitleList) obj;
            if (c.getTitleName().equals(titleName) && c.getTitleId().equals(titleId)) return true;
        }

        return false;
    }

}