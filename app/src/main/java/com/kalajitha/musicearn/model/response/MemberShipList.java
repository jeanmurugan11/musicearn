package com.kalajitha.musicearn.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MemberShipList {


    @SerializedName("memberShipId")
    @Expose
    private String memberShipId;

    @SerializedName("memberShipName")
    @Expose
    private String memberShipName;


    public String getId() {
        return memberShipId;
    }

    public void setId(String id) {
        this.memberShipId = id;
    }

    public String getName() {
        return memberShipName;
    }

    public void setName(String name) {
        this.memberShipName = name;
    }


    //to display object as a string in spinner
    @Override
    public String toString() {
        return memberShipName;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof MemberShipList){
            MemberShipList c = (MemberShipList)obj;
            if(c.getName().equals(memberShipName) && c.getId().equals(memberShipId) ) return true;
        }

        return false;
    }

}
