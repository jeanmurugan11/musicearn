package com.kalajitha.musicearn.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CityList {

    @SerializedName("cityId")
    @Expose
    private String cityid;

    @SerializedName("cityName")
    @Expose
    private String cityName;


    public String getId() {
        return cityid;
    }

    public void setId(String id) {
        this.cityid = id;
    }

    public String getName() {
        return cityName;
    }

    public void setName(String name) {
        this.cityName = name;
    }


    //to display object as a string in spinner
    @Override
    public String toString() {
        return cityName;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof CityList){
            CityList c = (CityList)obj;
            if(c.getName().equals(cityName) && c.getId().equals(cityid) ) return true;
        }

        return false;
    }

}
