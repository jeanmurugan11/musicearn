package com.kalajitha.musicearn.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class VenderOptionList {


    @SerializedName("vendorServiceId")
    @Expose
    private String vendorServiceId;

    @SerializedName("vendorServiceName")
    @Expose
    private String vendorServiceName;


    public String getId() {
        return vendorServiceId;
    }

    public void setId(String id) {
        this.vendorServiceId = id;
    }

    public String getName() {
        return vendorServiceName;
    }

    public void setName(String name) {
        this.vendorServiceName = name;
    }


    //to display object as a string in spinner
    @Override
    public String toString() {
        return vendorServiceName;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof VenderOptionList){
            VenderOptionList c = (VenderOptionList)obj;
            if(c.getName().equals(vendorServiceName) && c.getId().equals(vendorServiceId) ) return true;
        }

        return false;
    }

}
