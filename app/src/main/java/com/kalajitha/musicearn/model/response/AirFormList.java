package com.kalajitha.musicearn.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AirFormList {


    @SerializedName("artFormId")
    @Expose
    private String artFormId;

    @SerializedName("artformName")
    @Expose
    private String artformName;


    public String getId() {
        return artFormId;
    }

    public void setId(String id) {
        this.artFormId = id;
    }

    public String getName() {
        return artformName;
    }

    public void setName(String name) {
        this.artformName = name;
    }


    //to display object as a string in spinner
    @Override
    public String toString() {
        return artformName;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof AirFormList){
            AirFormList c = (AirFormList)obj;
            if(c.getName().equals(artformName) && c.getId().equals(artFormId)) return true;
        }
        return false;
    }

}
