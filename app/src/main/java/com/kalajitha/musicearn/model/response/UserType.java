package com.kalajitha.musicearn.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserType {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("userTypeName")
    @Expose
    private String userTypeName;

    public String getTitleId() {
        return id;
    }

    public void setTitleId(String titleId) {
        this.id = titleId;
    }

    public String getTitleName() {
        return userTypeName;
    }

    public void setTitleName(String titleName) {
        this.userTypeName = titleName;
    }

    //to display object as a string in spinner
    @Override
    public String toString() {
        return userTypeName;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof UserType) {
            UserType c = (UserType) obj;
            if (c.getTitleName().equals(userTypeName) && c.getTitleId().equals(id)) return true;
        }

        return false;
    }

}