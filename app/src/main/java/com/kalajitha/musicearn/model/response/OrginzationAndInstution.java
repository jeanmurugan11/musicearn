package com.kalajitha.musicearn.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrginzationAndInstution {

    @SerializedName("organisationRoleId")
    @Expose
    private String organisationRoleId;
    @SerializedName("organisationRoleName")
    @Expose
    private String organisationRoleName;

    public String getTitleId() {
        return organisationRoleId;
    }

    public void setTitleId(String titleId) {
        this.organisationRoleId = titleId;
    }

    public String getTitleName() {
        return organisationRoleName;
    }

    public void setTitleName(String titleName) {
        this.organisationRoleName = titleName;
    }

    //to display object as a string in spinner
    @Override
    public String toString() {
        return organisationRoleName;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof OrginzationAndInstution) {
            OrginzationAndInstution c = (OrginzationAndInstution) obj;
            if (c.getTitleName().equals(organisationRoleName) && c.getTitleId().equals(organisationRoleId)) return true;
        }
        return false;
    }

}