package com.kalajitha.musicearn.navigationliveo;



public class RowItem {

    public RowItem(int imageId){
        this.imageId=imageId;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    private int imageId;
}
