package com.kalajitha.musicearn.navigationliveo;

import android.view.View;

public interface ClickListener {

    public void itemClicked(View view, int position);

    public void rowClicked(View view, int position);

}
