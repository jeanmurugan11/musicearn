package com.kalajitha.musicearn.fragment;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kalajitha.musicearn.R;
import com.kalajitha.musicearn.adapter.TabPagerItem;
import com.kalajitha.musicearn.adapter.ViewPagerAdapter;
import com.kalajitha.musicearn.fragment.user.MusicianFragment;

import java.util.ArrayList;
import java.util.List;


public class UserViewPagerFragment extends Fragment {

    private static final String TEXT_FRAGMENT = "TEXT_FRAGMENT";

	private List<TabPagerItem> mTabs = new ArrayList<>();

    public static UserViewPagerFragment newInstance(String text){

        UserViewPagerFragment mFragment = new UserViewPagerFragment();
        Bundle mBundle = new Bundle();
        mBundle.putString(TEXT_FRAGMENT, text);
        mFragment.setArguments(mBundle);
        return mFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createTabPagerItem();
    }

    private void createTabPagerItem() {
        mTabs.add(new TabPagerItem(getString(R.string.musician), MusicianFragment.newInstance(getString(R.string.musician))));
        mTabs.add(new TabPagerItem(getString(R.string.dancer), DancerFragment.newInstance(getString(R.string.dancer))));
        mTabs.add(new TabPagerItem(getString(R.string.teacher), TeacherFragment.newInstance(getString(R.string.teacher))));
        mTabs.add(new TabPagerItem(getString(R.string.student), StudentFragment.newInstance(getString(R.string.student))));
        mTabs.add(new TabPagerItem(getString(R.string.guest), GuestFragment.newInstance(getString(R.string.guest))));
        mTabs.add(new TabPagerItem(getString(R.string.press), PressFragment.newInstance(getString(R.string.press))));
        mTabs.add(new TabPagerItem(getString(R.string.sponsor), SponserFragment.newInstance(getString(R.string.sponsor))));


        mTabs.add(new TabPagerItem(getString(R.string.audience), AudienceFragment.newInstance(getString(R.string.audience))));
        mTabs.add(new TabPagerItem(getString(R.string.vendor), VenderFragment.newInstance(getString(R.string.vendor))));
       /*
        mTabs.add(new TabPagerItem(getString(R.string.venue), VenueFragment.newInstance(getString(R.string.venue))));
       mTabs.add(new TabPagerItem(getString(R.string.account), AccountFragment.newInstance(getString(R.string.account))));*/
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_viewpager, container, false);

        rootView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT ));

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

    	ViewPager mViewPager = (ViewPager) view.findViewById(R.id.viewPager);
    	
    	//mViewPager.setOffscreenPageLimit(mTabs.size());
    	mViewPager.setOffscreenPageLimit(1);

        mViewPager.setAdapter(new ViewPagerAdapter(getChildFragmentManager(), mTabs));

        TabLayout mSlidingTabLayout = (TabLayout) view.findViewById(R.id.tabLayout);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mSlidingTabLayout.setElevation(15);
        }

        mSlidingTabLayout.setupWithViewPager(mViewPager);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().setTitle(getArguments().getString(TEXT_FRAGMENT));
    }
}