/*
 * Copyright 2015 Rudson Lima
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kalajitha.musicearn.fragment.user;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SearchView;
import android.widget.Toast;

import com.kalajitha.musicearn.croppings.BitmapUtil;
import com.kalajitha.musicearn.croppings.CropperImageActivity;
import com.kalajitha.musicearn.croppings.ImageViewRounded;
import com.kalajitha.musicearn.croppings.RoundedShapeBitmap;
import com.kalajitha.musicearn.croppings.Utils;
import com.kalajitha.musicearn.R;
import com.kalajitha.musicearn.adapter.GetUserListAdapter;
import com.kalajitha.musicearn.constants.Constants;
import com.kalajitha.musicearn.constants.IConstant;
import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.AirFormCategoryList;
import com.kalajitha.musicearn.model.response.AirFormGreduList;
import com.kalajitha.musicearn.model.response.AirFormList;
import com.kalajitha.musicearn.model.response.AirFormTypeList;
import com.kalajitha.musicearn.model.response.CityList;
import com.kalajitha.musicearn.model.response.CountryList;
import com.kalajitha.musicearn.model.response.MemberShipList;
import com.kalajitha.musicearn.model.response.StateList;
import com.kalajitha.musicearn.model.response.TitleList;
import com.kalajitha.musicearn.model.response.UserResponce;
import com.kalajitha.musicearn.model.response.VenderOptionList;
import com.kalajitha.musicearn.objects.UniversalPreferences;
import com.kalajitha.musicearn.root.MusicEarnApp;
import com.kalajitha.musicearn.view.ButtonMyriadProBold;
import com.kalajitha.musicearn.view.EditViewRegular;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.kalajitha.musicearn.constants.Constants.Camara.bitmap;


public class MusicianFragment extends Fragment implements MusicianMVP.View {
    public static final String TAG = MusicianFragment.class.getSimpleName();
    private static final String TEXT_FRAGMENT = "TEXT_FRAGMENT";
    @BindView(R.id.image_profile)
    ImageViewRounded profileImageMusicianFragment;
    @BindView(R.id.name_musician_fragment_profix)
    EditViewRegular profixNameMusicianFragment;
    @BindView(R.id.first_name)
    EditViewRegular firstNameMusicianFragment;
    @BindView(R.id.midile_name)
    EditViewRegular middleNameMusicianFragment;
    @BindView(R.id.last_name)
    EditViewRegular lastNameMusicianFragment;
    @BindView(R.id.mobile_number)
    EditViewRegular mobileNumberMusicianFragment;
    @BindView(R.id.email_address)
    EditViewRegular emailMusicianFragment;
    @BindView(R.id.male_radio)
    RadioButton maleRadio;
    @BindView(R.id.female_radio)
    RadioButton femaleRadio;
    @BindView(R.id.register_radiogroup)
    RadioGroup registerRadiogroup;
    @BindView(R.id.user_submit)
    ButtonMyriadProBold userSubmit;
    @BindView(R.id.profile_title_edittext)
    EditText titleEditText;
    @BindView(R.id.country_edittext)
    EditViewRegular countryEdittext;
    @BindView(R.id.state_edittext)
    EditViewRegular stateEdittext;
    @BindView(R.id.city_edittext)
    EditViewRegular cityEdittext;
    @BindView(R.id.art_form)
    EditViewRegular airFromEdittext;
    @BindView(R.id.category_edittext)
    EditViewRegular categoryEdittext;
    @BindView(R.id.type_edittext)
    EditViewRegular typeEdittext;
    @BindView(R.id.gredu_edittext)
    EditViewRegular greduEdittext;
    @BindView(R.id.category_addon_edittext)
    EditViewRegular categorySecondEdittext;
    @BindView(R.id.type_addon_edittext)
    EditViewRegular typeSecondEdittext;
    @BindView(R.id.gredu_addon_edittext)
    EditViewRegular greduSecondEdittext;
    @BindView(R.id.searchView)
    SearchView searchView;
    @BindView(R.id.search_frame_layout)
    FrameLayout searchFrameLayout;
    @BindView(R.id.list)
    ListView list;
    @BindView(R.id.index)
    LinearLayout index;
    @BindView(R.id.fab_add)
    FloatingActionButton fabAdd;
    @BindView(R.id.parentFramagLaout)
    FrameLayout parentFramagLaout;
    @BindView(R.id.fab_list)
    FloatingActionButton fabList;
    @BindView(R.id.child_linearLaout)
    LinearLayout childLinearLaout;
    View view;
    Unbinder unbinder;
    Context mContext;

    @Inject
    MusicianMVP.Presenter presenter;
    GetUserListAdapter adapter1;
    List<UserResponce> usersList;

    public static MusicianFragment newInstance(String text) {
        MusicianFragment mFragment = new MusicianFragment();
        Bundle mBundle = new Bundle();
        mBundle.putString(TEXT_FRAGMENT, text);
        mFragment.setArguments(mBundle);
        return mFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_main, container, false);
        view.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        unbinder = ButterKnife.bind(this, view);
        mContext = container.getContext();
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        ((MusicEarnApp)getActivity().getApplication()).getComponent().inject(this);
         presenter.setView(this);

        Constants.titleId=null;
        Constants.countryId=null;
        Constants.stateId=null;
        Constants.cityId=null;
        Constants.airFormId=null;
        Constants.airFormCategoryId=null;
        Constants.airFormTypeId=null;
        Constants.airAcceretionId=null;
        Constants.additionalAirFormCategoryId=null;
        Constants.addtionalAirFormTypeId=null;
        Constants.addtionalAirAcceretionId=null;
        Constants.radiogroup_value="1";
        Constants.Camara.profileImagePath=null;
        bitmap=null;
        getUserList();
    }

    @Override
    public void onResume() {
        super.onResume();
        //Onresure code here
        presenter.setView(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        presenter.rxUnsubscribe();
    }

    @OnClick({R.id.profile_title_edittext, R.id.country_edittext, R.id.state_edittext,
            R.id.city_edittext, R.id.art_form, R.id.category_edittext,
            R.id.type_edittext, R.id.gredu_edittext,
            R.id.category_addon_edittext, R.id.type_addon_edittext,
            R.id.gredu_addon_edittext})
    public void submit(View view) {
        switch (view.getId()) {
            case R.id.profile_title_edittext:
                try {
                    if(MusicEarnApp.isNetworkConnected(mContext)){
                        presenter.loadTitle();
                    }else{
                        presenter.showInputError(Constants.InternetConnection_Fails);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.country_edittext:
                try {
                    if(MusicEarnApp.isNetworkConnected(mContext)){
                        presenter.loadCountry();
                    }else{
                        presenter.showInputError(Constants.InternetConnection_Fails);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.state_edittext:
                try {
                    if(MusicEarnApp.isNetworkConnected(mContext)){
                        if(Constants.countryId !=null){
                            Outer.GetStateRequest myOuter2 = new Outer.GetStateRequest();
                            myOuter2.setCountryId(Constants.countryId);

                            presenter.loadState(myOuter2);
                        }else{
                            presenter.showInputError(Constants.CountryRequeiredMessage);
                        }
                    }else{
                        presenter.showInputError(Constants.InternetConnection_Fails);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.city_edittext:
                try {
                    if(MusicEarnApp.isNetworkConnected(mContext)){
                        if(Constants.countryId !=null && Constants.stateId !=null){
                            Outer.GetCityRequest myOuter2 = new Outer.GetCityRequest();
                            myOuter2.setCountryId(Constants.countryId);
                            myOuter2.setStateId(Constants.stateId);
                            presenter.loadCity(myOuter2);
                        }else{
                            presenter.showInputError(Constants.CountryRequeiredMessage+" "+Constants.StateRequeiredMessage);
                        }
                    }else{
                        presenter.showInputError(Constants.InternetConnection_Fails);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.category_edittext:

                try {
                    if(MusicEarnApp.isNetworkConnected(mContext)){
                        if(Constants.airFormId !=null){
                            Outer.GetArtFormCategory myOuter2 = new Outer.GetArtFormCategory();
                            myOuter2.setArtFormId(Constants.airFormId);
                            presenter.loadAirFormCategory(myOuter2);
                        }else{
                            presenter.showInputError(Constants.AirFormRequeiredMessage);
                        }
                    }else{
                        presenter.showInputError(Constants.InternetConnection_Fails);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.type_edittext:
                try {
                    if(MusicEarnApp.isNetworkConnected(mContext)){
                        if(Constants.airFormId != null && Constants.airFormCategoryId != null){
                            Outer.GetArtFormType myOuter2 = new Outer.GetArtFormType();
                            myOuter2.setArtFormId(Constants.airFormId);
                            myOuter2.setArtFormCategoryId(Constants.airFormCategoryId);
                            presenter.loadAirFormType(myOuter2);
                        }else{
                            presenter.showInputError(Constants.AirFormRequeiredMessage+"&"+Constants.AirForCategormRequeiredMessage);
                        }
                    }else{
                        presenter.showInputError(Constants.InternetConnection_Fails);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.gredu_edittext:
                try {
                    if(MusicEarnApp.isNetworkConnected(mContext)){
                        presenter.loadAirFormGradu();
                    }else{
                        presenter.showInputError(Constants.InternetConnection_Fails);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.category_addon_edittext:
                try {
                    if(MusicEarnApp.isNetworkConnected(mContext)){
                        if(Constants.airFormId !=null){
                            Outer.GetArtFormCategory myOuter2 = new Outer.GetArtFormCategory();
                            myOuter2.setArtFormId(Constants.airFormId);
                            presenter.loadAdditionalAirFormCategory(myOuter2);
                        }else{
                            presenter.showInputError(Constants.AirFormRequeiredMessage);
                        }
                    }else{
                        presenter.showInputError(Constants.InternetConnection_Fails);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.type_addon_edittext:
                try {
                    if(MusicEarnApp.isNetworkConnected(mContext)){
                        if(Constants.airFormId !=null && Constants.airFormCategoryId !=null){
                            Outer.GetArtFormType myOuter2 = new Outer.GetArtFormType();
                            myOuter2.setArtFormId(Constants.airFormId);
                            myOuter2.setArtFormCategoryId(Constants.airFormCategoryId);
                            presenter.loadAdditionalAirFormType(myOuter2);
                        }else{
                            presenter.showInputError(Constants.AirFormRequeiredMessage+"&"+Constants.AirForCategormRequeiredMessage);
                        }
                    }else{
                        presenter.showInputError(Constants.InternetConnection_Fails);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.gredu_addon_edittext:
                try {
                    if(MusicEarnApp.isNetworkConnected(mContext)){
                        presenter.loadAdditionalAirFormGradu();
                    }else{
                        presenter.showInputError(Constants.InternetConnection_Fails);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

        }

    }

    @OnClick({R.id.user_submit})
    public void createUser(){
        if(MusicEarnApp.isNetworkConnected(MusicEarnApp.getAppContext())){
            Outer.SaveUser saveUser =new Outer.SaveUser();
            saveUser.setPrefixName(profixNameMusicianFragment.getText().toString().trim());
            saveUser.setTitleId(Constants.titleId);
            saveUser.setCountryId(Constants.countryId);
            saveUser.setStateId(Constants.stateId);
            saveUser.setCityId(Constants.cityId);
            saveUser.setAirFormId(Constants.airFormId);
            saveUser.setAirFormCategoryId(Constants.airFormCategoryId);
            saveUser.setAirFormTypeId(Constants.airFormTypeId);
            saveUser.setAirAcceretionId(Constants.airAcceretionId);
            saveUser.setAdditionalArtFormCategoryId(Constants.additionalAirFormCategoryId);
            saveUser.setAddtionalArtFormTypeId(Constants.addtionalAirFormTypeId);
            saveUser.setAddtionalAirAcceretionId(Constants.addtionalAirAcceretionId);
            saveUser.setRadiogroup_value(Constants.radiogroup_value);
            saveUser.setfName(firstNameMusicianFragment.getText().toString().trim());
            saveUser.setmName(middleNameMusicianFragment.getText().toString().trim());
            saveUser.setlName(lastNameMusicianFragment.getText().toString().trim());
            saveUser.setMobileNumber(mobileNumberMusicianFragment.getText().toString().trim());
            saveUser.setEmailId(emailMusicianFragment.getText().toString().trim());
            saveUser.setProfileImagePath(Constants.Camara.profileImagePath);
            saveUser.setBitmap(Constants.Camara.bitmap);
            saveUser.setUserTypeId("2");
            presenter.submitButtonClicked(MusicEarnApp.getAppContext(),saveUser);
        }else{
            presenter.showInputError(Constants.InternetConnection_Fails);
        }
    }

    @OnClick(R.id.image_profile)
    void imageButtonOnclick() {
        try {
            selectImage();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick({R.id.male_radio, R.id.female_radio})
    public void onRadioButtonClicked(RadioButton radioButton) {

        boolean checked = radioButton.isChecked();
        // Check which radio button was clicked
        switch (radioButton.getId()) {
            case R.id.male_radio:
                if (checked) {
                    Constants.radiogroup_value = "1";
                }
                break;
            case R.id.female_radio:
                if (checked) {
                    Constants.radiogroup_value = "2";
                }
                break;
        }
    }

    @OnClick({R.id.fab_list, R.id.fab_add})
    public void listFloatButtonOnclick(FloatingActionButton floatingActionButton) {
        switch (floatingActionButton.getId()) {
            case R.id.fab_list:
                searchFrameLayout.setVisibility(View.VISIBLE);
                parentFramagLaout.setVisibility(View.VISIBLE);
                childLinearLaout.setVisibility(View.GONE);
                fabList.setVisibility(View.GONE);
                getUserList();
                break;
            case R.id.fab_add:
                searchFrameLayout.setVisibility(View.GONE);
                parentFramagLaout.setVisibility(View.GONE);
                childLinearLaout.setVisibility(View.VISIBLE);
                fabList.setVisibility(View.VISIBLE);
               if(MusicEarnApp.isNetworkConnected(mContext)){
                    presenter.loadAirForm();
                }else{
                    presenter.showInputError(Constants.InternetConnection_Fails);
                }
                break;
        }

    }




    @Override
    public void showInputError(String s) {

        Toast.makeText(mContext, ""+s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoader() {

        MusicEarnApp.showProgress(mContext);
    }

    @Override
    public void closeLoader() {

        MusicEarnApp.closeProgress();
    }

    @Override
    public void updateUserList(List<UserResponce> userResponces) {
        try {
            usersList = new ArrayList<>();
            if (userResponces != null) {
                adapter1 = new GetUserListAdapter(getActivity(), userResponces);
                list.setAdapter(adapter1);
                usersList = userResponces; /*there is no use now */
                Constants.userModuleSync=null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void updateTitle(final List<TitleList> titleList) {

        try {
            ArrayAdapter<TitleList> dataAdapter;
            dataAdapter = new ArrayAdapter<>(getActivity(),
                    android.R.layout.simple_spinner_dropdown_item, titleList);
            new AlertDialog.Builder(getActivity())
                    .setTitle(Constants.TitleHeading)
                    .setCancelable(true)
                    .setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            titleEditText.setText(titleList.get(which).getTitleName());
                            Constants.titleId = titleList.get(which).getTitleId();
                            System.out.println(Constants.TitleHeading+Constants.titleId);
                        }
                    }).create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateCountry(final List<CountryList> countryLists) {
        try {
            System.out.println("countryListssize"+countryLists.size());

            ArrayAdapter<CountryList> dataAdapter = new ArrayAdapter<>(getActivity(),
                    android.R.layout.simple_spinner_dropdown_item, countryLists);
            new AlertDialog.Builder(getActivity())
                    .setTitle(Constants.TitleHeading)
                    .setCancelable(true)
                    .setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            countryEdittext.setText(countryLists.get(which).getName());
                            Constants.countryId = countryLists.get(which).getId();
                            System.out.println(Constants.CountryHeading+Constants.countryId);
                        }
                    }).create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateState(final List<StateList> stateLists) {
        try {
            ArrayAdapter<StateList> dataAdapter = new ArrayAdapter<>(getActivity(),
                    android.R.layout.simple_spinner_dropdown_item, stateLists);
            new AlertDialog.Builder(getActivity())
                    .setTitle(Constants.StateHeading)
                    .setCancelable(true)
                    .setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            stateEdittext.setText(stateLists.get(which).getName());
                            Constants.stateId = stateLists.get(which).getId();
                            System.out.println(Constants.StateHeading+ Constants.stateId);
                        }
                    }).create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateCity(final List<CityList> cityLists) {
        try {
            ArrayAdapter<CityList> dataAdapter = new ArrayAdapter<>(getActivity(),
                    android.R.layout.simple_spinner_dropdown_item, cityLists);
            new AlertDialog.Builder(getActivity())
                    .setTitle(Constants.CityHeading)
                    .setCancelable(true)
                    .setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            cityEdittext.setText(cityLists.get(which).getName());
                            Constants.cityId = cityLists.get(which).getId();
                            System.out.println(Constants.CityHeading+ Constants.cityId);
                        }
                    }).create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateAirForm(final List<AirFormList> airFormLists) {

        for (int i = 0; i < airFormLists.size(); i++) {
            try {
                if (airFormLists.get(i).getName().equalsIgnoreCase("Music")) {
                    Constants.airFormId = airFormLists.get(i).getId();
                    airFromEdittext.setText(airFormLists.get(i).getName());
                    System.out.println(Constants.AirFormHeading+Constants.airFormId);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void updateAirFormCategory(final List<AirFormCategoryList> airFormCategoryLists) {
        try {
            ArrayAdapter<AirFormCategoryList> dataAdapter = new ArrayAdapter<>(getActivity(),
                    android.R.layout.simple_spinner_dropdown_item, airFormCategoryLists);
            new AlertDialog.Builder(getActivity())
                    .setTitle(Constants.AirFormCategoryHeading)
                    .setCancelable(true)
                    .setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            categoryEdittext.setText(airFormCategoryLists.get(which).getName());
                            Constants.airFormCategoryId = airFormCategoryLists.get(which).getId();
                            System.out.println(Constants.AirFormCategoryHeading+ Constants.airFormCategoryId);
                        }
                    }).create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateAirFormType(final List<AirFormTypeList> airFormTypeLists) {

        try {
            ArrayAdapter<AirFormTypeList> dataAdapter = new ArrayAdapter<>(getActivity(),
                    android.R.layout.simple_spinner_dropdown_item, airFormTypeLists);
            new AlertDialog.Builder(getActivity())
                    .setTitle(Constants.AirFormTypeHeading)
                    .setCancelable(true)
                    .setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            typeEdittext.setText(airFormTypeLists.get(which).getName());
                            Constants.airFormTypeId = airFormTypeLists.get(which).getId();
                            System.out.println(Constants.AirFormTypeHeading+ Constants.airFormTypeId);
                        }
                    }).create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateAirFormGradu(final List<AirFormGreduList> airFormGreduLists) {

        try {
            ArrayAdapter<AirFormGreduList> dataAdapter = new ArrayAdapter<>(getActivity(),
                    android.R.layout.simple_spinner_dropdown_item, airFormGreduLists);
            new AlertDialog.Builder(getActivity())
                    .setTitle(Constants.AirFormGradeHeading)
                    .setCancelable(true)
                    .setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            greduEdittext.setText(airFormGreduLists.get(which).getName());
                            Constants.airAcceretionId = airFormGreduLists.get(which).getId();
                            System.out.println(Constants.AirFormGradeHeading+ Constants.airAcceretionId);
                        }
                    }).create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateAdditionalAirFormCategory(final List<AirFormCategoryList> airFormCategoryLists) {
        try {
            ArrayAdapter<AirFormCategoryList> dataAdapter;
            dataAdapter = new ArrayAdapter<>(getActivity(),
                    android.R.layout.simple_spinner_dropdown_item, airFormCategoryLists);
            new AlertDialog.Builder(getActivity())
                    .setTitle(Constants.AdditionalAirFormCategoryHeading)
                    .setCancelable(true)
                    .setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            categorySecondEdittext.setText(airFormCategoryLists.get(which).getName());
                            Constants.additionalAirFormCategoryId = airFormCategoryLists.get(which).getId();
                            System.out.println(Constants.AdditionalAirFormCategoryHeading+ Constants.additionalAirFormCategoryId);
                        }
                    }).create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateAdditionalAirFormType(final List<AirFormTypeList> airFormTypeLists) {
        try {
            ArrayAdapter<AirFormTypeList> dataAdapter = new ArrayAdapter<>(getActivity(),
                    android.R.layout.simple_spinner_dropdown_item, airFormTypeLists);
            new AlertDialog.Builder(getActivity())
                    .setTitle(Constants.AdditionalAirFormTypeHeading)
                    .setCancelable(true)
                    .setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            typeSecondEdittext.setText(airFormTypeLists.get(which).getName());
                            Constants.addtionalAirFormTypeId = airFormTypeLists.get(which).getId();
                            System.out.println(Constants.AdditionalAirFormTypeHeading+ Constants.addtionalAirFormTypeId);
                        }
                    }).create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateAdditionalAirFormGradu(final List<AirFormGreduList> airFormGreduLists) {
        try {
            ArrayAdapter<AirFormGreduList> dataAdapter = new ArrayAdapter<>(getActivity(),
                    android.R.layout.simple_spinner_dropdown_item, airFormGreduLists);
            new AlertDialog.Builder(getActivity())
                    .setTitle(Constants.AirFormGradeHeading)
                    .setCancelable(true)
                    .setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            greduSecondEdittext.setText(airFormGreduLists.get(which).getName());
                            Constants.addtionalAirAcceretionId = airFormGreduLists.get(which).getId();
                            System.out.println(Constants.AirFormGradeHeading+ Constants.addtionalAirAcceretionId);
                        }
                    }).create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateMemberList(List<MemberShipList> memberShipLists) {

    }

    @Override
    public void updateVenderOptionList(List<VenderOptionList> memberShipLists) {

    }

    /*image*/
    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    initCamera();
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
                    i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                    startActivityForResult(i, Constants.Camara.FROM_LIBRARY);
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void initCamera() {
        Utils.STORAGE availableStorage = Utils.getStorageWithFreeSpace(mContext);
        String rootPath = Utils.getRootPath(mContext, availableStorage);
        File folder = new File(rootPath);
        if (!folder.isDirectory()) {
            folder.mkdir();
        }
        File fileCamera = new File(Utils.getImagePath(mContext, availableStorage, true));
        Constants.Camara.profileImagePath = fileCamera.getPath();
        Log.d("log_tag", "profileImagePath: " + Constants.Camara.profileImagePath);
        if (!fileCamera.exists())
            try {
                fileCamera.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        Uri mImageUri = Uri.fromFile(fileCamera);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
        startActivityForResult(intent, Constants.Camara.FROM_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.Camara.FROM_CAMERA && resultCode == Activity.RESULT_OK) {
            if (Constants.Camara.profileImagePath != null && !Constants.Camara.profileImagePath.equals("")) {
                startActivityForResult(new Intent(mContext, CropperImageActivity.class)
                        .putExtra("Path", Constants.Camara.profileImagePath), Constants.Camara.ACTION_REQUEST_CROP);
            }
        }
        if (requestCode == Constants.Camara.CROP_PIC && resultCode != 0) {
            Bundle extras = data.getExtras();  // Create an instance of bundle and get the returned data
            bitmap = extras.getParcelable("data");  // get the cropped bitmap from extras
            profileImageMusicianFragment.setImageBitmap(bitmap);  // set image bitmap to image view
            profileImageMusicianFragment.setImageBitmap(RoundedShapeBitmap.getRoundedShape(bitmap, 120));
        }
        if (requestCode == Constants.Camara.FROM_LIBRARY && resultCode == Activity.RESULT_OK && null != data) {
            Constants.Camara.profileImagePath = Utils.getRealPathFromURI(mContext,data.getData());
            if (Constants.Camara.profileImagePath != null && !Constants.Camara.profileImagePath.equals("")) {
                startActivityForResult(new Intent(mContext, CropperImageActivity.class)
                        .putExtra("Path", Constants.Camara.profileImagePath), Constants.Camara.ACTION_REQUEST_CROP);
            }
        }else if (requestCode == Constants.Camara.ACTION_REQUEST_CROP && resultCode == Activity.RESULT_OK) {
            String filePath = data.getStringExtra("picture_path");
            System.out.println(" filePath>>>" + filePath);
            bitmap = Utils.decodeFile(new File(filePath));
            try {
                int rotation = BitmapUtil.getExifOrientation(filePath);
                if (bitmap != null) {
                    Matrix matrix = new Matrix();
                    matrix.setRotate(rotation);
                    bitmap = Bitmap.createBitmap(bitmap, 0, 0,
                            bitmap.getWidth(), bitmap.getHeight(),
                            matrix, true);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (bitmap != null) {
                profileImageMusicianFragment.setImageBitmap(bitmap);
            }
        }
    }

    public void getUserList() {
        try {
            if(MusicEarnApp.isNetworkConnected(mContext)){

                String userId =UniversalPreferences.getInstance().get(IConstant.SharedPref_LoginedUserid,"");
                Outer.GetUsers myOuter2 = new Outer.GetUsers();
                myOuter2.setUserTypeId("2");
                myOuter2.setUserName(userId);
                presenter.loadUserMusiciearnList(myOuter2);
            }else{
                presenter.showInputError(Constants.InternetConnection_Fails);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
