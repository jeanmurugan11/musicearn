package com.kalajitha.musicearn.fragment.user;

import android.content.Context;

import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.AirFormCategoryList;
import com.kalajitha.musicearn.model.response.AirFormGreduList;
import com.kalajitha.musicearn.model.response.AirFormList;
import com.kalajitha.musicearn.model.response.AirFormTypeList;
import com.kalajitha.musicearn.model.response.CityList;
import com.kalajitha.musicearn.model.response.CountryList;
import com.kalajitha.musicearn.model.response.MemberShipList;
import com.kalajitha.musicearn.model.response.StateList;
import com.kalajitha.musicearn.model.response.TitleList;
import com.kalajitha.musicearn.model.response.UserResponce;
import com.kalajitha.musicearn.model.response.VenderOptionList;

import java.util.List;

import rx.Observable;

public interface MusicianMVP {

    interface View {

        void showInputError(String s);
        void showLoader();
        void closeLoader();
        void updateUserList(List<UserResponce> userResponces);/*get the user list form server*/
        void updateTitle(List<TitleList> titleList);
        void updateCountry(List<CountryList>countryLists);
        void updateState(List<StateList>stateLists);
        void updateCity(List<CityList>cityLists);
        void updateAirForm(List<AirFormList>airFormLists);
        void updateAirFormCategory(List<AirFormCategoryList>airFormCategoryLists);
        void updateAirFormType(List<AirFormTypeList>airFormTypeLists);
        void updateAirFormGradu(List<AirFormGreduList>airFormGreduLists);
        /*addon*/
        void updateAdditionalAirFormCategory(List<AirFormCategoryList>airFormCategoryLists);
        void updateAdditionalAirFormType(List<AirFormTypeList>airFormTypeLists);
        void updateAdditionalAirFormGradu(List<AirFormGreduList>airFormGreduLists);
        void updateMemberList(List<MemberShipList>memberShipLists);
        void updateVenderOptionList(List<VenderOptionList>memberShipLists);
    }
    interface Presenter{

        void setView(MusicianMVP.View view);
        void rxUnsubscribe();
        void submitButtonClicked(Context context,Outer.SaveUser saveUser);

        void loadUserMusiciearnList(Outer.GetUsers myOuter2);
        void loadUserDancerList(Outer.GetUsers myOuter2);
        void loadUserTeacherList(Outer.GetUsers myOuter2);
        void loadUserStudentList(Outer.GetUsers myOuter2);
        void loadUserGuesttList(Outer.GetUsers myOuter2);
        void loadUserPresstList(Outer.GetUsers myOuter2);
        void loadUserSponserList(Outer.GetUsers myOuter2);
        void loadUserAudianceList(Outer.GetUsers myOuter2);
        void loadUservenderList(Outer.GetUsers myOuter2);

        void loadTitle();
        void loadCountry();
        void loadState(Outer.GetStateRequest myOuter2);
        void loadCity(Outer.GetCityRequest myOuter2);
        void loadAirForm();
        void loadAirFormCategory(Outer.GetArtFormCategory myOuter2);
        void loadAirFormType(Outer.GetArtFormType myOuter2);
        void loadAirFormGradu();
        void loadAdditionalAirFormCategory(Outer.GetArtFormCategory myOuter2);
        void loadAdditionalAirFormType(Outer.GetArtFormType myOuter2);
        void loadAdditionalAirFormGradu();
        void loadMemberList();
        void loadVenderOptionList();

        void showInputError(String s);

    }
    interface Model{

        Observable<List<UserResponce>> getUserModuleMusiciearnList(Outer.GetUsers myOuter2);
        Observable<List<UserResponce>> getUserModuleDancerList(Outer.GetUsers myOuter2);
        Observable<List<UserResponce>> getUserModuleTeacherList(Outer.GetUsers myOuter2);
        Observable<List<UserResponce>> getUserModuleStudentList(Outer.GetUsers myOuter2);
        Observable<List<UserResponce>> getUserModuleGuestList(Outer.GetUsers myOuter2);
        Observable<List<UserResponce>> getUserModulePressList(Outer.GetUsers myOuter2);
        Observable<List<UserResponce>> getUserModuleSponserList(Outer.GetUsers myOuter2);
        Observable<List<UserResponce>> getUserModuleAudianceList(Outer.GetUsers myOuter2);
        Observable<List<UserResponce>> getUserModuleVenderList(Outer.GetUsers myOuter2);

        Observable<List<TitleList>> getTitleList();
        Observable<List<CountryList>> getCountryList();
        Observable<List<StateList>> getStateList(Outer.GetStateRequest myOuter2);
        Observable<List<CityList>> getCityList(Outer.GetCityRequest myOuter2);
        Observable<List<AirFormList>> getAirFormList();
        Observable<List<AirFormCategoryList>> getAirFormCategoryList(Outer.GetArtFormCategory myOuter2);
        Observable<List<AirFormTypeList>> getAirFormTypeList(Outer.GetArtFormType myOuter2);
        Observable<List<AirFormGreduList>> getAirFormGraduList();
        Observable<List<MemberShipList>> getMemberList();
        Observable<List<VenderOptionList>> getVenderOptionList();

        void createUser(Context context ,Outer.SaveUser saveUser);
    }
}
