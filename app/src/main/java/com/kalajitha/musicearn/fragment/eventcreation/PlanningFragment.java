/*
 * Copyright 2015 Rudson Lima
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kalajitha.musicearn.fragment.eventcreation;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.kalajitha.musicearn.R;
import com.kalajitha.musicearn.adapter.EventListAdapter;
import com.kalajitha.musicearn.constants.Constants;
import com.kalajitha.musicearn.constants.IConstant;
import com.kalajitha.musicearn.croppings.BitmapUtil;
import com.kalajitha.musicearn.croppings.CropperImageActivity;
import com.kalajitha.musicearn.croppings.ImageViewRounded;
import com.kalajitha.musicearn.croppings.RoundedShapeBitmap;
import com.kalajitha.musicearn.croppings.Utils;
import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.ListofCreatedEvent;
import com.kalajitha.musicearn.model.response.PurposeofEventResponse;
import com.kalajitha.musicearn.network.APIServiceInterface;
import com.kalajitha.musicearn.network.volleymultiform.VolleyMultipartRequest;
import com.kalajitha.musicearn.network.volleymultiform.VolleySingleton;
import com.kalajitha.musicearn.objects.UniversalPreferences;
import com.kalajitha.musicearn.root.MusicEarnApp;
import com.kalajitha.musicearn.view.ButtonMyriadProBold;
import com.kalajitha.musicearn.view.EditViewRegular;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.Unbinder;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

import static com.kalajitha.musicearn.constants.Constants.Camara.bitmap;


public class PlanningFragment extends Fragment
        implements EventListAdapter.EventActionButtonClick {

    public static final String TAG = PlanningFragment.class.getSimpleName();
    private static final String TEXT_FRAGMENT = "TEXT_FRAGMENT";

    @BindView(R.id.event_purpose_name_edittext)
    EditViewRegular eventPurposeNameEdittext;
    @BindView(R.id.event_title_edittext)
    EditViewRegular eventTitleEdittext;
    @BindView(R.id.event_start_date_textview)
    EditViewRegular eventStartDateTextview;
    @BindView(R.id.event_end_date_textview)
    EditViewRegular eventEndDateTextview;
    @BindView(R.id.event_slogan_editview)
    EditViewRegular eventSloganEditview;
    @BindView(R.id.event_creation_avatar)
    ImageViewRounded eventCreationAvatar;
    @BindView(R.id.event_create_button)
    ButtonMyriadProBold eventCreateButton;
    @BindView(R.id.event_list)
    ListView eventList;

    View view;
    Unbinder unbinder;
    Context mContext;
    private DatePickerDialog startDatePickerDialog;
    private DatePickerDialog endDatePickerDialog;
    SimpleDateFormat dateFormatter;
    String eventPurposeNameId;

    @Inject
    APIServiceInterface apiServiceInterface;

    EventListAdapter adapter1;

    List<PurposeofEventResponse> eventPurposeResponses; /*purpose of event list*/

    List<ListofCreatedEvent> eventResponsesTable;

    public static PlanningFragment newInstance(String text) {
        PlanningFragment mFragment = new PlanningFragment();
        Bundle mBundle = new Bundle();
        mBundle.putString(TEXT_FRAGMENT, text);
        mFragment.setArguments(mBundle);
        return mFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_planning, container, false);
        view.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        unbinder = ButterKnife.bind(this, view);
        mContext = container.getContext();
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        //dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        ((MusicEarnApp) getActivity().getApplication()).getComponent().inject(this);
        getStartDate();
        getEndDate();
        listofEventCreation();
        return view;
    }

    public void callAdapter(){
        adapter1 = new EventListAdapter(mContext, eventResponsesTable);
        eventList.setAdapter(adapter1);
        adapter1.setEventActionButtonClickListener(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        ((MusicEarnApp) getActivity().getApplication()).getComponent().inject(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        //Onresure code here

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();

    }


    @OnClick(R.id.event_creation_avatar)
    void imageButtonOnclick() {
        try {
            selectImage();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @OnClick({R.id.event_start_date_textview, R.id.event_end_date_textview})
    void dataTextViewOnclick(EditViewRegular textViewRegular) {
        try {
            switch (textViewRegular.getId()) {
                case R.id.event_start_date_textview:
                    MusicEarnApp.hideSoftKeyboard(getActivity());
                    startDatePickerDialog.show();
                    break;
                case R.id.event_end_date_textview:
                    MusicEarnApp.hideSoftKeyboard(getActivity());
                    endDatePickerDialog.show();
                    break;
                default:
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*image*/
    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    initCamera();
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    File file = new File(Environment.getExternalStorageDirectory() + File.separator + "img.jpg");
                    i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                    startActivityForResult(i, Constants.Camara.FROM_LIBRARY);
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void initCamera() {
        Utils.STORAGE availableStorage = Utils.getStorageWithFreeSpace(mContext);
        String rootPath = Utils.getRootPath(mContext, availableStorage);
        File folder = new File(rootPath);
        if (!folder.isDirectory()) {
            folder.mkdir();
        }
        File fileCamera = new File(Utils.getImagePath(mContext, availableStorage, true));
        Constants.Camara.profileImagePath = fileCamera.getPath();
        Log.d("log_tag", "profileImagePath: " + Constants.Camara.profileImagePath);
        if (!fileCamera.exists())
            try {
                fileCamera.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        Uri mImageUri = Uri.fromFile(fileCamera);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
        startActivityForResult(intent, Constants.Camara.FROM_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.Camara.FROM_CAMERA && resultCode == Activity.RESULT_OK) {
            if (Constants.Camara.profileImagePath != null && !Constants.Camara.profileImagePath.equals("")) {
                startActivityForResult(new Intent(mContext, CropperImageActivity.class)
                        .putExtra("Path", Constants.Camara.profileImagePath), Constants.Camara.ACTION_REQUEST_CROP);
            }
        }
        if (requestCode == Constants.Camara.CROP_PIC && resultCode != 0) {
            Bundle extras = data.getExtras();  // Create an instance of bundle and get the returned data
            bitmap = extras.getParcelable("data");  // get the cropped bitmap from extras
            eventCreationAvatar.setImageBitmap(bitmap);  // set image bitmap to image view
            eventCreationAvatar.setImageBitmap(RoundedShapeBitmap.getRoundedShape(bitmap, 120));
        }
        if (requestCode == Constants.Camara.FROM_LIBRARY && resultCode == Activity.RESULT_OK && null != data) {
            Constants.Camara.profileImagePath = Utils.getRealPathFromURI(mContext, data.getData());
            if (Constants.Camara.profileImagePath != null && !Constants.Camara.profileImagePath.equals("")) {
                startActivityForResult(new Intent(mContext, CropperImageActivity.class)
                        .putExtra("Path", Constants.Camara.profileImagePath), Constants.Camara.ACTION_REQUEST_CROP);
            }
        } else if (requestCode == Constants.Camara.ACTION_REQUEST_CROP && resultCode == Activity.RESULT_OK) {
            String filePath = data.getStringExtra("picture_path");
            System.out.println(" filePath>>>" + filePath);
            bitmap = Utils.decodeFile(new File(filePath));
            try {
                int rotation = BitmapUtil.getExifOrientation(filePath);
                if (bitmap != null) {
                    Matrix matrix = new Matrix();
                    matrix.setRotate(rotation);
                    bitmap = Bitmap.createBitmap(bitmap, 0, 0,
                            bitmap.getWidth(), bitmap.getHeight(),
                            matrix, true);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (bitmap != null) {
                eventCreationAvatar.setImageBitmap(bitmap);
            }
        }
    }

    @OnClick(R.id.event_create_button)
    void createEventButtonOnclick() {
        if (MusicEarnApp.isNetworkConnected(mContext)) {
            if (TextUtils.isEmpty(eventPurposeNameEdittext.getText().toString().trim())) {
                Toast.makeText(mContext, "" + "Please Choose Purpose of The Event", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(eventTitleEdittext.getText().toString().trim())) {
                Toast.makeText(mContext, "" + "Please Enter Title of The Event", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(eventStartDateTextview.getText().toString().trim())) {
                Toast.makeText(mContext, "" + "Please Enter Start Date of The Event", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(eventEndDateTextview.getText().toString().trim())) {
                Toast.makeText(mContext, "" + "Please Enter End Date of The Event", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(eventSloganEditview.getText().toString().trim())) {
                Toast.makeText(mContext, "" + "Please Enter Slogan of The Event", Toast.LENGTH_SHORT).show();
            } else {
                String loginedUserId = UniversalPreferences.getInstance().get(IConstant.SharedPref_LoginedUserid, "");
                Outer.CreateEventRequest createEventRequest = new Outer.CreateEventRequest();
                createEventRequest.setEventPurposeID(eventPurposeNameId);
                createEventRequest.setEventTile(eventTitleEdittext.getText().toString());
                createEventRequest.setEventStart(eventStartDateTextview.getText().toString());
                createEventRequest.setEventEnd(eventEndDateTextview.getText().toString());
                createEventRequest.setSlogan(eventSloganEditview.getText().toString());
                createEventRequest.setUserName(loginedUserId);
                createEventRequest.setProfileImagePath(Constants.Camara.profileImagePath);
                createEventRequest.setBitmap(Constants.Camara.bitmap);
                CreateEvent(mContext, createEventRequest);
            }

        } else {
            Toast.makeText(mContext, "" + "Please Check Your Internet", Toast.LENGTH_SHORT).show();
        }
    }


    @OnClick(R.id.event_purpose_name_edittext)
    public void getPurposeoftheEvent() {

        if (MusicEarnApp.isNetworkConnected(mContext)) {

            MusicEarnApp.showProgress(mContext);

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("", "");

            } catch (JSONException e) {
                e.printStackTrace();
            }

            AndroidNetworking.post("http://45.113.189.118/MusicDev/api/Event/cmbPurposeEvent")
                    .setTag("test")
                    .addJSONObjectBody(jsonObject)
                    .setOkHttpClient(provideClient())
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {

                            MusicEarnApp.closeProgress();

                            PurposeofEventResponse eventPurposeResponse = null;
                            eventPurposeResponses = new ArrayList<PurposeofEventResponse>();

                            for (int i = 0; i < response.length(); i++) {
                                try {
                                    JSONObject jsonObject = response.getJSONObject(i);
                                    eventPurposeResponse = new PurposeofEventResponse(jsonObject.getString("id"),
                                            jsonObject.getString("purposename"));
                                    eventPurposeResponses.add(eventPurposeResponse);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            try {
                                ArrayAdapter<PurposeofEventResponse> dataAdapter = new ArrayAdapter<>(getActivity(),
                                        android.R.layout.simple_spinner_dropdown_item, eventPurposeResponses);
                                new AlertDialog.Builder(getActivity())
                                        .setTitle(Constants.StateHeading)
                                        .setCancelable(true)
                                        .setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                                eventPurposeNameEdittext.setText(eventPurposeResponses.get(which).getName());
                                                eventPurposeNameId = eventPurposeResponses.get(which).getId();
                                            }
                                        }).create().show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(ANError error) {

                            error.printStackTrace();
                            MusicEarnApp.closeProgress();
                        }
                    });
        } else {
            Toast.makeText(mContext, "" + "Please Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }


    @OnItemClick(R.id.event_list)
    void onItemSelected(int position){
        Log.e("Tracks", String.valueOf(position));
        eventPurposeNameEdittext.setText(eventResponsesTable.get(position).getEventPurpose());
        eventPurposeNameId=eventResponsesTable.get(position).getEventId();
    }


    long startTime = 0;

    private void getStartDate() {
        Calendar newCalendar = Calendar.getInstance();
        //newCalendar.add(Calendar.DATE, -1);
        //long a=System.currentTimeMillis()-1;
        //System.out.println("yesterdays date is:"+a);

        startDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                Calendar newDate = Calendar.getInstance();

                newDate.set(year, monthOfYear, dayOfMonth);

                //startTime = newDate.getTimeInMillis();

                SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE");

                Date date = new Date(year, monthOfYear, dayOfMonth - 1);

                startTime = date.getTime();

                String dayOfWeek = simpledateformat.format(date);

                System.out.println("day-->" + dayOfWeek);

                String dayName = dayOfWeek;

                eventStartDateTextview.setText(dateFormatter.format(newDate.getTime()));

            }

        }, newCalendar.get(Calendar.YEAR),
                newCalendar.get(Calendar.MONTH),
                newCalendar.get(Calendar.DAY_OF_MONTH));
        // toDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        startDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 12 * 60 * 60 * 1000);


    }

    private void getEndDate() {
        Calendar newCalendar = Calendar.getInstance();
        //newCalendar.add(Calendar.DATE, -1);
        //long a=System.currentTimeMillis()-1;
        //System.out.println("yesterdays date is:"+a);

        endDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE");
                Date date = new Date(year, monthOfYear, dayOfMonth - 1);
                String dayOfWeek = simpledateformat.format(date);
                System.out.println("day-->" + dayOfWeek);
                String dayName = dayOfWeek;
                eventEndDateTextview.setText(dateFormatter.format(newDate.getTime()));

            }

        }, newCalendar.get(Calendar.YEAR),
                newCalendar.get(Calendar.MONTH),
                newCalendar.get(Calendar.DAY_OF_MONTH));
        // toDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        //endDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 12 * 60 * 60 * 1000);

        if (startTime != 0) {
            endDatePickerDialog.getDatePicker().setMinDate(startTime - 12 * 60 * 60 * 1000);
        }

    }

    public void  CreateEvent(final Context MContext, final Outer.CreateEventRequest createEvent) {

        MusicEarnApp.showProgress(MContext);

        String url = " http://45.113.189.118/MusicDev/api/Event/CreateEvent";

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        try {
                            MusicEarnApp.closeProgress();

                            String resultResponse1 = new String(response.data);

                            try {

                                JSONObject jsonObject = new JSONObject(resultResponse1);
                                String resultResponse = jsonObject.getString("message");
                                Toast.makeText(MContext, "" + resultResponse, Toast.LENGTH_SHORT).show();

                                listofEventCreation();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                MusicEarnApp.closeProgress();

                NetworkResponse networkResponse = error.networkResponse;

                String resultResponse = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        resultResponse = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        resultResponse = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    Log.e("Error Message", result + networkResponse.statusCode);
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        resultResponse = jsonObject.getString("message");
                        listofEventCreation();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                   /* if (networkResponse.statusCode == 404) {
                        resultResponse = "Resource not found";
                    } else if (networkResponse.statusCode == 401) {
                        resultResponse = result + " Please login again";
                    } else if (networkResponse.statusCode == 400) {
                        resultResponse = result + " Check your inputs";
                    } else if (networkResponse.statusCode == 500) {
                        resultResponse = result + " Something is getting wrong";
                    }*/
                }
                try {
                    //Toast.makeText(MContext, "" + resultResponse, Toast.LENGTH_SHORT).show();
                    Log.i("Error", resultResponse);
                    error.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                try {
					/*Post Method  Form data eventPurposeID,eventTile,eventStart,eventEnd,slogan,UserName=9043147655,[file,filename]*/
                    params.put("eventPurposeID", createEvent.getEventPurposeID());/*eventPurposeID*/
                    params.put("eventTile", createEvent.getEventTile());/*eventTile*/
                    params.put("eventStart", createEvent.getEventStart());/*eventStart*/
                    params.put("eventEnd", createEvent.getEventEnd());/*eventEnd*/
                    params.put("slogan", createEvent.getSlogan()); /*slogan*/
                    params.put("UserName", createEvent.getUserName());/*UserName*/

                    for (Map.Entry<String, String> entry : params.entrySet()) {
                        System.out.println(entry);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                byte[] photoimageByte = new byte[0];
                try {
                    if (createEvent.getBitmap() != null) {
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        createEvent.getBitmap().compress(Bitmap.CompressFormat.JPEG, 100, baos);
                        photoimageByte = baos.toByteArray();
                        params.put("profileImage", new DataPart(createEvent.getProfileImagePath(), photoimageByte, "image/jpeg"));
                        System.out.println("eventcreation-->" + createEvent.getProfileImagePath());
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                return params;
            }
        };
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(MContext).addToRequestQueue(multipartRequest);
    }

    public OkHttpClient provideClient() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();

        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder().addInterceptor(interceptor).build();
    }

    private void listofEventCreation() {

        if (MusicEarnApp.isNetworkConnected(mContext)) {

            MusicEarnApp.showProgress(mContext);

            String loginedUserId = UniversalPreferences.getInstance().get(IConstant.SharedPref_LoginedUserid, "");

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("UserName", loginedUserId);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            AndroidNetworking.post("http://45.113.189.118/MusicDev/api/Event/GetEventForDataTable")
                    .addJSONObjectBody(jsonObject) // posting json
                    .setTag("test")
                    //.setPriority(Priority.MEDIUM)
                    .setOkHttpClient(provideClient())
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {

                            MusicEarnApp.closeProgress();

                            eventResponsesTable = new ArrayList<>();
                            ListofCreatedEvent eventResponse;

                            for (int i = 0; i < response.length(); i++) {
                                try {
                                    JSONObject jsonObject = response.getJSONObject(i);
                                    eventResponse = new ListofCreatedEvent(
                                            jsonObject.getString("eventId"),
                                            jsonObject.getString("eventTile"),
                                            jsonObject.getString("eventStart"),
                                            jsonObject.getString("eventEnd"),
                                            jsonObject.getString("eventPurpose"),
                                            jsonObject.getString("editCode"),
                                            jsonObject.getString("deleteCode"));

                                    eventResponsesTable.add(eventResponse);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            callAdapter();


                        }

                        @Override
                        public void onError(ANError error) {
                            // handle error
                            error.printStackTrace();
                            MusicEarnApp.closeProgress();
                        }
                    });
        } else {
            Toast.makeText(mContext, "" + "Please Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void editButtonOnClick(View v, int position) {

        Toast.makeText(mContext, ""+position, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void deleteButtonOnClick(View v, int position) {

        Toast.makeText(mContext, ""+position, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void createActivityButtonOnClick(View v, int position) {

        Toast.makeText(mContext, ""+position, Toast.LENGTH_SHORT).show();
    }
}
