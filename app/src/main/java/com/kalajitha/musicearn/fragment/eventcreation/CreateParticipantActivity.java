package com.kalajitha.musicearn.fragment.eventcreation;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.kalajitha.musicearn.R;
import com.kalajitha.musicearn.view.ButtonMyriadProBold;
import com.kalajitha.musicearn.view.ButtonMyriadProRegular;
import com.kalajitha.musicearn.view.EditViewRegular;
import com.kalajitha.musicearn.view.TextViewBold;
import com.kalajitha.musicearn.view.TextViewRegular;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jean on 23/9/17.
 */

public class CreateParticipantActivity extends Activity {
    @BindView(R.id.create_title_edittext)
    EditViewRegular createTitleEdittext;
    @BindView(R.id.create_activity_edittext)
    EditViewRegular createActivityEdittext;
    @BindView(R.id.create_purpose_name_edittext)
    EditViewRegular createPurposeNameEdittext;
    @BindView(R.id.create_role_edittext)
    EditViewRegular createRoleEdittext;
    @BindView(R.id.create_performance_edittext)
    EditViewRegular createPerformanceEdittext;
    @BindView(R.id.create_type_edittext)
    EditViewRegular createTypeEdittext;
    @BindView(R.id.create_name_edittext)
    EditViewRegular createNameEdittext;
    @BindView(R.id.budget_textview)
    TextViewBold budgetTextview;
    @BindView(R.id.create_renumeration_edittext)
    EditViewRegular createRenumerationEdittext;
    @BindView(R.id.transportation_textview)
    TextViewRegular transportationTextview;
    @BindView(R.id.transportation_layout_one)
    LinearLayout transportationLayoutOne;
    @BindView(R.id.accomadation_textview)
    TextViewRegular accomadationTextview;
    @BindView(R.id.transportation_layout_two)
    LinearLayout transportationLayoutTwo;
    @BindView(R.id.create_accomadation_edittext)
    EditViewRegular createAccomadationEdittext;
    @BindView(R.id.create_participant_button)
    ButtonMyriadProRegular createParticipantButton;
    @BindView(R.id.creation_participant_list)
    TextViewRegular creationParticipantList;
    @BindView(R.id.create_button)
    ButtonMyriadProBold createButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_participant);
        ButterKnife.bind(this);

    }
}
