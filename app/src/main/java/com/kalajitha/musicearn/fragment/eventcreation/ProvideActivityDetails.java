package com.kalajitha.musicearn.fragment.eventcreation;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.kalajitha.musicearn.R;
import com.kalajitha.musicearn.constants.Constants;
import com.kalajitha.musicearn.constants.IConstant;
import com.kalajitha.musicearn.model.response.ActivityPurposeResponse;
import com.kalajitha.musicearn.model.response.PurposeofEventResponse;
import com.kalajitha.musicearn.model.response.VenuePurposeResponse;
import com.kalajitha.musicearn.objects.UniversalPreferences;
import com.kalajitha.musicearn.root.MusicEarnApp;
import com.kalajitha.musicearn.view.ButtonMyriadProBold;
import com.kalajitha.musicearn.view.EditViewRegular;
import com.kalajitha.musicearn.view.TextViewRegular;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by jean on 23/9/17.
 */

public class ProvideActivityDetails extends Activity {
    @BindView(R.id.event_title_edittext)
    EditViewRegular eventTitleEdittext;
    @BindView(R.id.event_activity_edittext)
    EditViewRegular eventActivityEdittext;
    @BindView(R.id.event_start_date_textview)
    TextViewRegular eventStartDateTextview;
    @BindView(R.id.event_end_date_textview)
    TextViewRegular eventEndDateTextview;
    @BindView(R.id.activity_purpose_name_edittext)
    EditViewRegular activityPurposeNameEdittext;
    @BindView(R.id.venue_purpose_name_edittext)
    EditViewRegular venuePurposeNameEdittext;
    @BindView(R.id.event_create_button)
    ButtonMyriadProBold eventCreateButton;
    @BindView(R.id.event_list)
    ListView eventList;
    Context mContext;

    List<ActivityPurposeResponse> eventPurposeResponses;
    List<ActivityPurposeResponse> venuePurposeResponses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.provideactivitydetails);
        ButterKnife.bind(this);
        mContext = this;

    }

    String eventPurposeNameId;

    @OnClick(R.id.activity_purpose_name_edittext)
    public void getPurposeoftheActivity() {

        if (MusicEarnApp.isNetworkConnected(mContext)) {

            MusicEarnApp.showProgress(mContext);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("", "");

            } catch (JSONException e) {
                e.printStackTrace();
            }
            AndroidNetworking.post("http://45.113.189.118/MusicDev/api/Event/cmbPurposeActivity")
                    .setTag("test")
                    .addJSONObjectBody(jsonObject)
                    .setOkHttpClient(provideClient())
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {

                            PurposeofEventResponse eventPurposeResponse = null;
                            eventPurposeResponses = new ArrayList<ActivityPurposeResponse>();

                            for (int i = 0; i < response.length(); i++) {
                                try {
                                    JSONObject jsonObject = response.getJSONObject(i);
                                    ActivityPurposeResponse activityPurposeResponse = new ActivityPurposeResponse(
                                            jsonObject.getString("id"),
                                            jsonObject.getString("activityName"));
                                    eventPurposeResponses.add(activityPurposeResponse);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            try {
                                ArrayAdapter<ActivityPurposeResponse> dataAdapter = new ArrayAdapter<>(mContext,
                                        android.R.layout.simple_spinner_dropdown_item, eventPurposeResponses);
                                new AlertDialog.Builder(mContext)
                                        .setTitle(Constants.StateHeading)
                                        .setCancelable(true)
                                        .setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                                activityPurposeNameEdittext.setText(eventPurposeResponses.get(which).getName());
                                                 eventPurposeNameId = eventPurposeResponses.get(which).getId();
                                            }
                                        }).create().show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError error) {

                            error.printStackTrace();
                            MusicEarnApp.closeProgress();
                        }
                    });
        } else {
            Toast.makeText(mContext, "" + "Please Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }


    @OnClick(R.id.venue_purpose_name_edittext)
    public void getPurposeoftheVenue() {

        if (MusicEarnApp.isNetworkConnected(mContext)) {

            MusicEarnApp.showProgress(mContext);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("UserName", UniversalPreferences.getInstance().get(IConstant.SharedPref_LoginedUserid, ""));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            AndroidNetworking.post("http://45.113.189.118/MusicDev/api/Event/cmbVenue")
                    .setTag("test")
                    .addJSONObjectBody(jsonObject)
                    .setOkHttpClient(provideClient())
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {

                            VenuePurposeResponse venuePurposeResponse = null;
                            venuePurposeResponses = new ArrayList<ActivityPurposeResponse>();

                            for (int i = 0; i < response.length(); i++) {
                                try {
                                    JSONObject jsonObject = response.getJSONObject(i);
                                    ActivityPurposeResponse activityPurposeResponse = new ActivityPurposeResponse(
                                            jsonObject.getString("id"),
                                            jsonObject.getString("venueName"));
                                    eventPurposeResponses.add(activityPurposeResponse);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            try {
                                ArrayAdapter<ActivityPurposeResponse> dataAdapter = new ArrayAdapter<>(mContext,
                                        android.R.layout.simple_spinner_dropdown_item, venuePurposeResponses);
                                new AlertDialog.Builder(mContext)
                                        .setTitle(Constants.StateHeading)
                                        .setCancelable(true)
                                        .setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                                venuePurposeNameEdittext.setText(venuePurposeResponses.get(which).getName());
                                                eventPurposeNameId = eventPurposeResponses.get(which).getId();
                                            }
                                        }).create().show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError error) {

                            error.printStackTrace();
                            MusicEarnApp.closeProgress();
                        }
                    });
        } else {
            Toast.makeText(mContext, "" + "Please Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }

    @OnClick(R.id.event_list)
    public void getPurposeoftheActivityList() {

        if (MusicEarnApp.isNetworkConnected(mContext)) {

            MusicEarnApp.showProgress(mContext);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("", "");

            } catch (JSONException e) {
                e.printStackTrace();
            }
            AndroidNetworking.post("http://45.113.189.118/MusicDev/api/Event/cmbPurposeActivity")
                    .setTag("test")
                    .addJSONObjectBody(jsonObject)
                    .setOkHttpClient(provideClient())
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {

                            PurposeofEventResponse eventPurposeResponse = null;
                            eventPurposeResponses = new ArrayList<ActivityPurposeResponse>();

                            for (int i = 0; i < response.length(); i++) {
                                try {
                                    JSONObject jsonObject = response.getJSONObject(i);
                                    ActivityPurposeResponse activityPurposeResponse = new ActivityPurposeResponse(
                                            jsonObject.getString("id"),
                                            jsonObject.getString("activityName"));
                                    eventPurposeResponses.add(activityPurposeResponse);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            try {
                                ArrayAdapter<ActivityPurposeResponse> dataAdapter = new ArrayAdapter<>(mContext,
                                        android.R.layout.simple_spinner_dropdown_item, eventPurposeResponses);
                                new AlertDialog.Builder(mContext)
                                        .setTitle(Constants.StateHeading)
                                        .setCancelable(true)
                                        .setAdapter(dataAdapter, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                                activityPurposeNameEdittext.setText(eventPurposeResponses.get(which).getName());
                                                eventPurposeNameId = eventPurposeResponses.get(which).getId();
                                            }
                                        }).create().show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError error) {

                            error.printStackTrace();
                            MusicEarnApp.closeProgress();
                        }
                    });
        } else {
            Toast.makeText(mContext, "" + "Please Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }


    public OkHttpClient provideClient() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();

        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder().addInterceptor(interceptor).build();
    }

}
