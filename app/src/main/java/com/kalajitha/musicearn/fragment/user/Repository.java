package com.kalajitha.musicearn.fragment.user;

import android.content.Context;

import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.AirFormCategoryList;
import com.kalajitha.musicearn.model.response.AirFormGreduList;
import com.kalajitha.musicearn.model.response.AirFormList;
import com.kalajitha.musicearn.model.response.AirFormTypeList;
import com.kalajitha.musicearn.model.response.CityList;
import com.kalajitha.musicearn.model.response.CountryList;
import com.kalajitha.musicearn.model.response.MemberShipList;
import com.kalajitha.musicearn.model.response.StateList;
import com.kalajitha.musicearn.model.response.TitleList;
import com.kalajitha.musicearn.model.response.UserResponce;
import com.kalajitha.musicearn.model.response.VenderOptionList;

import java.util.List;

import rx.Observable;

/**
 * Created by supercomputer on 4/7/17.
 */

public interface Repository {

    /*Userlist Repository*/
    Observable<List<UserResponce>> getUserMusiciearnListFromMemory();
    Observable<List<UserResponce>> getUserMusiciearnListFromNetwork(Outer.GetUsers myOuter2);
    Observable<List<UserResponce>> getUserMusiciearnListData(Outer.GetUsers myOuter2);

    /*Dancerlist from Repository*/
    Observable<List<UserResponce>> getUserDancerListFromMemory();
    Observable<List<UserResponce>> getUserDancerListFromNetwork(Outer.GetUsers myOuter2);
    Observable<List<UserResponce>> getUserDancerListData(Outer.GetUsers myOuter2);

    /*Teacherlist from Repository*/
    Observable<List<UserResponce>> getUserTeacherListFromMemory();
    Observable<List<UserResponce>> getUserTeacherListFromNetwork(Outer.GetUsers myOuter2);
    Observable<List<UserResponce>> getUserTeacherListData(Outer.GetUsers myOuter2);

    /*Studentlist from Repository*/
    Observable<List<UserResponce>> getUserStudentListFromMemory();
    Observable<List<UserResponce>> getUserStudentListFromNetwork(Outer.GetUsers myOuter2);
    Observable<List<UserResponce>> getUserStudentListData(Outer.GetUsers myOuter2);

    /*Guestlist from Repository*/
    Observable<List<UserResponce>> getUserGuestListFromMemory();
    Observable<List<UserResponce>> getUserGuestListFromNetwork(Outer.GetUsers myOuter2);
    Observable<List<UserResponce>> getUserGuestListData(Outer.GetUsers myOuter2);

    /*Presslist from Repository*/
    Observable<List<UserResponce>> getUserPressListFromMemory();
    Observable<List<UserResponce>> getUserPressListFromNetwork(Outer.GetUsers myOuter2);
    Observable<List<UserResponce>> getUserPressListData(Outer.GetUsers myOuter2);

    /*Sponserlist from Repository*/
    Observable<List<UserResponce>> getUserSponserListFromMemory();
    Observable<List<UserResponce>> getUserSponserListFromNetwork(Outer.GetUsers myOuter2);
    Observable<List<UserResponce>> getUserSponserListData(Outer.GetUsers myOuter2);

    /*Audiance from Repository*/
    Observable<List<UserResponce>> getUserAudianceListFromMemory();
    Observable<List<UserResponce>> getUserAudianceListFromNetwork(Outer.GetUsers myOuter2);
    Observable<List<UserResponce>> getUserAudianceListData(Outer.GetUsers myOuter2);

    /*Vender from Repository*/
    Observable<List<UserResponce>> getUserVenderListFromMemory();
    Observable<List<UserResponce>> getUserVenderListFromNetwork(Outer.GetUsers myOuter2);
    Observable<List<UserResponce>> getUserVenderListData(Outer.GetUsers myOuter2);

    /*TitleHeading Repository*/
    Observable<List<TitleList>> getTitleFromMemory();
    Observable<List<TitleList>> getTitleFromNetwork();
    Observable<List<TitleList>> getTitleData();

/*CountryHeading*/
    Observable<List<CountryList>> getcountryFromMemory();
    Observable<List<CountryList>> getcountryFromNetwork();
    Observable<List<CountryList>> getcountryData();

    /*State*/
    Observable<List<StateList>> getStateFromMemory();
    Observable<List<StateList>> getStateFromNetwork(Outer.GetStateRequest myOuter2);
    Observable<List<StateList>> getStateData(Outer.GetStateRequest myOuter2);

    /*City*/
    Observable<List<CityList>> getCityFromMemory();
    Observable<List<CityList>> getCityFromNetwork(Outer.GetCityRequest myOuter2 );
    Observable<List<CityList>> getcityData(Outer.GetCityRequest myOuter2);

    /*Air Form*/
    Observable<List<AirFormList>> getAirFormMemory();
    Observable<List<AirFormList>> getAirFromNetwork();
    Observable<List<AirFormList>> getAirFormData();

    /*Air Form Category*/
    Observable<List<AirFormCategoryList>> getAirFormCategoryFromMemory();
    Observable<List<AirFormCategoryList>> getAirFormCategoryFromNewwork(Outer.GetArtFormCategory myOuter2);
    Observable<List<AirFormCategoryList>> getAirFormCategoryData(Outer.GetArtFormCategory myOuter2);

    /*Air Form type*/
    Observable<List<AirFormTypeList>> getAirFormTypeFromMemory();
    Observable<List<AirFormTypeList>> getAirFormTypeFromNetwork(Outer.GetArtFormType myOuter2);
    Observable<List<AirFormTypeList>> getAirFormTypeData(Outer.GetArtFormType myOuter2);

    /*Air Form Gradu*/
    Observable<List<AirFormGreduList>> getairFormGraduFromMemory();
    Observable<List<AirFormGreduList>> getairFormGraduNetwork();
    Observable<List<AirFormGreduList>> getairFormGraduData();


    /*Air Form Member*/
    Observable<List<MemberShipList>> getMemberFromMemory();
    Observable<List<MemberShipList>> getMemberNetwork();
    Observable<List<MemberShipList>> getMemberData();


    /*Air Form Vender*/
    Observable<List<VenderOptionList>> getVenderOptionFromMemory();
    Observable<List<VenderOptionList>> getVenderOptionNetwork();
    Observable<List<VenderOptionList>> getvenderOptionData();

    void saveUser(Context context,Outer.SaveUser saveUser);




}
