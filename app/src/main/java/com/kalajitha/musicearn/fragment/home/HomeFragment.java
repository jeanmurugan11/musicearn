package com.kalajitha.musicearn.fragment.home;

import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.kalajitha.musicearn.R;
import com.kalajitha.musicearn.adapter.CustomAdapter;
import com.kalajitha.musicearn.navigationliveo.RowItem;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    private static final String TEXT_FRAGMENT = "TEXT_FRAGMENT";
    ListView listView;
    List<RowItem> rowItems;
    public static  final String TAG=HomeFragment.class.getSimpleName();
    int[] imageId ={R.drawable.newsdahboard,R.drawable.recent,R.drawable.timeline,R.drawable.upcomingevents};
    Context mContext;

    public static HomeFragment newInstance(String text){
        HomeFragment mFragment = new HomeFragment();
        Bundle mBundle = new Bundle();
        mBundle.putString(TEXT_FRAGMENT, text);
        mFragment.setArguments(mBundle);
        return mFragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        rootView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT ));


        listView=(ListView)rootView.findViewById(R.id.listView);

        rowItems=new ArrayList<RowItem>() ;

        for (int i = 0; i < imageId.length; i++) {
            RowItem item = new RowItem(imageId[i]);
            System.out.println("listitem---->"+item.getImageId());
            rowItems.add(item);
        }

        mContext=container.getContext();

        CustomAdapter adapter=new CustomAdapter(mContext,rowItems);
        listView.setAdapter(adapter);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().setTitle(getArguments().getString(TEXT_FRAGMENT));

    }

}