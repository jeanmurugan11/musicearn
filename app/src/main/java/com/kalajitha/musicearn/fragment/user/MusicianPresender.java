package com.kalajitha.musicearn.fragment.user;

import android.content.Context;

import com.kalajitha.musicearn.constants.Constants;
import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.AirFormCategoryList;
import com.kalajitha.musicearn.model.response.AirFormGreduList;
import com.kalajitha.musicearn.model.response.AirFormList;
import com.kalajitha.musicearn.model.response.AirFormTypeList;
import com.kalajitha.musicearn.model.response.CityList;
import com.kalajitha.musicearn.model.response.CountryList;
import com.kalajitha.musicearn.model.response.MemberShipList;
import com.kalajitha.musicearn.model.response.StateList;
import com.kalajitha.musicearn.model.response.TitleList;
import com.kalajitha.musicearn.model.response.UserResponce;
import com.kalajitha.musicearn.model.response.VenderOptionList;
import com.kalajitha.musicearn.utility.Validator;

import java.util.List;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;



public class MusicianPresender implements MusicianMVP.Presenter {

    private MusicianMVP.View view;

    private Subscription subscription=null;

    private MusicianMVP.Model model;


    public MusicianPresender(MusicianMVP.Model model) {
        this.model = model;
    }

    @Override
    public void setView(MusicianMVP.View view) {

        this.view=view;
    }

    @Override
    public void rxUnsubscribe() {

        if(subscription !=null){
            if(!subscription.isUnsubscribed()){
                subscription.unsubscribe();
            }
        }
    }

    @Override
    public void submitButtonClicked(Context context, Outer.SaveUser saveUser) {

        if(view != null){
            System.out.println("userTypeId-->"+saveUser.getUserTypeId());
            String userTypeId=saveUser.getUserTypeId();

            if(saveUser.getPrefixName().isEmpty()){
                view.showInputError(Constants.ProfixNameRequeiredMessage);
            }
            else if(saveUser.getTitleId() == null){
                view.showInputError(Constants.TittleRequeiredMessage);
            }
            else if(saveUser.getfName().isEmpty()){
                view.showInputError(Constants.FirstNameRequeiredMessage);
            }
            else if(saveUser.getmName().isEmpty()){
                view.showInputError(Constants.MiddleNameRequeiredMessage);
            }
            else if(saveUser.getlName().isEmpty()){
                view.showInputError(Constants.LastNameRequeiredMessage);
            }
            else if(saveUser.getMobileNumber().isEmpty() || saveUser.getMobileNumber().length()<10){
                view.showInputError(Constants.PhoneNumberRequeiredMessage);
            }
            else if(!Validator.isEmailValid(saveUser.getEmailId())){
                view.showInputError(Constants.EmailRequeiredMessage);
            }
            else if(saveUser.getCountryId() == null){
                view.showInputError(Constants.CountryRequeiredMessage);
            }
            else if(saveUser.getStateId() == null){
                view.showInputError(Constants.StateRequeiredMessage);
            }
            else if(saveUser.getCityId() == null){
                view.showInputError(Constants.CityRequeiredMessage);
            }
            else if ("6".equals(userTypeId) || "7".equals(userTypeId)
                    || "8".equals(userTypeId) || "9".equals(userTypeId) || "10".equals(userTypeId)) {

                if("9".equals(userTypeId)) {
                    if(Constants.memberShopAvailablelity) {
                        if(saveUser.getMemberShipId() ==null){
                            view.showInputError(Constants.MemberShipIdRequeiredMessage);
                        }
                        else{
                            model.createUser(context,saveUser);
                        }
                    }
                    else{
                        model.createUser(context,saveUser);
                    }
                }else if("10".equals(userTypeId)) {

                    if(Constants.memberShopAvailablelity) {
                        if(saveUser.getMemberShipId() ==null){
                            view.showInputError(Constants.MemberShipIdRequeiredMessage);
                        }
                        else{
                            model.createUser(context,saveUser);
                        }
                    }
                    else{
                        model.createUser(context,saveUser);
                    }
                }else {
                    model.createUser(context,saveUser);
                }
            } else {
                try {
                    if(saveUser.getAirFormId() == null){
                        view.showInputError(Constants.AirFormRequeiredMessage);
                    }
                    else if(saveUser.getAirFormCategoryId() == null){
                        view.showInputError(Constants.AirForCategormRequeiredMessage);
                    }
                    else if(saveUser.getAirFormTypeId() == null){
                        view.showInputError(Constants.AirForTypeRequeiredMessage);
                    }
                    else if(saveUser.getAirAcceretionId() == null){
                        view.showInputError(Constants.AirForGradeRequeiredMessage);
                    }
                    else{
                        model.createUser(context,saveUser);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

    @Override
    public void loadUserMusiciearnList(Outer.GetUsers myOuter2) {

        if(view != null){
            view.showLoader();
        }
        subscription=model.getUserModuleMusiciearnList(myOuter2)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<UserResponce>>() {
                    @Override
                    public void onCompleted() {
                        if(view != null) view.closeLoader();
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view!=null){
                            view.closeLoader();
                            view.showInputError(Constants.UserListErrorMessage);
                        }
                    }
                    @Override
                    public void onNext(List<UserResponce> userResponce) {
                        view.updateUserList(userResponce);
                    }
                });
    }

    @Override
    public void loadUserDancerList(Outer.GetUsers myOuter2) {
        if(view != null){
            view.showLoader();
        }
        subscription=model.getUserModuleDancerList(myOuter2)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<UserResponce>>() {
                    @Override
                    public void onCompleted() {
                        if(view != null) view.closeLoader();
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view!=null){
                            view.closeLoader();
                            view.showInputError(Constants.UserListErrorMessage);
                        }
                    }
                    @Override
                    public void onNext(List<UserResponce> userResponce) {
                        view.updateUserList(userResponce);
                    }
                });
    }

    @Override
    public void loadUserTeacherList(Outer.GetUsers myOuter2) {

        if(view != null){
            view.showLoader();
        }
        subscription=model.getUserModuleTeacherList(myOuter2)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<UserResponce>>() {
                    @Override
                    public void onCompleted() {
                        if(view != null) view.closeLoader();
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view!=null){
                            view.closeLoader();
                            view.showInputError(Constants.UserListErrorMessage);
                        }
                    }
                    @Override
                    public void onNext(List<UserResponce> userResponce) {
                        view.updateUserList(userResponce);
                    }
                });
    }

    @Override
    public void loadUserStudentList(Outer.GetUsers myOuter2) {
        if(view != null){
            view.showLoader();
        }
        subscription=model.getUserModuleStudentList(myOuter2)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<UserResponce>>() {
                    @Override
                    public void onCompleted() {
                        if(view != null) view.closeLoader();
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view!=null){
                            view.closeLoader();
                            view.showInputError(Constants.UserListErrorMessage);
                        }
                    }
                    @Override
                    public void onNext(List<UserResponce> userResponce) {
                        view.updateUserList(userResponce);
                    }
                });
    }

    @Override
    public void loadUserGuesttList(Outer.GetUsers myOuter2) {
        if(view != null){
            view.showLoader();
        }
        subscription=model.getUserModuleGuestList(myOuter2)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<UserResponce>>() {
                    @Override
                    public void onCompleted() {
                        if(view != null) view.closeLoader();
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view!=null){
                            view.closeLoader();
                            view.showInputError(Constants.UserListErrorMessage);
                        }
                    }
                    @Override
                    public void onNext(List<UserResponce> userResponce) {
                        view.updateUserList(userResponce);
                    }
                });
    }

    @Override
    public void loadUserPresstList(Outer.GetUsers myOuter2) {
        if(view != null){
            view.showLoader();
        }
        subscription=model.getUserModulePressList(myOuter2)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<UserResponce>>() {
                    @Override
                    public void onCompleted() {
                        if(view != null) view.closeLoader();
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view!=null){
                            view.closeLoader();
                            view.showInputError(Constants.UserListErrorMessage);
                        }
                    }
                    @Override
                    public void onNext(List<UserResponce> userResponce) {
                        view.updateUserList(userResponce);
                    }
                });
    }

    @Override
    public void loadUserSponserList(Outer.GetUsers myOuter2) {
        if(view != null){
            view.showLoader();
        }
        subscription=model.getUserModuleSponserList(myOuter2)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<UserResponce>>() {
                    @Override
                    public void onCompleted() {
                        if(view != null) view.closeLoader();
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view!=null){
                            view.closeLoader();
                            view.showInputError(Constants.UserListErrorMessage);
                        }
                    }
                    @Override
                    public void onNext(List<UserResponce> userResponce) {
                        view.updateUserList(userResponce);
                    }
                });
    }

    @Override
    public void loadUserAudianceList(Outer.GetUsers myOuter2) {
        if(view != null){
            view.showLoader();
        }
        subscription=model.getUserModuleAudianceList(myOuter2)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<UserResponce>>() {
                    @Override
                    public void onCompleted() {
                        if(view != null) view.closeLoader();
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view!=null){
                            view.closeLoader();
                            view.showInputError(Constants.UserListErrorMessage);
                        }
                    }
                    @Override
                    public void onNext(List<UserResponce> userResponce) {
                        view.updateUserList(userResponce);
                    }
                });
    }

    @Override
    public void loadUservenderList(Outer.GetUsers myOuter2) {
        if(view != null){
            view.showLoader();
        }
        subscription=model.getUserModuleVenderList(myOuter2)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<UserResponce>>() {
                    @Override
                    public void onCompleted() {
                        if(view != null) view.closeLoader();
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view!=null){
                            view.closeLoader();
                            view.showInputError(Constants.UserListErrorMessage);
                        }
                    }
                    @Override
                    public void onNext(List<UserResponce> userResponce) {
                        view.updateUserList(userResponce);
                    }
                });
    }

    @Override
    public void loadTitle() {

        if(view != null){
            view.showLoader();
        }
        subscription=model.getTitleList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<TitleList>>() {
                    @Override
                    public void onCompleted() {
                        if(view != null) view.closeLoader();
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view!=null){
                            view.closeLoader();
                            view.showInputError(Constants.TitleErrorMessage);
                        }
                    }

                    @Override
                    public void onNext(List<TitleList> titleLists) {

                        view.updateTitle(titleLists);
                    }
                });
    }

    @Override
    public void loadCountry() {

        if(view != null){
            view.showLoader();
        }
        subscription=model.getCountryList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<CountryList>>() {
                    @Override
                    public void onCompleted() {
                        if(view != null) view.closeLoader();
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view != null){
                            view.closeLoader();
                            view.showInputError(Constants.CountryErrorMessage);
                        }
                    }
                    @Override
                    public void onNext(List<CountryList> countryLists) {
                        view.updateCountry(countryLists);
                    }
                });
    }

    @Override
    public void loadState(Outer.GetStateRequest myOuter2) {

        if(view != null){
            view.showLoader();
        }
        subscription=model.getStateList(myOuter2)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<StateList>>() {
                    @Override
                    public void onCompleted() {
                        if(view != null){
                            view.closeLoader();
                        }
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view != null){
                            view.closeLoader();
                            view.showInputError(Constants.StateErrorMessage);
                        }
                    }

                    @Override
                    public void onNext(List<StateList> stateLists) {

                        view.updateState(stateLists);
                    }
                });
    }

    @Override
    public void loadCity(Outer.GetCityRequest myOuter2) {

        if(view!=null){
            view.showLoader();
        }
        subscription=model.getCityList(myOuter2)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<CityList>>() {
                    @Override
                    public void onCompleted() {
                        if(view!=null){
                            view.closeLoader();
                        }
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view!=null){
                            view.closeLoader();
                            view.showInputError(Constants.CityErrorMessage);
                        }
                    }
                    @Override
                    public void onNext(List<CityList> cityLists) {

                        view.updateCity(cityLists);
                    }
                });
    }

    @Override
    public void loadAirForm() {

        if(view!=null){
            view.showLoader();
        }
        subscription=model.getAirFormList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<AirFormList>>() {
                    @Override
                    public void onCompleted() {
                        if(view!=null){
                            view.closeLoader();
                        }
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view!=null){
                            view.closeLoader();
                            view.showInputError(Constants.AirFormErrorMessage);
                        }
                    }
                    @Override
                    public void onNext(List<AirFormList> airFormList) {

                        view.updateAirForm(airFormList);
                    }
                });
    }

    @Override
    public void loadAirFormCategory(Outer.GetArtFormCategory myOuter2) {

        if(view!=null){
            view.showLoader();
        }
        subscription=model.getAirFormCategoryList(myOuter2)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<AirFormCategoryList>>() {
                    @Override
                    public void onCompleted() {
                        if(view!=null){
                            view.closeLoader();
                        }
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view!=null){
                            view.closeLoader();
                            view.showInputError(Constants.AirFormCategoryErrorMessage);
                        }
                    }

                    @Override
                    public void onNext(List<AirFormCategoryList> airFormCategoryLists) {

                        view.updateAirFormCategory(airFormCategoryLists);
                    }
                });
    }

    @Override
    public void loadAirFormType(Outer.GetArtFormType myOuter2) {

        if(view!=null){
            view.showLoader();
        }
        subscription=model.getAirFormTypeList(myOuter2)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<AirFormTypeList>>() {
                    @Override
                    public void onCompleted() {
                        if(view!=null){
                            view.closeLoader();
                        }
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view!=null){
                            view.closeLoader();
                            view.showInputError(Constants.CityErrorMessage);
                        }
                    }
                    @Override
                    public void onNext(List<AirFormTypeList> airFormTypeLists) {

                        view.updateAirFormType(airFormTypeLists);
                    }
                });
    }

    @Override
    public void loadAirFormGradu() {
        if(view!=null){
            view.showLoader();
        }
        subscription=model.getAirFormGraduList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<AirFormGreduList>>() {
                    @Override
                    public void onCompleted() {
                        if(view!=null){
                            view.closeLoader();
                        }
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view!=null){
                            view.closeLoader();
                            view.showInputError(Constants.AirFormGradeErrorMessage);
                        }
                    }
                    @Override
                    public void onNext(List<AirFormGreduList> airFormGreduLists) {

                        view.updateAirFormGradu(airFormGreduLists);
                    }
                });
    }

    @Override
    public void loadAdditionalAirFormCategory(Outer.GetArtFormCategory myOuter2) {

        if(view!=null){
            view.showLoader();
        }
        subscription=model.getAirFormCategoryList(myOuter2)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<AirFormCategoryList>>() {
                    @Override
                    public void onCompleted() {
                        if(view!=null){
                            view.closeLoader();
                        }
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view!=null){
                            view.closeLoader();
                            view.showInputError(Constants.AirFormCategoryErrorMessage);
                        }
                    }
                    @Override
                    public void onNext(List<AirFormCategoryList> airFormCategoryLists) {

                        view.updateAdditionalAirFormCategory(airFormCategoryLists);
                    }
                });
    }

    @Override
    public void loadAdditionalAirFormType(Outer.GetArtFormType myOuter2) {
        if(view!=null){
            view.showLoader();
        }
        subscription=model.getAirFormTypeList(myOuter2)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<AirFormTypeList>>() {
                    @Override
                    public void onCompleted() {
                        if(view!=null){
                            view.closeLoader();
                        }
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view!=null){
                            view.closeLoader();
                            view.showInputError(Constants.CityErrorMessage);
                        }
                    }
                    @Override
                    public void onNext(List<AirFormTypeList> airFormTypeLists) {

                        view.updateAdditionalAirFormType(airFormTypeLists);
                    }
                });
    }

    @Override
    public void loadAdditionalAirFormGradu() {
        if(view!=null){
            view.showLoader();
        }
        subscription=model.getAirFormGraduList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<AirFormGreduList>>() {
                    @Override
                    public void onCompleted() {
                        if(view!=null){
                            view.closeLoader();
                        }
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view!=null){
                            view.closeLoader();
                            view.showInputError(Constants.AirFormGradeErrorMessage);
                        }
                    }
                    @Override
                    public void onNext(List<AirFormGreduList> airFormGreduLists) {

                        view.updateAdditionalAirFormGradu(airFormGreduLists);
                    }
                });
    }

    @Override
    public void loadMemberList() {

        if(view!=null){
            view.showLoader();
        }
        subscription=model.getMemberList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<MemberShipList>>() {
                    @Override
                    public void onCompleted() {
                        if(view!=null){
                            view.closeLoader();
                        }
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view!=null){
                            view.closeLoader();
                            view.showInputError(Constants.MemberErrorMessage);
                        }
                    }
                    @Override
                    public void onNext(List<MemberShipList> airFormGreduLists) {

                        view.updateMemberList(airFormGreduLists);
                    }
                });
    }

    @Override
    public void loadVenderOptionList() {

        if(view!=null){
            view.showLoader();
        }
        subscription=model.getVenderOptionList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<VenderOptionList>>() {
                    @Override
                    public void onCompleted() {
                        if(view!=null){
                            view.closeLoader();
                        }
                    }
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if(view!=null){
                            view.closeLoader();
                            view.showInputError(Constants.VenderOpionErrorMessage);
                        }
                    }
                    @Override
                    public void onNext(List<VenderOptionList> venderOptionLists) {

                        view.updateVenderOptionList(venderOptionLists);
                    }
                });
    }

    @Override
    public void showInputError(String s) {

        view.showInputError(s);
    }
}
