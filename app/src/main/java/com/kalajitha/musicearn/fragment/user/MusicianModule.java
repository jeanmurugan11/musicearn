package com.kalajitha.musicearn.fragment.user;

import com.kalajitha.musicearn.network.APIServiceInterface;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by supercomputer on 4/7/17.
 */
@Module
public class MusicianModule {

    @Provides
    public MusicianMVP.Presenter provideMusicianFragmentPresenter(MusicianMVP.Model model){

        return new MusicianPresender(model);
    }

    @Provides
    public MusicianMVP.Model provideMusicianFragmentModel(Repository repository){

        return new MusicianModel(repository);
    }


    @Singleton
    @Provides
    public Repository provideRepo(APIServiceInterface movieApiService) {
        return new MusicianRepository(movieApiService);
    }
}
