package com.kalajitha.musicearn.fragment.user;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.kalajitha.musicearn.constants.Constants;
import com.kalajitha.musicearn.constants.IConstant;
import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.AirFormCategoryList;
import com.kalajitha.musicearn.model.response.AirFormGreduList;
import com.kalajitha.musicearn.model.response.AirFormList;
import com.kalajitha.musicearn.model.response.AirFormTypeList;
import com.kalajitha.musicearn.model.response.CityList;
import com.kalajitha.musicearn.model.response.CountryList;
import com.kalajitha.musicearn.model.response.MemberShipList;
import com.kalajitha.musicearn.model.response.StateList;
import com.kalajitha.musicearn.model.response.TitleList;
import com.kalajitha.musicearn.model.response.UserResponce;
import com.kalajitha.musicearn.model.response.VenderOptionList;
import com.kalajitha.musicearn.network.APIServiceInterface;
import com.kalajitha.musicearn.network.IConstantsV2;
import com.kalajitha.musicearn.network.volleymultiform.VolleyMultipartRequest;
import com.kalajitha.musicearn.network.volleymultiform.VolleySingleton;
import com.kalajitha.musicearn.objects.UniversalPreferences;
import com.kalajitha.musicearn.root.MusicEarnApp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.functions.Action1;


/**
 * Created by supercomputer on 4/7/17.
 */

public class MusicianRepository implements Repository {

    APIServiceInterface apiServiceInterface;
    private List<TitleList> titleLists;
    private List<CountryList> countryLists;
    private List<StateList> stateLists;
    private List<CityList> cityLists;
    private List<AirFormList> airFormLists;
    private List<AirFormCategoryList> airFormCategoryLists;
    private List<AirFormTypeList> airFormTypeLists;
    private List<AirFormGreduList> airFormGreduLists;
    private List<MemberShipList> memberShipListsLists;
    private List<VenderOptionList> venderOptionLists;

    /*User  ModuleFragment List*/
    private List<UserResponce> userMusicieanList;
    private List<UserResponce> userDancerList;
    private List<UserResponce> userTeacher;
    private List<UserResponce> userStudent;
    private List<UserResponce> userGuest;
    private List<UserResponce> userPress;
    private List<UserResponce> userSponser;
    private List<UserResponce> userAudiance;
    private List<UserResponce> userVender;

    private long timestamp;
    private static final long STALE_MS = 20 * 10000; // Data is stale after 20minut

    public MusicianRepository(APIServiceInterface apiServiceInterface) {
        this.apiServiceInterface = apiServiceInterface;
        this.timestamp = System.currentTimeMillis();
        titleLists = new ArrayList<>();
        countryLists = new ArrayList<>();
        stateLists = new ArrayList<>();
        cityLists = new ArrayList<>();
        airFormLists = new ArrayList<>();
        airFormCategoryLists = new ArrayList<>();
        airFormTypeLists = new ArrayList<>();
        airFormGreduLists = new ArrayList<>();
        memberShipListsLists = new ArrayList<>();
        venderOptionLists = new ArrayList<>();
        /*UserModule*/
        userMusicieanList =new ArrayList<>();
        userDancerList =new ArrayList<>();
        userTeacher =new ArrayList<>();
        userStudent =new ArrayList<>();
        userGuest =new ArrayList<>();
        userPress =new ArrayList<>();
        userSponser =new ArrayList<>();
        userAudiance =new ArrayList<>();
        userVender =new ArrayList<>();
    }

    public boolean isUpToDate() {
        return System.currentTimeMillis() - timestamp < STALE_MS;
    }


    /*---------Musician Start-------------*/

    @Override
    public Observable<List<UserResponce>> getUserMusiciearnListFromMemory() {
        if (isUpToDate() && userMusicieanList.size() > 0) {
            return Observable.just(userMusicieanList);
        } else {
                timestamp = System.currentTimeMillis();
                userMusicieanList.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<List<UserResponce>> getUserMusiciearnListFromNetwork(Outer.GetUsers myOuter2) {
        Observable<List<UserResponce>>listObservable=apiServiceInterface.getUserList(myOuter2);
        return listObservable.doOnNext(new Action1<List<UserResponce>>() {
            @Override
            public void call(List<UserResponce> userResponces) {
                userMusicieanList = userResponces;
            }
        });
    }

    @Override
    public Observable<List<UserResponce>> getUserMusiciearnListData(Outer.GetUsers myOuter2) {

        if(Constants.userModuleSync !=null){
            if(Constants.userModuleSync.equals("2")){
                return getUserMusiciearnListFromNetwork(myOuter2);
            }else{
                return getUserMusiciearnListFromMemory().switchIfEmpty(getUserMusiciearnListFromNetwork(myOuter2));
            }
        }else
            return getUserMusiciearnListFromMemory().switchIfEmpty(getUserMusiciearnListFromNetwork(myOuter2));
    }

    /*-------------Musician End--------------*/



    /*---------Dancer start---------*/

    @Override
    public Observable<List<UserResponce>> getUserDancerListFromMemory() {
        if (isUpToDate() && userDancerList.size() > 0) {
            return Observable.just(userDancerList);
        } else {
            timestamp = System.currentTimeMillis();
            userDancerList.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<List<UserResponce>> getUserDancerListFromNetwork(Outer.GetUsers myOuter2) {
        Observable<List<UserResponce>>listObservable=apiServiceInterface.getUserList(myOuter2);
        return listObservable.doOnNext(new Action1<List<UserResponce>>() {
            @Override
            public void call(List<UserResponce> userResponces) {
                userDancerList = userResponces;
            }
        });
    }

    @Override
    public Observable<List<UserResponce>> getUserDancerListData(Outer.GetUsers myOuter2) {

        if(Constants.userModuleSync !=null){
            if(Constants.userModuleSync.equals("12")){
                return getUserDancerListFromNetwork(myOuter2);
            }else{
                return getUserDancerListFromMemory().switchIfEmpty(getUserDancerListFromNetwork(myOuter2));
            }
        }else
            return getUserDancerListFromMemory().switchIfEmpty(getUserDancerListFromNetwork(myOuter2));
    }


    /*---------Dancer end---------*/



    /*-----Teacher start-------*/

    @Override
    public Observable<List<UserResponce>> getUserTeacherListFromMemory() {
        if (isUpToDate() && userTeacher.size() > 0) {
            return Observable.just(userTeacher);
        } else {
            timestamp = System.currentTimeMillis();
            userTeacher.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<List<UserResponce>> getUserTeacherListFromNetwork(Outer.GetUsers myOuter2) {
        Observable<List<UserResponce>>listObservable=apiServiceInterface.getUserList(myOuter2);
        return listObservable.doOnNext(new Action1<List<UserResponce>>() {
            @Override
            public void call(List<UserResponce> userResponces) {
                userTeacher = userResponces;
            }
        });
    }

    @Override
    public Observable<List<UserResponce>> getUserTeacherListData(Outer.GetUsers myOuter2) {
        if(Constants.userModuleSync !=null){
            if(Constants.userModuleSync.equals("4")){
                return getUserTeacherListFromNetwork(myOuter2);
            }else{
                return getUserTeacherListFromMemory().switchIfEmpty(getUserTeacherListFromNetwork(myOuter2));
            }
        }else
            return getUserTeacherListFromMemory().switchIfEmpty(getUserTeacherListFromNetwork(myOuter2));
    }

       /*-----Teacher end-------*/

    /*-----Student start-------*/

    @Override
    public Observable<List<UserResponce>> getUserStudentListFromMemory() {
        if (isUpToDate() && userStudent.size() > 0) {
            return Observable.just(userStudent);
        } else {
            timestamp = System.currentTimeMillis();
            userStudent.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<List<UserResponce>> getUserStudentListFromNetwork(Outer.GetUsers myOuter2) {
        Observable<List<UserResponce>>listObservable=apiServiceInterface.getUserList(myOuter2);
        return listObservable.doOnNext(new Action1<List<UserResponce>>() {
            @Override
            public void call(List<UserResponce> userResponces) {
                userStudent = userResponces;
            }
        });
    }

    @Override
    public Observable<List<UserResponce>> getUserStudentListData(Outer.GetUsers myOuter2) {
        if(Constants.userModuleSync !=null){
            if(Constants.userModuleSync.equals("5")){
                return getUserStudentListFromNetwork(myOuter2);
            }else{
                return getUserStudentListFromMemory().switchIfEmpty(getUserStudentListFromNetwork(myOuter2));
            }
        }else
            return getUserStudentListFromMemory().switchIfEmpty(getUserStudentListFromNetwork(myOuter2));
    }

       /*-----Student end-------*/

       /*-----Guest start-------*/

    @Override
    public Observable<List<UserResponce>> getUserGuestListFromMemory() {
        if (isUpToDate() && userGuest.size() > 0) {
            return Observable.just(userGuest);
        } else {
            timestamp = System.currentTimeMillis();
            userGuest.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<List<UserResponce>> getUserGuestListFromNetwork(Outer.GetUsers myOuter2) {
        Observable<List<UserResponce>>listObservable=apiServiceInterface.getUserList(myOuter2);
        return listObservable.doOnNext(new Action1<List<UserResponce>>() {
            @Override
            public void call(List<UserResponce> userResponces) {
                userGuest = userResponces;
            }
        });
    }

    @Override
    public Observable<List<UserResponce>> getUserGuestListData(Outer.GetUsers myOuter2) {
        if(Constants.userModuleSync !=null) {
            if(Constants.userModuleSync.equals("6")){
                return getUserGuestListFromNetwork(myOuter2);
            }else{
                return getUserGuestListFromMemory().switchIfEmpty(getUserGuestListFromNetwork(myOuter2));
            }
        }else
            return getUserGuestListFromMemory().switchIfEmpty(getUserGuestListFromNetwork(myOuter2));
    }

       /*-----Guest end-------*/

    /*-----Press start-------*/

    @Override
    public Observable<List<UserResponce>> getUserPressListFromMemory() {
        if (isUpToDate() && userPress.size() > 0) {
            return Observable.just(userPress);
        } else {
            timestamp = System.currentTimeMillis();
            userPress.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<List<UserResponce>> getUserPressListFromNetwork(Outer.GetUsers myOuter2) {
        Observable<List<UserResponce>>listObservable=apiServiceInterface.getUserList(myOuter2);
        return listObservable.doOnNext(new Action1<List<UserResponce>>() {
            @Override
            public void call(List<UserResponce> userResponces) {
                userPress = userResponces;
            }
        });
    }

    @Override
    public Observable<List<UserResponce>> getUserPressListData(Outer.GetUsers myOuter2) {
        if(Constants.userModuleSync !=null) {
            if(Constants.userModuleSync.equals("7")){
                return getUserPressListFromNetwork(myOuter2);
            }else{
                return getUserPressListFromMemory().switchIfEmpty(getUserPressListFromNetwork(myOuter2));
            }
        }else
            return getUserPressListFromMemory().switchIfEmpty(getUserPressListFromNetwork(myOuter2));
    }

       /*-----Press start-------*/

    /*-----Sponser start-------*/

    @Override
    public Observable<List<UserResponce>> getUserSponserListFromMemory() {
        if (isUpToDate() && userSponser.size() > 0) {
            return Observable.just(userSponser);
        } else {
            timestamp = System.currentTimeMillis();
            userSponser.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<List<UserResponce>> getUserSponserListFromNetwork(Outer.GetUsers myOuter2) {
        Observable<List<UserResponce>>listObservable=apiServiceInterface.getUserList(myOuter2);
        return listObservable.doOnNext(new Action1<List<UserResponce>>() {
            @Override
            public void call(List<UserResponce> userResponces) {
                userSponser = userResponces;
            }
        });
    }

    @Override
    public Observable<List<UserResponce>> getUserSponserListData(Outer.GetUsers myOuter2) {
        if(Constants.userModuleSync !=null) {
            if(Constants.userModuleSync.equals("8")){
                return getUserSponserListFromNetwork(myOuter2);
            }else{
                return getUserSponserListFromMemory().switchIfEmpty(getUserSponserListFromNetwork(myOuter2));
            }
        }else
            return getUserSponserListFromMemory().switchIfEmpty(getUserSponserListFromNetwork(myOuter2));
    }


       /*-----Sponer end-------*/

    /*-----Audiance start-------*/

    @Override
    public Observable<List<UserResponce>> getUserAudianceListFromMemory() {
        if (isUpToDate() && userAudiance.size() > 0) {
            return Observable.just(userAudiance);
        } else {
            timestamp = System.currentTimeMillis();
            userAudiance.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<List<UserResponce>> getUserAudianceListFromNetwork(Outer.GetUsers myOuter2) {
        Observable<List<UserResponce>>listObservable=apiServiceInterface.getUserList(myOuter2);
        return listObservable.doOnNext(new Action1<List<UserResponce>>() {
            @Override
            public void call(List<UserResponce> userResponces) {
                userAudiance = userResponces;
            }
        });
    }

    @Override
    public Observable<List<UserResponce>> getUserAudianceListData(Outer.GetUsers myOuter2) {

        if(Constants.userModuleSync !=null) {
            if(Constants.userModuleSync.equals("9")){
                return getUserAudianceListFromNetwork(myOuter2);
            }else{
                return getUserAudianceListFromMemory().switchIfEmpty(getUserAudianceListFromNetwork(myOuter2));
            }
        }else
            return getUserAudianceListFromMemory().switchIfEmpty(getUserAudianceListFromNetwork(myOuter2));

    }

    /*-----Audiance end-------*/

    /*Vender start */
    @Override
    public Observable<List<UserResponce>> getUserVenderListFromMemory() {
        if (isUpToDate() && userVender.size() > 0) {
            return Observable.just(userVender);
        } else {
            timestamp = System.currentTimeMillis();
            userVender.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<List<UserResponce>> getUserVenderListFromNetwork(Outer.GetUsers myOuter2) {
        Observable<List<UserResponce>>listObservable=apiServiceInterface.getUserList(myOuter2);
        return listObservable.doOnNext(new Action1<List<UserResponce>>() {
            @Override
            public void call(List<UserResponce> userResponces) {
                userVender = userResponces;
            }
        });
    }

    @Override
    public Observable<List<UserResponce>> getUserVenderListData(Outer.GetUsers myOuter2) {
        if(Constants.userModuleSync !=null) {
            if(Constants.userModuleSync.equals("10")){
                return getUserVenderListFromNetwork(myOuter2);
            }else{
                return getUserVenderListFromMemory().switchIfEmpty(getUserVenderListFromNetwork(myOuter2));
            }
        }else
            return getUserVenderListFromMemory().switchIfEmpty(getUserVenderListFromNetwork(myOuter2));

    }

 /*Vender stop */



    @Override
    public Observable<List<TitleList>> getTitleFromMemory() {

        if (isUpToDate() && titleLists.size() > 0) {
            return Observable.just(titleLists);
        } else {
            timestamp = System.currentTimeMillis();
            titleLists.clear();
            return Observable.empty();
        }

    }

    @Override
    public Observable<List<TitleList>> getTitleFromNetwork() {
        Observable<List<TitleList>> topRatedObservable = apiServiceInterface.getTitleList("");
        return topRatedObservable.doOnNext(new Action1<List<TitleList>>() {
            @Override
            public void call(List<TitleList> titleList) {
                titleLists = titleList;
            }
        });
    }

    @Override
    public Observable<List<TitleList>> getTitleData() {
        return getTitleFromMemory().switchIfEmpty(getTitleFromNetwork());
    }

    @Override
    public Observable<List<CountryList>> getcountryFromMemory() {

        if (isUpToDate() && countryLists.size() > 0) {
            return Observable.just(countryLists);
        } else {
            timestamp = System.currentTimeMillis();
            countryLists.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<List<CountryList>> getcountryFromNetwork() {
        Observable<List<CountryList>> topRatedObservable = apiServiceInterface.getCountryList("");
        return topRatedObservable.doOnNext(new Action1<List<CountryList>>() {
            @Override
            public void call(List<CountryList> countryList) {
                countryLists = countryList;
            }
        });
    }

    @Override
    public Observable<List<CountryList>> getcountryData() {
        return getcountryFromMemory().switchIfEmpty(getcountryFromNetwork());
    }

    @Override
    public Observable<List<StateList>> getStateFromMemory() {

        if (isUpToDate() && stateLists.size() > 0) {
            return Observable.just(stateLists);
        } else {
            timestamp = System.currentTimeMillis();
            stateLists.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<List<StateList>> getStateFromNetwork(Outer.GetStateRequest myOuter2) {
        Observable<List<StateList>> topRatedObservable;
        topRatedObservable = apiServiceInterface.getStateList(myOuter2);
        return topRatedObservable.doOnNext(new Action1<List<StateList>>() {
            @Override
            public void call(List<StateList> stateList) {
                stateLists = stateList;
            }
        });
    }

    @Override
    public Observable<List<StateList>> getStateData(Outer.GetStateRequest myOuter2) {

        return getStateFromMemory().switchIfEmpty(getStateFromNetwork(myOuter2));
    }

    @Override
    public Observable<List<CityList>> getCityFromMemory() {
        if (isUpToDate() && cityLists.size() > 0) {
            return Observable.just(cityLists);
        } else {
            timestamp = System.currentTimeMillis();
            cityLists.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<List<CityList>> getCityFromNetwork(Outer.GetCityRequest myOuter2) {
        Observable<List<CityList>> topRatedObservable;
        topRatedObservable = apiServiceInterface.getCityList(myOuter2);
        return topRatedObservable.doOnNext(new Action1<List<CityList>>() {
            @Override
            public void call(List<CityList> cityList) {
                cityLists = cityList;
            }
        });
    }

    @Override
    public Observable<List<CityList>> getcityData(Outer.GetCityRequest myOuter2) {
        return getCityFromMemory().switchIfEmpty(getCityFromNetwork(myOuter2));
    }

    @Override
    public Observable<List<AirFormList>> getAirFormMemory() {
        if (isUpToDate() && airFormLists.size() > 0) {
            return Observable.just(airFormLists);
        } else {
            timestamp = System.currentTimeMillis();
            airFormLists.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<List<AirFormList>> getAirFromNetwork() {
        Observable<List<AirFormList>> topRatedObservable = apiServiceInterface.getArtFormList("");
        return topRatedObservable.doOnNext(new Action1<List<AirFormList>>() {
            @Override
            public void call(List<AirFormList> airFormList) {
                airFormLists = airFormList;
            }
        });
    }

    @Override
    public Observable<List<AirFormList>> getAirFormData() {
        return getAirFormMemory().switchIfEmpty(getAirFromNetwork());
    }

    @Override
    public Observable<List<AirFormCategoryList>> getAirFormCategoryFromMemory() {
        if (isUpToDate() && airFormCategoryLists.size() > 0) {
            return Observable.just(airFormCategoryLists);
        } else {
            timestamp = System.currentTimeMillis();
            airFormCategoryLists.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<List<AirFormCategoryList>> getAirFormCategoryFromNewwork(Outer.GetArtFormCategory myOuter2) {
        Observable<List<AirFormCategoryList>> topRatedObservable;
        topRatedObservable = apiServiceInterface.getArtFormCatecory(myOuter2);
        return topRatedObservable.doOnNext(new Action1<List<AirFormCategoryList>>() {
            @Override
            public void call(List<AirFormCategoryList> airFormCategoryList) {
                airFormCategoryLists = airFormCategoryList;
            }
        });
    }

    @Override
    public Observable<List<AirFormCategoryList>> getAirFormCategoryData(Outer.GetArtFormCategory myOuter2) {
        return getAirFormCategoryFromMemory().switchIfEmpty(getAirFormCategoryFromNewwork(myOuter2));
    }


    @Override
    public Observable<List<AirFormTypeList>> getAirFormTypeFromMemory() {
        if (isUpToDate() && airFormTypeLists.size() > 0) {
            return Observable.just(airFormTypeLists);
        } else {
            timestamp = System.currentTimeMillis();
            airFormTypeLists.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<List<AirFormTypeList>> getAirFormTypeFromNetwork(Outer.GetArtFormType myOuter2) {
        Observable<List<AirFormTypeList>> topRatedObservable;
        topRatedObservable = apiServiceInterface.getArtFomType(myOuter2);
        return topRatedObservable.doOnNext(new Action1<List<AirFormTypeList>>() {
            @Override
            public void call(List<AirFormTypeList> airFormTypeList) {
                airFormTypeLists = airFormTypeList;
            }
        });
    }

    @Override
    public Observable<List<AirFormTypeList>> getAirFormTypeData(Outer.GetArtFormType myOuter2) {
        return getAirFormTypeFromMemory().switchIfEmpty(getAirFormTypeFromNetwork(myOuter2));
    }


    @Override
    public Observable<List<AirFormGreduList>> getairFormGraduFromMemory() {
        if (isUpToDate() && airFormGreduLists.size() > 0) {
            return Observable.just(airFormGreduLists);
        } else {
            timestamp = System.currentTimeMillis();
            airFormGreduLists.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<List<AirFormGreduList>> getairFormGraduNetwork() {
        Observable<List<AirFormGreduList>> topRatedObservable = apiServiceInterface.getGredu("");
        return topRatedObservable.doOnNext(new Action1<List<AirFormGreduList>>() {
            @Override
            public void call(List<AirFormGreduList> airFormGreduList) {
                airFormGreduLists = airFormGreduList;
            }
        });
    }

    @Override
    public Observable<List<AirFormGreduList>> getairFormGraduData() {
        return getairFormGraduFromMemory().switchIfEmpty(getairFormGraduNetwork());
    }

    @Override
    public Observable<List<MemberShipList>> getMemberFromMemory() {
        if (isUpToDate() && memberShipListsLists.size() > 0) {
            return Observable.just(memberShipListsLists);
        } else {
            timestamp = System.currentTimeMillis();
            memberShipListsLists.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<List<MemberShipList>> getMemberNetwork() {
        Observable<List<MemberShipList>> topRatedObservable = apiServiceInterface.getMemberList("");
        return topRatedObservable.doOnNext(new Action1<List<MemberShipList>>() {
            @Override
            public void call(List<MemberShipList> airFormGreduList) {
                memberShipListsLists = airFormGreduList;
            }
        });
    }

    @Override
    public Observable<List<MemberShipList>> getMemberData() {
        return getMemberFromMemory().switchIfEmpty(getMemberNetwork());
    }

    @Override
    public Observable<List<VenderOptionList>> getVenderOptionFromMemory() {
        if (isUpToDate() && venderOptionLists.size() > 0) {
            return Observable.just(venderOptionLists);
        } else {
            timestamp = System.currentTimeMillis();
            venderOptionLists.clear();
            return Observable.empty();
        }
    }

    @Override
    public Observable<List<VenderOptionList>> getVenderOptionNetwork() {
        Observable<List<VenderOptionList>> topRatedObservable = apiServiceInterface.getVenderOptionList("");
        return topRatedObservable.doOnNext(new Action1<List<VenderOptionList>>() {
            @Override
            public void call(List<VenderOptionList> venderOptionList) {
                venderOptionLists = venderOptionList;
            }
        });
    }

    @Override
    public Observable<List<VenderOptionList>> getvenderOptionData() {
        return getVenderOptionFromMemory().switchIfEmpty(getVenderOptionNetwork());
    }

    private String resultResponse = null;

    @Override
    public void saveUser(final Context context,final Outer.SaveUser saveUser) {

        MusicEarnApp.showProgress(context);

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                IConstantsV2.USERMODULE_USER_CREATION, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    MusicEarnApp.closeProgress();
                    resultResponse = new String(response.data);
                    try {
                        JSONObject jsonObject = new JSONObject(resultResponse);
                        String resultResponse = jsonObject.getString("message");
                        Toast.makeText(context, ""+resultResponse, Toast.LENGTH_SHORT).show();
                        Constants.userModuleSync=saveUser.getUserTypeId();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MusicEarnApp.closeProgress();
                NetworkResponse networkResponse = error.networkResponse;
                resultResponse = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        resultResponse = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        resultResponse = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    Log.e("Error Message", result + networkResponse.statusCode);
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        resultResponse = jsonObject.getString("message");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                   /* if (networkResponse.statusCode == 404) {
                        resultResponse = "Resource not found";
                    } else if (networkResponse.statusCode == 401) {
                        resultResponse = result + " Please login again";
                    } else if (networkResponse.statusCode == 400) {
                        resultResponse = result + " Check your inputs";
                    } else if (networkResponse.statusCode == 500) {
                        resultResponse = result + " Something is getting wrong";
                    }*/
                }
                try {
                    Toast.makeText(context, ""+resultResponse, Toast.LENGTH_SHORT).show();
                    Log.i("Error", resultResponse);
                    error.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                try {
                    String userId = UniversalPreferences.getInstance().get(IConstant.SharedPref_LoginedUserid, "");
                    params.put("userTypeId", saveUser.getUserTypeId());
                    params.put("userName", userId);
                    params.put("titleId", saveUser.getTitleId());
                    params.put("prefixName", saveUser.getPrefixName());
                    params.put("firstName", saveUser.getfName());
                    params.put("middleName", saveUser.getmName());
                    params.put("lastName", saveUser.getlName());
                    params.put("mobileNumber",saveUser.getMobileNumber());
                    params.put("emailId", saveUser.getEmailId());
                    params.put("cityId", saveUser.getCityId());
                    params.put("stateId", saveUser.getStateId());
                    params.put("countryId", saveUser.getCountryId());
                    params.put("gender", saveUser.getRadiogroup_value());
                    if(saveUser.getUserTypeId() != null) {

                        if("6".equals(saveUser.getUserTypeId()) || "7".equals(saveUser.getUserTypeId())
                                || "8".equals(saveUser.getUserTypeId()) || "9".equals(saveUser.getUserTypeId())
                                || "10".equals(saveUser.getUserTypeId())) {

                            if("9".equals(saveUser.getUserTypeId())) {

                                params.put("membershipTypeId", saveUser.getMemberShipId());

                            }else if("10".equals(saveUser.getUserTypeId())) {

                                JSONObject venderService;
                                JSONArray jsonArray = new JSONArray();
                                String jsonStr;
                                for(int i=0; i<saveUser.getVenderServiceSelectedList().size(); i++){

                                    try {
                                        venderService = new JSONObject();
                                        venderService.put("vendorServiceId", saveUser.getVenderServiceSelectedList().get(i));
                                        jsonArray.put(venderService);
                                       /* jsonStr = jsonArray.toString();
                                        System.out.println("jsonString: "+ jsonStr);*/

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                                jsonStr = jsonArray.toString();
                                System.out.println("jsonString: "+ jsonStr);
                                params.put("membershipTypeId", saveUser.getMemberShipId());
                                params.put("vendorServiceList", jsonStr);

                            }
                        }
                    }else{
                        params.put("airFormId", saveUser.getAirFormId());
                        params.put("mainCategoryId", saveUser.getAirFormCategoryId());
                        params.put("mainPerformanceCategoryId", saveUser.getAirFormTypeId());
                        params.put("mainPerformanceOthers", "test others data");
                        params.put("mainAIRAccreditationGradeId", saveUser.getAirAcceretionId());
                        params.put("secondCategoryId", saveUser.getAdditionalArtFormCategoryId());
                        params.put("secondPerformanceCategoryId", saveUser.getAddtionalAirAcceretionId());
                        params.put("secondPerformanceOthers", "test others data");
                        params.put("secondAIRAccreditationGradeId", saveUser.getAddtionalAirAcceretionId());
                    }

                    System.out.println("using entrySet and toString");

                    for (Map.Entry<String, String> entry : params.entrySet()) {
                        System.out.println(entry);
                    }
                    System.out.println();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                byte[] photoimageByte = new byte[0];
               try {
                   if(saveUser.getBitmap()!= null){
                       ByteArrayOutputStream baos = new ByteArrayOutputStream();
                       saveUser.getBitmap().compress(Bitmap.CompressFormat.JPEG, 100, baos);
                       photoimageByte = baos.toByteArray();
                       params.put("profileImage", new DataPart(saveUser.getProfileImagePath(), photoimageByte, "image/jpeg"));
                   }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return params;
            }
        };
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(context).addToRequestQueue(multipartRequest);

    }


}
