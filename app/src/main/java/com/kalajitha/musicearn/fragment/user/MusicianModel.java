package com.kalajitha.musicearn.fragment.user;

import android.content.Context;

import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.model.response.AirFormCategoryList;
import com.kalajitha.musicearn.model.response.AirFormGreduList;
import com.kalajitha.musicearn.model.response.AirFormList;
import com.kalajitha.musicearn.model.response.AirFormTypeList;
import com.kalajitha.musicearn.model.response.CityList;
import com.kalajitha.musicearn.model.response.CountryList;
import com.kalajitha.musicearn.model.response.MemberShipList;
import com.kalajitha.musicearn.model.response.StateList;
import com.kalajitha.musicearn.model.response.TitleList;
import com.kalajitha.musicearn.model.response.UserResponce;
import com.kalajitha.musicearn.model.response.VenderOptionList;

import java.util.List;

import rx.Observable;

/**
 * Created by supercomputer on 4/7/17.
 */

public class MusicianModel implements MusicianMVP.Model {

    Repository repository;

    public MusicianModel(Repository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<List<UserResponce>> getUserModuleMusiciearnList(Outer.GetUsers myOuter2) {
        return repository.getUserMusiciearnListData(myOuter2);
    }

    @Override
    public Observable<List<UserResponce>> getUserModuleDancerList(Outer.GetUsers myOuter2) {
        return repository.getUserDancerListData(myOuter2);
    }

    @Override
    public Observable<List<UserResponce>> getUserModuleTeacherList(Outer.GetUsers myOuter2) {
        return repository.getUserTeacherListData(myOuter2);
    }

    @Override
    public Observable<List<UserResponce>> getUserModuleStudentList(Outer.GetUsers myOuter2) {
        return repository.getUserStudentListData(myOuter2);
    }

    @Override
    public Observable<List<UserResponce>> getUserModuleGuestList(Outer.GetUsers myOuter2) {
        return repository.getUserGuestListData(myOuter2);
    }

    @Override
    public Observable<List<UserResponce>> getUserModulePressList(Outer.GetUsers myOuter2) {
        return repository.getUserPressListData(myOuter2);
    }

    @Override
    public Observable<List<UserResponce>> getUserModuleSponserList(Outer.GetUsers myOuter2) {
        return repository.getUserSponserListData(myOuter2);
    }

    @Override
    public Observable<List<UserResponce>> getUserModuleAudianceList(Outer.GetUsers myOuter2) {
        return repository.getUserAudianceListData(myOuter2);
    }

    @Override
    public Observable<List<UserResponce>> getUserModuleVenderList(Outer.GetUsers myOuter2) {
        return repository.getUserVenderListData(myOuter2);
    }


    @Override
    public Observable<List<TitleList>> getTitleList() {
        return repository.getTitleData();
    }

    @Override
    public Observable<List<CountryList>> getCountryList() {
        return repository.getcountryData();
    }

    @Override
    public Observable<List<StateList>> getStateList(Outer.GetStateRequest myOuter2) {
        return repository.getStateData(myOuter2);
    }

    @Override
    public Observable<List<CityList>> getCityList(Outer.GetCityRequest myOuter2) {
        return repository.getcityData(myOuter2);
    }

    @Override
    public Observable<List<AirFormList>> getAirFormList() {
        return repository.getAirFormData();
    }

    @Override
    public Observable<List<AirFormCategoryList>> getAirFormCategoryList(Outer.GetArtFormCategory myOuter2) {
        return repository.getAirFormCategoryData(myOuter2);
    }

    @Override
    public Observable<List<AirFormTypeList>> getAirFormTypeList(Outer.GetArtFormType myOuter2) {
        return repository.getAirFormTypeData(myOuter2);
    }

    @Override
    public Observable<List<AirFormGreduList>> getAirFormGraduList() {
        return repository.getairFormGraduData();
    }

    @Override
    public Observable<List<MemberShipList>> getMemberList() {
        return repository.getMemberData();
    }

    @Override
    public Observable<List<VenderOptionList>> getVenderOptionList() {
        return repository.getvenderOptionData();
    }

    @Override
    public void createUser(Context context ,Outer.SaveUser saveUser) {
         repository.saveUser(context,saveUser);
    }
}
