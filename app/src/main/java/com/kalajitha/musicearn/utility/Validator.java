package com.kalajitha.musicearn.utility;

public class Validator {

	public final static boolean isValidEmail(final String email) {
        boolean valid = false;

		if (email != null) {
			valid = android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
		}
		return valid;
	}	 


	public static boolean isEmailValid(final String emailAddress) {
		return Validator.isValidEmail(emailAddress);
	}

	
}