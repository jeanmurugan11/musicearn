package com.kalajitha.musicearn.utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.kalajitha.musicearn.model.request.Outer;
import com.kalajitha.musicearn.network.volleymultiform.VolleyMultipartRequest;
import com.kalajitha.musicearn.network.volleymultiform.VolleySingleton;
import com.kalajitha.musicearn.root.MusicEarnApp;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

public class NetworkRequestUsingVolly {

	public static void CreateEvent(final Context MContext , final Outer.CreateEventRequest createEvent) {

		MusicEarnApp.showProgress(MContext);
		String url="http://demo.musicearn.in/api/Event/CreateEvent";

		VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url,
				new Response.Listener<NetworkResponse>() {
					@Override
					public void onResponse(NetworkResponse response) {
						try {
							MusicEarnApp.closeProgress();
							String resultResponse1 = new String(response.data);
							try {

								JSONObject jsonObject = new JSONObject(resultResponse1);
								String resultResponse = jsonObject.getString("message");
								Toast.makeText(MContext, "" + resultResponse, Toast.LENGTH_SHORT).show();

								//PlanningFragment.eventCreationResponse(resultResponse);

							} catch (JSONException e) {
								e.printStackTrace();
							}

						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				MusicEarnApp.closeProgress();
				NetworkResponse networkResponse = error.networkResponse;
				String resultResponse = "Unknown error";
				if (networkResponse == null) {
					if (error.getClass().equals(TimeoutError.class)) {
						resultResponse = "Request timeout";
					} else if (error.getClass().equals(NoConnectionError.class)) {
						resultResponse = "Failed to connect server";
					}
				} else {
					String result = new String(networkResponse.data);
					Log.e("Error Message", result + networkResponse.statusCode);
					try {
						JSONObject jsonObject = new JSONObject(result);
						resultResponse = jsonObject.getString("message");
					} catch (JSONException e) {
						e.printStackTrace();
					}
				   /* if (networkResponse.statusCode == 404) {
                        resultResponse = "Resource not found";
                    } else if (networkResponse.statusCode == 401) {
                        resultResponse = result + " Please login again";
                    } else if (networkResponse.statusCode == 400) {
                        resultResponse = result + " Check your inputs";
                    } else if (networkResponse.statusCode == 500) {
                        resultResponse = result + " Something is getting wrong";
                    }*/
				}
				try {
					Toast.makeText(MContext, "" + resultResponse, Toast.LENGTH_SHORT).show();
					Log.i("Error", resultResponse);
					error.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}) {
			@Override
			protected Map<String, String> getParams() {
				Map<String, String> params = new HashMap<>();
				try {
					/*Post Method  Form data eventPurposeID,eventTile,eventStart,eventEnd,slogan,UserName=9043147655,[file,filename]*/
					params.put("eventPurposeID", createEvent.getEventPurposeID());/*eventPurposeID*/
					params.put("eventTile", createEvent.getEventTile());/*eventTile*/
					params.put("eventStart", createEvent.getEventStart());/*eventStart*/
					params.put("eventEnd", createEvent.getEventEnd());/*eventEnd*/
					params.put("slogan", createEvent.getSlogan()); /*slogan*/
					params.put("UserName", createEvent.getUserName());/*UserName*/

					for (Map.Entry<String, String> entry : params.entrySet()) {
						System.out.println(entry);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return params;
			}

			@Override
			protected Map<String, DataPart> getByteData() {
				Map<String, DataPart> params = new HashMap<>();
				byte[] photoimageByte = new byte[0];
				try {
					if (createEvent.getBitmap() != null) {
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						createEvent.getBitmap().compress(Bitmap.CompressFormat.JPEG, 100, baos);
						photoimageByte = baos.toByteArray();
						params.put("profileImage", new DataPart(createEvent.getProfileImagePath(), photoimageByte, "image/jpeg"));
						System.out.println("eventcreation-->"+createEvent.getProfileImagePath());
					}


				} catch (Exception e) {
					e.printStackTrace();
				}
				return params;
			}
		};
		multipartRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
		VolleySingleton.getInstance(MContext).addToRequestQueue(multipartRequest);
	}
}